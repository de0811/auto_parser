#include <string>
#include <stdio.h>
#include <stdarg.h>

class Log{
public:
    enum{
        LOG_ALL,
        LOG_INFO,
        LOG_DEBUG,
        LOG_WARING,
        LOG_ERROR,
    };
    static void print(unsigned int type, char const* const _Format, ...);
    static void print(unsigned int type, int deeps, char const* const _Format, ...);
};
void Log::print(unsigned int type, char const* const _Format, ...){
    std::string str_log = "";
    switch(type){
        case LOG_INFO:
        str_log += "";
        break;
        case LOG_DEBUG:
        str_log += "DDDD ";
        break;
        case LOG_WARING:
        str_log += "@@@@ ";
        break;
        case LOG_ERROR:
        str_log += "!!!! ";
        break;
    }

    str_log += _Format;

    int _Result;
    // char buf[10000] = {0,};
    va_list _ArgList;
/*
    va_start(_ArgList, _Format);
    vsprintf(buf, str_log.c_str(), _ArgList);
    va_end(_ArgList);
    printf(buf);
    */


    va_start(_ArgList, _Format);
    _Result = vprintf(str_log.c_str(), _ArgList);
    va_end(_ArgList);
}

void Log::print(unsigned int type, int deeps, char const* const _Format, ...){
    // if(type == LOG_DEBUG) return;
    std::string str_log = "";
    switch(type){
        case LOG_INFO:
        str_log += "";
        break;
        case LOG_DEBUG:
        str_log += "DDDD ";
        break;
        case LOG_WARING:
        str_log += "@@@@ ";
        break;
        case LOG_ERROR:
        str_log += "!!!! ";
        break;
    }
    for(int i = 0 ; i < deeps; i++) str_log += "\t";

    str_log += _Format;

    int _Result;
    // char buf[10000] = {0,};
    va_list _ArgList;
/*
    va_start(_ArgList, _Format);
    vsprintf(buf, str_log.c_str(), _ArgList);
    va_end(_ArgList);
    printf(buf);
    */


    va_start(_ArgList, _Format);
    _Result = vprintf(str_log.c_str(), _ArgList);
    va_end(_ArgList);
}