
#include "dex_common.h"
#include "dex_base_parssing.h"
#include "dex_encoded_value.h"
#include <vector>




#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ANNOTATION_SET
#define _IN_ANNOTATION_SET

class Annotation_set : public Base_dex_parssing, public Base_dex_reference{

private:
#pragma pack(push, 1)
    struct Annotation_set_item{
                u4                                      size;
                std::vector<u4>                         annotation_off_item;
    };
#pragma pack(pop)
    std::vector<unsigned int>               m_annotation_set_offs;
    std::vector<Annotation_set_item>        m_annotation_sets;
protected:
    unsigned int __parssing(unsigned int _command_flag);
public:
};

unsigned int Annotation_set::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    Base_dex_reference* pannotation = get_reference(eTYPE_ANNOTATION_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        Log::print(Log::LOG_INFO, "**Annotation_set_item ----\n");
        Log::print(Log::LOG_INFO, "------------------------------------------------------------------------------------------------------\n");
        if(pannotation == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Annotation_set.__parssing reference NULL !\n");
            return m_start_offset;

        }
    }

    m_annotation_sets.reserve(m_offset_count);
    m_annotation_set_offs.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        m_annotation_set_offs.push_back(m_start_offset + offset_revision);
        Annotation_set_item annotation_set_item;
        annotation_set_item.size = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotation_set.size\t 0x%02X\n", m_start_offset + offset_revision, i, annotation_set_item.size);
        offset_revision += sizeof(u4);
        
        annotation_set_item.annotation_off_item.reserve(annotation_set_item.size);
        for(unsigned int k = 0; k < annotation_set_item.size; k++){
            u4 annotation_off = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotation_set[%d].annotation_off\t 0x%08X\t %s\n", m_start_offset + offset_revision, i, k, annotation_off, pannotation->as_off_string(annotation_off).c_str());
            offset_revision += sizeof(u4);
            annotation_set_item.annotation_off_item.push_back(annotation_off);
        }
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
        m_annotation_sets.push_back(annotation_set_item);
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");


    return m_start_offset + offset_revision;
}


#endif /* _IN_ANNOTATION_SET */