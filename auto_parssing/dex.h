﻿#pragma warning(disable : 4996)
/*
참조 문서
https://source.android.com/devices/tech/dalvik/dex-format#code-item
https://android.googlesource.com/platform/dalvik/+/android-4.4.2_r2/libdex
http://www.nurilab.co.kr/?p=465
https://github.com/VirusTotal/yara
*/
#include <stdio.h>
#include <vector>
#include "dex_common.h"
#include "dex_header.h"
#include "dex_map_list.h"
#include "dex_string_ids.h"
#include "dex_type_ids.h"
#include "dex_type_list.h"
#include "dex_proto_ids.h"
#include "dex_field_ids.h"
#include "dex_method_ids.h"
#include "dex_access_flags.h"
#include "dex_class_defs.h"
#include "dex_code_items.h"
#include "dex_call_site_ids.h"
#include "dex_method_handle_ids.h"
#include "dex_class_data.h"
#include "dex_debug_info.h"
#include "dex_annotation.h"
#include "dex_annotation_set.h"
#include "dex_annotation_set_ref.h"
#include "dex_annotations_directory.h"
#include "dex_encoded_array.h"
#include "cd_option.h"


#ifdef  __cplusplus
extern "C" {
#endif
/*
opt.add_opt("", CD_option<C_dex>::OPT_TYPE::MAIN, 2, 0, 0, &dex, &C_dex::parssing, NULL);
opt.add_opt("-h", CD_option<C_dex>::OPT_TYPE::MAIN, 2, 0, 0, &dex, &C_dex::parssing, NULL);
opt.add_opt("-s", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 1, 1, &dex, &C_dex::set_dex_path, NULL);
opt.add_opt("-w", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 1, 1, &dex, &C_dex::set_write_path, NULL);
opt.add_opt("-p", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 0, 0, &dex, &C_dex::set_log_print, NULL);
opt.add_opt("-ob", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 0, 0, &dex, &C_dex::set_apk_obfuscation, NULL);
opt.add_opt("-no", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 0, 0, &dex, &C_dex::set_apk_normalization, NULL);
*/
unsigned int help_print(std::vector<const char*> args){
    printf("----------------------------------------------------------------------\n");
    printf("auto_parssing.exe -s <dex_path>\n");
    printf("OPTION\n");
    printf("MAIN : default\n");
    printf("DESC : dex_path Parssing\n");
    printf("    MUST OPTION : -s <dex_path>\n");
    printf("    DESC : dex file path\n");
    printf("    OPTIONALLY OPTION : -p\n");
    printf("    DESC : running print\n");
    printf("    OPTIONALLY OPTION : -ob\n");
    printf("    DESC : obfuscation apply\n");
    printf("            MUST OPTION : -w <out_dex_path>\n");
    printf("            DESC : write path\n");
    printf("    OPTIONALLY OPTION : -no\n");
    printf("    DESC : normalization apply\n");
    printf("            MUST OPTION : -w\n");
    printf("            DESC : write path\n");
    printf("MAIN : -h\n");
    printf("DESC : help\n");
    printf("----------------------------------------------------------------------\n");
    return I_option_state::eSTATE_OK;
}
#ifdef  __cplusplus
}
#endif

//dex
class C_dex : public I_option_state{
protected:
    Dex_header              dex_header;
    Map_list                map_list;
    String_ids              string_ids;
    Type_ids                type_ids;
    Type_lists              type_lists;
    Proto_ids               proto_ids;
    Field_ids               field_ids;
    Method_ids              method_ids;
    Class_defs              class_defs;
    Code_items              code_items;
    Call_site_ids           call_site_ids;
    Method_handle_ids       method_handle_ids;
    Class_data              class_data;
    Debug_info              debug_info;
    Annotation              annotation;
    Encoded_array           encoded_array;
    Annotation_set          annotation_set;
    Annotation_set_ref      annotation_set_ref;
    Annotations_directory   annotations_directory;

    void parssing_dex(unsigned char* pBuf, unsigned int size);
// ------------ 하위는 CD_option 관련--------------------------------------------
private:
    char* _dex_path = NULL;
    char* _write_path = NULL;
    bool _log_print = false;
    bool _apk_obfuscation = false;
    bool _apk_normalization = false;
    unsigned int _command_flag = 0;
public:
    unsigned int set_dex_path(std::vector<const char*> args);
    unsigned int set_write_path(std::vector<const char*> args);
    unsigned int set_log_print(std::vector<const char*> args);
    unsigned int set_apk_obfuscation(std::vector<const char*> args);
    unsigned int set_apk_normalization(std::vector<const char*> args);
    unsigned int parssing(std::vector<const char*> args);



    unsigned int get_file_size(char *pathname)
    {
        FILE* pf = fopen(pathname, "rb");
        if (!pf) return 0;

        fseek(pf, 0, SEEK_END);
        unsigned int size = ftell(pf);

        fclose(pf);

        return size;
    }
};

unsigned int C_dex::set_dex_path(std::vector<const char*> args){
    printf("option -s \n");
    for(unsigned int i = 0; i < args.size(); i++){
        printf("-s args::%s\n", args[i]);
    }
    if(args.size() == 0){
        return eSTATE_ERROR_ARGS_COUNT;
    }
    _dex_path = (char*)args[0];
    return eSTATE_OK;
}
unsigned int C_dex::set_write_path(std::vector<const char*> args){
    printf("option -w \n");
    for(unsigned int i = 0; i < args.size(); i++){
        printf("-w args::%s\n", args[i]);
    }
    if(args.size() == 0){
        return eSTATE_ERROR_ARGS_COUNT;
    }
    _write_path = (char*)args[0];

    return eSTATE_OK;
}
unsigned int C_dex::set_log_print(std::vector<const char*> args){
    printf("option -p \n");
    for(unsigned int i = 0; i < args.size(); i++){
        printf("-p args::%s\n", args[i]);
    }
    _command_flag |= Base_dex_parssing::eFLAG_LOG;
    // Base_dex_parssing::set_command_flag(Base_dex_parssing::eFLAG_LOG);
    _log_print = true;
    return eSTATE_OK;
}
unsigned int C_dex::set_apk_obfuscation(std::vector<const char*> args){
    printf("option -ob \n");
    for(unsigned int i = 0; i < args.size(); i++){
        printf("-ob args::%s\n", args[i]);
    }
    _command_flag |= Base_dex_parssing::eFLAG_OBFUSCATION;
    // Base_dex_parssing::set_command_flag(Base_dex_parssing::eFLAG_OBFUSCATION);
    _apk_obfuscation = true;
    return eSTATE_OK;
}
unsigned int C_dex::set_apk_normalization(std::vector<const char*> args){
    printf("option -no \n");
    for(unsigned int i = 0; i < args.size(); i++){
        printf("-no args::%s\n", args[i]);
    }
    _command_flag |= Base_dex_parssing::eFLAG_NORMALIZATION;
    // Base_dex_parssing::set_command_flag(Base_dex_parssing::eFLAG_NORMALIZATION);
    _apk_normalization = true;
    return eSTATE_OK;
}
unsigned int C_dex::parssing(std::vector<const char*> args){
    printf("option -default \n");
    for(unsigned int i = 0; i < args.size(); i++){
        printf("-default args::%s\n", args[i]);
    }
    if(_dex_path == NULL){
        printf("ERROR !! option -s add Please\n");
		return eSTATE_ERROR;
    }
    if(_command_flag & Base_dex_parssing::eFLAG_NORMALIZATION || \
        _command_flag & Base_dex_parssing::eFLAG_OBFUSCATION ){
        if(_write_path == NULL){
            printf("ERROR !! option -w add Please\n");
            // return eSTATE_ERROR;
        }
    }
    if(_write_path != NULL){
        if( !(_command_flag & Base_dex_parssing::eFLAG_NORMALIZATION || \
            _command_flag & Base_dex_parssing::eFLAG_OBFUSCATION) ){
            printf("ERROR !! option -no / -ob add Please\n");
        }
    }

    unsigned int size = get_file_size(_dex_path);
    unsigned char* pBuf = new unsigned char[size + 1];

// /*
    FILE *ff;

    ff = fopen(_dex_path, "rb");
    if(ff)
    {
        fread(pBuf, 1, size, ff);
        printf("File Path :: %s SIZE[%d]\n", _dex_path, size);
        fclose(ff);
        parssing_dex(pBuf, size);

        if(_write_path != NULL){
            ff = fopen(_write_path, "wb");
            if(ff)
            {
                printf("Write File Path :: %s\n", _write_path);
                fwrite(pBuf, 1, size, ff);
                fclose(ff);
            }
        }


    }
// */
    delete[] pBuf;

    return eSTATE_OK;
}

void C_dex::parssing_dex(unsigned char* pBuf, unsigned int size) {
    if( (int) size <= 0 ) return;

    dex_header.init(pBuf,                       0,                                                                          size);
    dex_header.parssing(_command_flag);
    map_list.init(pBuf,                         dex_header.m_header_item.map_off,                                           2);
    map_list.parssing(_command_flag);
    string_ids.init(pBuf,                       map_list.get(Map_list::eTYPE_STRING_ID_ITEM).first,                         map_list.get(Map_list::eTYPE_STRING_ID_ITEM).second);
    string_ids.parssing(_command_flag);
    type_ids.init(pBuf,                         map_list.get(Map_list::eTYPE_TYPE_ID_ITEM).first,                           map_list.get(Map_list::eTYPE_TYPE_ID_ITEM).second);
    type_ids.parssing(_command_flag);
    type_lists.init(pBuf,                       map_list.get(Map_list::eTYPE_TYPE_LIST).first,                              map_list.get(Map_list::eTYPE_TYPE_LIST).second);
    type_lists.parssing(_command_flag);
    proto_ids.init(pBuf,                        map_list.get(Map_list::eTYPE_PROTO_ID_ITEM).first,                          map_list.get(Map_list::eTYPE_PROTO_ID_ITEM).second);
    proto_ids.parssing(_command_flag);
    field_ids.init(pBuf,                        map_list.get(Map_list::eTYPE_FIELD_ID_ITEM).first,                          map_list.get(Map_list::eTYPE_FIELD_ID_ITEM).second);
    field_ids.parssing(_command_flag);
    method_ids.init(pBuf,                       map_list.get(Map_list::eTYPE_METHOD_ID_ITEM).first,                         map_list.get(Map_list::eTYPE_METHOD_ID_ITEM).second);
    method_ids.parssing(_command_flag);
    class_defs.init(pBuf,                       map_list.get(Map_list::eTYPE_CLASS_DEF_ITEM).first,                         map_list.get(Map_list::eTYPE_CLASS_DEF_ITEM).second);
    class_defs.parssing(_command_flag);
    code_items.init(pBuf,                       map_list.get(Map_list::eTYPE_CODE_ITEM).first,                              map_list.get(Map_list::eTYPE_CODE_ITEM).second);
    code_items.parssing(_command_flag);
    call_site_ids.init(pBuf,                    map_list.get(Map_list::eTYPE_CALL_SITE_ID_ITEM).first,                      map_list.get(Map_list::eTYPE_CALL_SITE_ID_ITEM).second);
    call_site_ids.parssing(_command_flag);
    method_handle_ids.init(pBuf,                map_list.get(Map_list::eTYPE_METHOD_HANDLE_ITEM).first,                     map_list.get(Map_list::eTYPE_METHOD_HANDLE_ITEM).second);
    method_handle_ids.parssing(_command_flag);
    class_data.init(pBuf,                       map_list.get(Map_list::eTYPE_CLASS_DATA_ITEM).first,                        map_list.get(Map_list::eTYPE_CLASS_DATA_ITEM).second);
    class_data.parssing(_command_flag);
    debug_info.init(pBuf,                       map_list.get(Map_list::eTYPE_DEBUG_INFO_ITEM).first,                        map_list.get(Map_list::eTYPE_DEBUG_INFO_ITEM).second);
    debug_info.parssing(_command_flag);
    annotation.init(pBuf,                       map_list.get(Map_list::eTYPE_ANNOTATION_ITEM).first,                        map_list.get(Map_list::eTYPE_ANNOTATION_ITEM).second);
    annotation.parssing(_command_flag);
    encoded_array.init(pBuf,                    map_list.get(Map_list::eTYPE_ENCODED_ARRAY_ITEM).first,                     map_list.get(Map_list::eTYPE_ENCODED_ARRAY_ITEM).second);
    encoded_array.parssing(_command_flag);
    annotation_set.init(pBuf,                   map_list.get(Map_list::eTYPE_ANNOTATION_SET_ITEM).first,                    map_list.get(Map_list::eTYPE_ANNOTATION_SET_ITEM).second);
    annotation_set.parssing(_command_flag);
    annotation_set_ref.init(pBuf,               map_list.get(Map_list::eTYPE_ANNOTATION_SET_REF_LIST).first,                map_list.get(Map_list::eTYPE_ANNOTATION_SET_REF_LIST).second);
    annotation_set_ref.parssing(_command_flag);
    annotations_directory.init(pBuf,            map_list.get(Map_list::eTYPE_ANNOTATIONS_DIRECTORY_ITEM).first,             map_list.get(Map_list::eTYPE_ANNOTATIONS_DIRECTORY_ITEM).second);
    annotations_directory.parssing(_command_flag);

    // proto_ids.convert();
    // class_defs.convert();


    // String_ids              tmp_string_ids;
    // tmp_string_ids.init(pBuf,                       map_list.get(Map_list::eTYPE_STRING_ID_ITEM).first,                         map_list.get(Map_list::eTYPE_STRING_ID_ITEM).second);
    // tmp_string_ids.parssing(_command_flag);

    dex_header.apply_cal_sha1(size);
    dex_header.apply_cal_checksum(size);


}

