#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>
#include <string>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_METHOD_HANDLE_IDS
#define _IN_METHOD_HANDLE_IDS

class Method_handle_ids : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Method_handle_item{
                u2              method_handle_type;                     //2
                u2              unused1;                                //4
                u2              field_or_method_id;                     //6
                u2              unused2;                                //8
    };
#pragma pack(pop)
    enum{
        eMETHOD_HANDLE_TYPE_STATIC_PUT              =0x00,
        eMETHOD_HANDLE_TYPE_STATIC_GET              =0x01,
        eMETHOD_HANDLE_TYPE_INSTANCE_PUT            =0x02,
        eMETHOD_HANDLE_TYPE_INSTANCE_GET            =0x03,
        eMETHOD_HANDLE_TYPE_INVOKE_STATIC           =0x04,
        eMETHOD_HANDLE_TYPE_INVOKE_INSTANCE         =0x05,
        eMETHOD_HANDLE_TYPE_INVOKE_CONSTRUCTOR      =0x06,
        eMETHOD_HANDLE_TYPE_INVOKE_DIRECT           =0x07,
        eMETHOD_HANDLE_TYPE_INVOKE_INTERFACE        =0x08,
    };

    std::vector<Method_handle_ids::Method_handle_item> m_method_handle_ids;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
public:
    Method_handle_ids(){init_reference(eTYPE_METHOD_HANDLE_ITEM, this);}
    std::string as_type_string(unsigned int type);
};

unsigned int Method_handle_ids::__parssing(unsigned int _command_flag){
    Base_dex_reference* pmethod_ids = get_reference(eTYPE_METHOD_ID_ITEM);
    Base_dex_reference* pproto_ids = get_reference(eTYPE_PROTO_ID_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        if(pmethod_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Method_handle_ids.__parssing() reference is NULL\n");
            return m_start_offset;
        }
        Log::print(Log::LOG_INFO, "**Method handle ids Full\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t DATA\n");
        Log::print(Log::LOG_INFO, "-----------------------------------------------------------------------------------------\n");
    }
    unsigned int offset_revision = 0;

    m_method_handle_ids.reserve(m_offset_count);
    Method_handle_item mhi;
    for(unsigned int i = 0; i < m_offset_count; i++){
        mhi.method_handle_type = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %4d\t method_handle_item.method_handle_type\t %d\t %s\n", m_start_offset + offset_revision, i, mhi.method_handle_type, as_type_string(mhi.method_handle_type).c_str());
        offset_revision += sizeof(u4);

        mhi.unused1 = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %4d\t method_handle_item.unused1\t %d\n", m_start_offset + offset_revision, i, mhi.unused1);
        offset_revision += sizeof(u2);

        mhi.field_or_method_id = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(mhi.method_handle_type == eMETHOD_HANDLE_TYPE_INVOKE_STATIC || mhi.method_handle_type == eMETHOD_HANDLE_TYPE_INVOKE_INSTANCE){
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %4d\t method_handle_item.field_or_method_id\t %d\t %s\n", m_start_offset + offset_revision, i, mhi.field_or_method_id, pmethod_ids->as_string(mhi.field_or_method_id).c_str());
        }
        else{
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %4d\t method_handle_item.field_or_method_id\t %d\t %s\n", m_start_offset + offset_revision, i, mhi.field_or_method_id, pproto_ids->as_string(mhi.field_or_method_id).c_str());
        }
        offset_revision += sizeof(u2);

        mhi.unused2 = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %4d\t method_handle_item.unused2 %d\n", m_start_offset + offset_revision, i, mhi.unused2);
        offset_revision += sizeof(u2);
        
        m_method_handle_ids.push_back(mhi);
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
    }
    return m_start_offset + offset_revision;
}

std::string Method_handle_ids::as_type_string(unsigned int type){
    switch(type){
        case eMETHOD_HANDLE_TYPE_STATIC_PUT              :
        return std::string("static-put");
        case eMETHOD_HANDLE_TYPE_STATIC_GET              :
        return std::string("static-get");
        case eMETHOD_HANDLE_TYPE_INSTANCE_PUT            :
        return std::string("instance_put");
        case eMETHOD_HANDLE_TYPE_INSTANCE_GET            :
        return std::string("instance-get");
        case eMETHOD_HANDLE_TYPE_INVOKE_STATIC           :
        return std::string("static-invoke");
        case eMETHOD_HANDLE_TYPE_INVOKE_INSTANCE         :
        return std::string("instance-invoke");
        case eMETHOD_HANDLE_TYPE_INVOKE_CONSTRUCTOR      :
        return std::string("constructor-invoke");
        case eMETHOD_HANDLE_TYPE_INVOKE_DIRECT           :
        return std::string("direct-invoke");
        case eMETHOD_HANDLE_TYPE_INVOKE_INTERFACE        :
        return std::string("interface-invoke");
        default:
        return std::string("");
    }
    return std::string("");
}

#endif /* _IN_METHOD_HANDLE_IDS */