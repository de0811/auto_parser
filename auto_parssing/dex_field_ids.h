#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_FIELD_IDS
#define _IN_FIELD_IDS



class Field_ids :public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Field_id_item{
                unsigned short              class_idx;                     //2
                unsigned short              return_type_idx;                //4
                unsigned int                name_idx;                 //8
    };
#pragma pack(pop)
    //Field_ids 의 모든 데이터 저장
    std::vector<Field_id_item>      m_field_ids;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
public:
    Field_ids(){init_reference(eTYPE_FIELD_ID_ITEM, this);}
    std::string as_string(unsigned int idx);
};

unsigned int Field_ids::__parssing(unsigned int _command_flag){
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        if(ptype_ids == NULL || pstr_ids == NULL) {
            Log::print(Log::LOG_INFO, "FIELD_IDS sub Reference ERROR !!");
            return m_start_offset;
        }
        Log::print(Log::LOG_INFO, "**field ids Full\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t\t IDX\t STRING\n");
        Log::print(Log::LOG_INFO, "-----------------------------------------------------------------------------------------\n");
    }


    unsigned int offset_revision = 0;
    m_field_ids.reserve(m_offset_count);

    for(unsigned int i = 0; i < m_offset_count; i++){
        Field_ids::Field_id_item field;

        field.class_idx = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t field_item.class_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, field.class_idx, ptype_ids->as_string(field.class_idx).c_str());
        offset_revision += sizeof(field.class_idx);
        field.return_type_idx = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t field_item.return_type_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, field.return_type_idx, ptype_ids->as_string(field.return_type_idx).c_str());
        offset_revision += sizeof(u2);
        field.name_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t field_item.name_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, field.name_idx, pstr_ids->as_string(field.name_idx).c_str());
        offset_revision += sizeof(u4);
        m_field_ids.push_back(field);

        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n\n");   
    return m_start_offset + offset_revision;
}

std::string Field_ids::as_string(unsigned int idx){
    if(m_field_ids.size() <= idx){
        Log::print(Log::LOG_ERROR, "ERROR !! Field_ids.as_string(%d) idx over !\n", idx);
        return std::string("");
    }
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    if(pstr_ids == NULL || ptype_ids == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Field_ids.as_string(%d) reference NULL !\n", idx);
        return std::string("");
    }
    std::string result("");
    result.append( ptype_ids->as_string(m_field_ids[idx].class_idx) );
    result.append("->");
    result.append( pstr_ids->as_string(m_field_ids[idx].name_idx) );
    result.append(":");
    result.append( ptype_ids->as_string(m_field_ids[idx].return_type_idx) );
    return result;
}


#endif /* _IN_FIELD_IDS */