#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_CALL_SITE_IDS
#define _IN_CALL_SITE_IDS

class Call_site_ids : public Base_dex_parssing, public Base_dex_reference{
private:
    std::vector<u4> m_call_site_ids;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
};

unsigned int Call_site_ids::__parssing(unsigned int _command_flag){
    if(get_command_flag(eFLAG_LOG)){
        Log::print(Log::LOG_INFO, "**Call Site ids Full\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t DATA\n");
        Log::print(Log::LOG_INFO, "-----------------------------------------------------------------------------------------\n");
    }
    unsigned int offset_revision = 0;

    m_call_site_ids.reserve(m_offset_count);
    unsigned int call_site_item = 0;
    for(unsigned int i = 0; i < m_offset_count; i++){
        call_site_item = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %4d\t %d\n", m_start_offset + offset_revision, i, call_site_item);
        offset_revision += sizeof(u4);
        m_call_site_ids.push_back(call_site_item);
    }
    return m_start_offset + offset_revision;
}

#endif /* _IN_CALL_SITE_IDS */