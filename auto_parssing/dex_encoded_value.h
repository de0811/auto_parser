#include "dex_common.h"
#include "dex_base_parssing.h"
#include "leb128.h"
#include <vector>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ENCODED_VALUE
#define _IN_ENCODED_VALUE

class Encoded_value : public Base_dex_parssing{
public:
    class Base_encoded{
    public:
        u1 type;
    };
    class Encoded_data : public Base_encoded{
    public:
        union {
            u1          _byte;
            s2          _short;
            u2          _char;
            s4          _int;
            s8          _long;
            u4          _int_float;     //float 를 받기 위해 우선적으로 int 값으로 받음
            float       _float;
            u8          _log_double;    //double 을 받기 위해 우선적으로 long 값으로 받음
            double      _double;
            u4          _str_idx;
            u4          _type_idx;
            u4          _field_idx;
            u4          _enum;
            u4          _method_idx;
            u4          _method_type;
            u4          _method_handle;
            u1          _bool;
        };
        // union Data data;
    };
    class Encoded_array : public Base_encoded{
    public:
        // u1 type;     //Base_encoded
        ULEB128                         _size;
        std::vector<Base_encoded*>       _data;
    };
    class Encoded_annotation : public Encoded_array{
    public:
        // u1 type;     //Base_encoded
        // ULEB128                         _size;       //Encoded_array
        // std::vector<Encoded_data>       _data;       //Encoded_array
        ULEB128                             type_idx;
        std::vector<ULEB128>                name_ids;
    };

    enum{
        eBYTE           =0x00,
        eSHORT          =0x02,
        eCHAR           =0x03,
        eINT            =0x04,
        eLONG           =0x06,
        eFLOAT          =0x10,
        eDOUBLE         =0x11,
        eMETHOD_TYPE    =0x15,
        eMETHOD_HANDLE  =0x16,
        eSTRING         =0x17,
        eTYPE           =0x18,
        eFIELD          =0x19,
        eMETHOD         =0x1a,
        eENUM           =0x1b,
        eARRAY          =0x1c,
        eANNOTATION     =0x1d,
        eNULL           =0x1e,
        eBOOLEAN        =0x1f,
    };
private:
    u8 __read_var_width(const u1** data, u1 arg, bool sign_extend);
    u1 m_type = 0;
    u1 m_arg = 0;
    Base_encoded *m_value = NULL;
    unsigned int __parssing_encoded_value(unsigned int start_offset, Base_encoded** encoded, int deeps, bool cprint);
    unsigned int __parssing_encoded_value(unsigned int start_offset, u1 type, u1 arg, Base_encoded** encoded, int deeps, bool cprint);
protected:
    unsigned int __parssing(unsigned int _command_flag);
public:
    unsigned int parssing_annotation(int deeps, bool cprint);
    unsigned int parssing_array(int deeps, bool cprint);
    std::string as_annotation_string();
};

unsigned int Encoded_value::__parssing_encoded_value(unsigned int start_offset, Base_encoded** encoded, int deeps, bool cprint){
    unsigned int offset_revision = 0;

    u1 enc = *((u1*)(m_dex + start_offset + offset_revision));
    offset_revision += sizeof(u1);
    u1 type = enc & 0x1f;
    u1 arg = enc >> 5;
    // Log::print(Log::LOG_INFO, deeps, "Annotation TYPE :: %d\n", type);

    return __parssing_encoded_value(start_offset + offset_revision, type, arg, encoded, deeps, cprint);
}

unsigned int Encoded_value::__parssing_encoded_value(unsigned int start_offset, u1 type, u1 arg, Base_encoded** encoded, int deeps, bool cprint){
    unsigned int offset_revision = 0;
    unsigned char *tmp_dex = m_dex + start_offset;
    deeps++;
    switch(type){
        case eBYTE           :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eBYTE;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_byte = static_cast<s1>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._byte\t 0x%08X\n", ed->_byte);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eSHORT          :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eSHORT;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_short = static_cast<s2>( __read_var_width((const u1**)&tmp_dex, arg, true) );
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._short\t 0x%08X\n", ed->_byte);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eCHAR           :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eCHAR;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_char = static_cast<u2>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._char\t 0x%08X\n", ed->_char);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eINT            :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eINT;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_int = static_cast<s4>( __read_var_width((const u1**)&tmp_dex, arg, true) );
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._int\t 0x%08X\n", ed->_int);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eLONG           :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eLONG;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_long = static_cast<s8>( __read_var_width((const u1**)&tmp_dex, arg, true) );
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._long\t 0x%08llX\n", ed->_long);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eFLOAT          :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eFLOAT;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_int_float = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) ) << (3 - arg) * 8;
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._float\t %f\n", ed->_float);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eDOUBLE         :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eDOUBLE;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_log_double = static_cast<u8>( __read_var_width((const u1**)&tmp_dex, arg, false) ) << (7 - arg) * 8;
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._double\t %llf\n", ed->_double);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eMETHOD_TYPE    :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eMETHOD_TYPE;
            Base_dex_reference* pproto_ids = get_reference(eTYPE_PROTO_ID_ITEM);
            if(cprint && pproto_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_method_type = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint && pproto_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_value._method_type\t %05d\t %s\n", ed->_method_type, pproto_ids->as_string(ed->_method_type).c_str());
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eMETHOD_HANDLE  :  //----------------------------------------?????????????????????????????????????????????????????????????????
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eMETHOD_HANDLE;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_method_handle = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._method_handle\t %05d\n", ed->_method_handle);
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eSTRING         :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eSTRING;
            Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
            if(cprint && pstr_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_str_idx = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint && pstr_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_value._str_idx\t %05d\t %s\n", ed->_str_idx, pstr_ids->as_string(ed->_str_idx).c_str());
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eTYPE           :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eTYPE;
            Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
            if(cprint && ptype_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_type_idx = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint && ptype_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_value._type_idx\t %05d\t %s\n", ed->_type_idx, ptype_ids->as_string(ed->_type_idx).c_str());
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
            // if(cprint) Log::print(Log::LOG_DEBUG, deeps, "Encoded_annotation.Annotation--- m_dex[0x%08X] start_offset[0x%08X] offset_revision[0x%08X]\n", m_dex, start_offset, offset_revision);
        }
            // if(cprint) Log::print(Log::LOG_DEBUG, deeps, "Encoded_annotation.Annotation---1 m_dex[0x%08X] start_offset[0x%08X] offset_revision[0x%08X]\n", m_dex, start_offset, offset_revision);
        return start_offset + offset_revision;
        case eFIELD          :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eFIELD;
            Base_dex_reference* pfield_ids = get_reference(eTYPE_FIELD_ID_ITEM);
            if(cprint && pfield_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_field_idx = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint && pfield_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_value._field_idx\t %05d\t %s\n", ed->_field_idx, pfield_ids->as_string(ed->_field_idx).c_str());
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eMETHOD         :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eMETHOD;
            Base_dex_reference* pmethod_ids = get_reference(eTYPE_METHOD_ID_ITEM);
            if(cprint && pmethod_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_method_idx = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint && pmethod_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_value._method_idx\t %05d\n", ed->_method_idx, pmethod_ids->as_string(ed->_method_idx).c_str());
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eENUM           :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eENUM;
            Base_dex_reference* pfield_ids = get_reference(eTYPE_FIELD_ID_ITEM);
            if(cprint && pfield_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_enum = static_cast<u4>( __read_var_width((const u1**)&tmp_dex, arg, false) );
            if(cprint && pfield_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_value._enum\t %05d\t %s\n", ed->_enum, pfield_ids->as_string(ed->_enum).c_str());
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eARRAY          :
        {
            Encoded_array* ea = new Encoded_array();
            ea->type = eARRAY;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ea->_size = readUnsignedLeb128((const u1**)&tmp_dex);
            ea->_data.reserve(ea->_size);
            offset_revision = tmp_dex - (m_dex + start_offset);
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_array.size\t %05d\n", ea->_size);
            for(unsigned int i = 0; i < ea->_size; i++){
                Base_encoded* be = NULL;
                offset_revision = __parssing_encoded_value(start_offset + offset_revision, &be, deeps, cprint) - start_offset;
                ea->_data.push_back(be);
            }
            *encoded = ea;
        }
        return start_offset + offset_revision;
        case eANNOTATION     :
        {
            Encoded_annotation* ean = new Encoded_annotation();
            ean->type = eANNOTATION;
            // if(cprint) Log::print(Log::LOG_INFO, "-----Encoded_Annotation\n");
            Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
            Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
            if(cprint && ptype_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ean->type_idx = readUnsignedLeb128((const u1**)&tmp_dex);
            offset_revision = tmp_dex - (m_dex + start_offset);
            if(cprint && ptype_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_annotation.type_idx\t %05d\t %s\n", ean->type_idx, ptype_ids->as_string(ean->type_idx).c_str());
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ean->_size = readUnsignedLeb128((const u1**)&tmp_dex);
            offset_revision = tmp_dex - (m_dex + start_offset);
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_annotation._size\t %05d\n", ean->_size);
            ean->name_ids.reserve(ean->_size);
            ean->_data.reserve(ean->_size);
            for(unsigned int i = 0; i < ean->_size; i++){
                if(cprint && pstr_ids != NULL) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
                tmp_dex = m_dex + start_offset + offset_revision;
                u4 name_idx = readUnsignedLeb128((const u1**)&tmp_dex);
                offset_revision = tmp_dex - (m_dex + start_offset);
                if(cprint && pstr_ids != NULL) Log::print(Log::LOG_INFO, deeps, "Encoded_annotation.name_idx[%05d]\t %05d\t %s\n", i, name_idx, pstr_ids->as_string(name_idx).c_str());
                ean->name_ids.push_back(name_idx);

                Base_encoded* be = NULL;
                // if(cprint) Log::print(Log::LOG_DEBUG, deeps, "Encoded_annotation.Annotation1 m_dex[0x%08X] start_offset[0x%08X] offset_revision[0x%08X]\n", m_dex, start_offset, offset_revision);
                offset_revision = __parssing_encoded_value(start_offset + offset_revision, &be, deeps, cprint) - start_offset;
                // if(cprint) Log::print(Log::LOG_DEBUG, deeps, "Encoded_annotation.Annotation2 m_dex[0x%08X] start_offset[0x%08X] offset_revision[0x%08X]\n", m_dex, start_offset, offset_revision);
                ean->_data.push_back(be);
            }
            *encoded = ean;
        }
        return start_offset + offset_revision;
        case eNULL           :
        {
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t Encoded_value.type\t eNULL\n", start_offset + offset_revision - 1);
            Encoded_data* ed = new Encoded_data();
            ed->type = eNULL;
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
        }
        return start_offset + offset_revision;
        case eBOOLEAN        :
        {
            Encoded_data* ed = new Encoded_data();
            ed->type = eBOOLEAN;
            if(cprint) Log::print(Log::LOG_INFO, "0x%08X\t ", start_offset + offset_revision);
            ed->_bool = arg;
            *encoded = ed;
            offset_revision = tmp_dex - (m_dex + start_offset);
            if(cprint) Log::print(Log::LOG_INFO, deeps, "Encoded_value._bool\t %05d\n", ed->_bool);
        }
        return start_offset + offset_revision;

    }
    return start_offset + offset_revision;
}

unsigned int Encoded_value::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    offset_revision = __parssing_encoded_value(m_start_offset + offset_revision, &m_value, 0, get_command_flag(eFLAG_LOG)) - m_start_offset;

    return m_start_offset + offset_revision;
}

/*
 * Reads variable width value, possibly sign extended at the last defined byte.
 */
u8 Encoded_value::__read_var_width(const u1** data, u1 arg, bool sign_extend) {
  u8 value = 0;
  for (u4 i = 0; i <= arg; i++) {
    value |= static_cast<u8>(*(*data)++) << (i * 8);
  }
  if (sign_extend) {
    int shift = (7 - arg) * 8;
    return (static_cast<s8>(value) << shift) >> shift;
  }
  return value;
}

unsigned int Encoded_value::parssing_annotation(int deeps, bool cprint){
    unsigned int offset_revision = 0;

    // u1 enc = *((u1*)(m_dex + m_start_offset + offset_revision));
    // offset_revision += sizeof(u1);
    // u1 type = enc & 0x1f;
    u1 type = eANNOTATION;
    // u1 arg = enc >> 5;
    u1 arg = 0;
    // Log::print(Log::LOG_INFO, deeps, "Annotation TYPE :: %d\n", type);

    return __parssing_encoded_value(m_start_offset, type, arg, &m_value, deeps, cprint);
}

unsigned int Encoded_value::parssing_array(int deeps, bool cprint){
    unsigned int offset_revision = 0;

    u1 type = eARRAY;
    u1 arg = 0;

    return __parssing_encoded_value(m_start_offset, type, arg, &m_value, deeps, cprint);
}

std::string Encoded_value::as_annotation_string(){
    if(m_value == NULL){
        Log::print(Log::LOG_ERROR, "None Parssing Error !!\n");
        return std::string("");
    }

    if(m_value->type != eANNOTATION){
        Log::print(Log::LOG_ERROR, "Parssing Annotation Type Error !! Type[%d]\n", m_value->type);
        return std::string("");
    }

    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    if(ptype_ids == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Encoded_value.Annotation reference NULL !\n");
        return std::string("");
    }

    std::string result = ptype_ids->as_string( ((Encoded_annotation*)m_value)->type_idx );

    return result;
}


#endif /*  _IN_ENCODED_VALUE */