#include "leb128.h"
#include "dex_base_parssing.h"
#include "dex_opcodes.h"
#include <vector>
#include <math.h>
#include <string>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_CODE_ITEMS
#define _IN_CODE_ITEMS

class Code_items : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Try_item{
        unsigned int                                    start_addr;
        unsigned short                                  insn_count;             //start_addr + insn_count - 1
        unsigned short                                  handler_off;
    };
    struct Encoded_type_addr_pair{
        ULEB128                                         type_idx;
        ULEB128                                         addr;
    };
    struct Encoded_catch_handler{
        SLEB128                                         size;
        std::vector<Encoded_type_addr_pair>             handlers;              //handlers[abs(size)]
        ULEB128                                         catch_all_addr;         //ULEB128(optional)         size 값이 양수일 경우 존재함
    };
    struct Encoded_catch_handler_list{
        SLEB128                                         size;
        std::vector<Encoded_catch_handler>              list;                   //list[handlers_size]
    };
    struct Code_item{
        unsigned short                                  registers_size;                     //2
        unsigned short                                  ins_size;                   //
        unsigned short                                  outs_size;
        unsigned short                                  tries_size;
        unsigned int                                    debug_info_off;
        unsigned int                                    insns_size;             //instructions
        std::vector<unsigned short>                     insns;                  //insns[insns_size]
        std::vector<Opcode>                             opcodes;                //임시로 만들었음
        unsigned short                                  padding;                 //insns_size 값이 0이 아니며 홀수인 경우만 존재
        std::vector<Try_item>                           tries;                  //try_item[tries_size] tries_size 0이 아닌 경우만 존재
        Encoded_catch_handler_list                      handlers;               //tries_size 0이 아닌 경우만 존재
    };
#pragma pack(pop)
    std::vector<Code_item> m_code_items;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
public:
    Code_items(){init_reference(eTYPE_CODE_ITEM, this);}
    // std::string as_string(unsigned int idx);
};


unsigned int Code_items::__parssing(unsigned int _command_flag){
    m_code_items.reserve(m_offset_count);
    int offset_revision = 0;
    // Log::print(Log::LOG_INFO, "Dex code def Offset[0x%08X] Max_COUNT[ %d ]\n", m_start_offset, m_offset_count);

    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "**Code item Full\n");
    for(unsigned int i = 0; i < m_offset_count; i++){
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "CODE OFFSETS [0x%08X]\n", m_start_offset + offset_revision);

        Code_items::Code_item _code;
        _code.registers_size = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tregisters_size[%d]\n", m_start_offset + offset_revision, _code.registers_size);
        offset_revision += sizeof(u2);
        _code.ins_size = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tins_size[%d]\n", m_start_offset + offset_revision, _code.ins_size);
        offset_revision += sizeof(u2);
        _code.outs_size = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tout_size[%d]\n", m_start_offset + offset_revision, _code.outs_size);
        offset_revision += sizeof(u2);
        _code.tries_size = *((u2*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \ttries_size[%d]\n", m_start_offset + offset_revision, _code.tries_size);
        offset_revision += sizeof(u2);
        _code.debug_info_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tdebug_info_off[0x%08X]\n", m_start_offset + offset_revision, _code.debug_info_off);
        offset_revision += sizeof(u4);
        _code.insns_size = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tinsns_size[%d]\n", m_start_offset + offset_revision, _code.insns_size);
        offset_revision += sizeof(u4);
        _code.insns.reserve(_code.insns_size);  //사이즈 이상의 값을 가지지 못함, PAYLOAD가 있더라도 최대 사이즈를 넘을 수 없음
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "insns==\n\t\t\n");
        Opcode opcode;
        unsigned int tmp_offset_revision = offset_revision;
        unsigned int isns_max_offset = m_start_offset + offset_revision + _code.insns_size * sizeof(u2);
        unsigned int opcode_parssing_offset = m_start_offset + offset_revision;
        while(opcode_parssing_offset != isns_max_offset){
            //m_dex, start_offset, byte count
            opcode.init(m_dex, m_start_offset + tmp_offset_revision, isns_max_offset - (m_start_offset + tmp_offset_revision));
            opcode_parssing_offset = opcode.parssing(get_command_flag());
            tmp_offset_revision = opcode_parssing_offset - m_start_offset;
            if(opcode_parssing_offset > isns_max_offset){
                Log::print(Log::LOG_ERROR, "ERROR !! CODE_ITEM insns Parssing Error !! isns_max_offset[0x%08X] :: opcode_parssing_offset[0x%08X] Over Byte[%d]\n", isns_max_offset, opcode_parssing_offset, opcode_parssing_offset - isns_max_offset);
                break;
            }
        }

        for(unsigned int k = 0; k < _code.insns_size; k++){
            unsigned int tm_of = m_start_offset + offset_revision;
            unsigned short tmp_insns = *((u2*)(m_dex + m_start_offset + offset_revision));
            _code.insns.push_back(tmp_insns);
            offset_revision += sizeof(u2);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "[0x%08X, %04X]  ", tm_of, tmp_insns);
            if(get_command_flag(eFLAG_LOG)) if(k + 1 != _code.insns_size && (k + 1) % 4 == 0) Log::print(Log::LOG_INFO, "\n\t\t");
        }
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
        if(_code.tries_size > 0 && _code.insns_size % 2 == 1){
            _code.padding = *((u2*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tpadding[%04X]\n", m_start_offset + offset_revision, _code.padding);
            offset_revision += sizeof(u2);
        }
        // Log::print(Log::LOG_DEBUG, "END Array OFFSET[0x%08X]\n", m_start_offset + offset_revision);
        if(_code.tries_size > 0){
            _code.tries.reserve(_code.tries_size);
            for(int k = 0; k < _code.tries_size; k++){
                Try_item try_item;
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tTRY_ITEM\n", m_start_offset + offset_revision);
                try_item.start_addr = *((u4*)(m_dex + m_start_offset + offset_revision));
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\tstart_addr[%04X]\n", m_start_offset + offset_revision, try_item.start_addr);
                offset_revision += sizeof(u4);
                try_item.insn_count = *((u2*)(m_dex + m_start_offset + offset_revision));
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\tinsn_count[%04X]\n", m_start_offset + offset_revision, try_item.insn_count);
                offset_revision += sizeof(u2);
                try_item.handler_off = *((u2*)(m_dex + m_start_offset + offset_revision));
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\thandler_off[%04X]\n", m_start_offset + offset_revision, try_item.handler_off);
                offset_revision += sizeof(u2);
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "start_addr[%x], insn_count[%x], handler_off[%x]\n", try_item.start_addr, try_item.insn_count, try_item.handler_off);
                _code.tries.push_back(try_item);
            }

            Encoded_catch_handler_list encoded_catch_handler_list;
            unsigned char* tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_catch_handler_list.size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \tencoded_catch_handler_list.size[%d]\n", m_start_offset + offset_revision, encoded_catch_handler_list.size);
            offset_revision = tmp_dex - m_dex - m_start_offset;

            encoded_catch_handler_list.list.reserve(encoded_catch_handler_list.size);
            for(int k = 0; k < encoded_catch_handler_list.size; k++){
                Encoded_catch_handler encoded_catch_handler;
                tmp_dex = m_dex + m_start_offset + offset_revision;
                encoded_catch_handler.size = readSignedLeb128((const unsigned char**)&tmp_dex);
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\tencoded_catch_handler.size[%d]\n", m_start_offset + offset_revision, encoded_catch_handler.size);
                offset_revision = tmp_dex - m_dex - m_start_offset;
                int handler_size_check = encoded_catch_handler.size <= 0 ? 1 : 0;

                encoded_catch_handler.handlers.reserve(abs(encoded_catch_handler.size));
                for(int n = 0; n < abs(encoded_catch_handler.size); n++){
                    Encoded_type_addr_pair encoded_type_addr_pair;
                    tmp_dex = m_dex + m_start_offset + offset_revision;
                    encoded_type_addr_pair.type_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex);
                    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\t\tencoded_type_addr_pair.type_idx[%d]\n", m_start_offset + offset_revision, encoded_type_addr_pair.type_idx);
                    offset_revision = tmp_dex - m_dex - m_start_offset;
                    tmp_dex = m_dex + m_start_offset + offset_revision;
                    encoded_type_addr_pair.addr = readUnsignedLeb128((const unsigned char**)&tmp_dex);
                    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\t\tencoded_type_addr_pair.addr[%d]\n", m_start_offset + offset_revision, encoded_type_addr_pair.addr);
                    offset_revision = tmp_dex - m_dex - m_start_offset;
                    encoded_catch_handler.handlers.push_back(encoded_type_addr_pair);
                }//for(int n = 0; n < encoded_catch_handler.size; n++)
                if(handler_size_check){
                    tmp_dex = m_dex + m_start_offset + offset_revision;
                    encoded_catch_handler.catch_all_addr = readUnsignedLeb128((const unsigned char**)&tmp_dex);
                    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X \t\tencoded_catch_handler.catch_all_addr[%X]\n", m_start_offset + offset_revision, encoded_catch_handler.catch_all_addr);
                    offset_revision = tmp_dex - m_dex - m_start_offset;
                }
                encoded_catch_handler_list.list.push_back(encoded_catch_handler);
            } //for(int k = 0; k < encoded_catch_handler_list.size; k++)
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
        }

        m_code_items.push_back(_code);

        //padding
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;

    } //for(int i = 0; i < m_offsets_idx_list.size(); i++)
    return m_start_offset + offset_revision;
}

#endif /* __IN_CODE_ITEMS */