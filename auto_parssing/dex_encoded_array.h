
#include "dex_common.h"
#include "dex_base_parssing.h"
#include "dex_encoded_value.h"
#include <vector>




#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ENCODED_ARRAY
#define _IN_ENCODED_ARRAY

class Encoded_array : public Base_dex_parssing, public Base_dex_reference{
private:
    std::vector<Encoded_value>         m_encoded_arrays;
protected:
    unsigned int __parssing(unsigned int _command_flag);
public:
};

unsigned int Encoded_array::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    // Base_dex_reference* pannotation = get_reference(eTYPE_ANNOTATION_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        Log::print(Log::LOG_INFO, "**Encoded_array ----\n");
        Log::print(Log::LOG_INFO, "------------------------------------------------------------------------------------------------------\n");
        // if(pannotation == NULL){
        //     Log::print(Log::LOG_ERROR, "ERROR !! Annotation_set_ref.__parssing() reference NULL !\n");
        //     return m_start_offset;
        // }
    }

    m_encoded_arrays.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        Encoded_value ev;
        ev.init(m_dex, m_start_offset + offset_revision, 1);
        offset_revision = ev.parssing_array(2, get_command_flag(eFLAG_LOG)) - m_start_offset;

        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");


    return m_start_offset + offset_revision;
}


#endif /* _IN_ENCODED_ARRAY */