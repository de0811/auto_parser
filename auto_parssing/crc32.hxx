
/*
#include <windows.h>
#include <stdio.h>
*/

#ifdef WIN32
#else
#include <string.h>
#endif

//**************************************************************************************************************************************************
//	CRC32
//**************************************************************************************************************************************************
#define UPDC32(c, crc) (crc32tab[((int)(crc) ^ (c)) & 0xff] ^ (((crc) >> 8) & 0x00FFFFFFL))


static 
unsigned int crc32tab[] = {
	0x00000000L, 0x77073096L, 0xEE0E612CL, 0x990951BAL, 0x076DC419L, 0x706AF48FL, 0xE963A535L, 0x9E6495A3L,
	0x0EDB8832L, 0x79DCB8A4L, 0xE0D5E91EL, 0x97D2D988L, 0x09B64C2BL, 0x7EB17CBDL, 0xE7B82D07L, 0x90BF1D91L,
	0x1DB71064L, 0x6AB020F2L, 0xF3B97148L, 0x84BE41DEL, 0x1ADAD47DL, 0x6DDDE4EBL, 0xF4D4B551L, 0x83D385C7L,
	0x136C9856L, 0x646BA8C0L, 0xFD62F97AL, 0x8A65C9ECL, 0x14015C4FL, 0x63066CD9L, 0xFA0F3D63L, 0x8D080DF5L,
	0x3B6E20C8L, 0x4C69105EL, 0xD56041E4L, 0xA2677172L, 0x3C03E4D1L, 0x4B04D447L, 0xD20D85FDL, 0xA50AB56BL,
	0x35B5A8FAL, 0x42B2986CL, 0xDBBBC9D6L, 0xACBCF940L, 0x32D86CE3L, 0x45DF5C75L, 0xDCD60DCFL, 0xABD13D59L,
	0x26D930ACL, 0x51DE003AL, 0xC8D75180L, 0xBFD06116L, 0x21B4F4B5L, 0x56B3C423L, 0xCFBA9599L, 0xB8BDA50FL,
	0x2802B89EL, 0x5F058808L, 0xC60CD9B2L, 0xB10BE924L, 0x2F6F7C87L, 0x58684C11L, 0xC1611DABL, 0xB6662D3DL,
	0x76DC4190L, 0x01DB7106L, 0x98D220BCL, 0xEFD5102AL, 0x71B18589L, 0x06B6B51FL, 0x9FBFE4A5L, 0xE8B8D433L,
	0x7807C9A2L, 0x0F00F934L, 0x9609A88EL, 0xE10E9818L, 0x7F6A0DBBL, 0x086D3D2DL, 0x91646C97L, 0xE6635C01L,
	0x6B6B51F4L, 0x1C6C6162L, 0x856530D8L, 0xF262004EL, 0x6C0695EDL, 0x1B01A57BL, 0x8208F4C1L, 0xF50FC457L,
	0x65B0D9C6L, 0x12B7E950L, 0x8BBEB8EAL, 0xFCB9887CL, 0x62DD1DDFL, 0x15DA2D49L, 0x8CD37CF3L, 0xFBD44C65L,
	0x4DB26158L, 0x3AB551CEL, 0xA3BC0074L, 0xD4BB30E2L, 0x4ADFA541L, 0x3DD895D7L, 0xA4D1C46DL, 0xD3D6F4FBL,
	0x4369E96AL, 0x346ED9FCL, 0xAD678846L, 0xDA60B8D0L, 0x44042D73L, 0x33031DE5L, 0xAA0A4C5FL, 0xDD0D7CC9L,
	0x5005713CL, 0x270241AAL, 0xBE0B1010L, 0xC90C2086L, 0x5768B525L, 0x206F85B3L, 0xB966D409L, 0xCE61E49FL,
	0x5EDEF90EL, 0x29D9C998L, 0xB0D09822L, 0xC7D7A8B4L, 0x59B33D17L, 0x2EB40D81L, 0xB7BD5C3BL, 0xC0BA6CADL,
	0xEDB88320L, 0x9ABFB3B6L, 0x03B6E20CL, 0x74B1D29AL, 0xEAD54739L, 0x9DD277AFL, 0x04DB2615L, 0x73DC1683L,
	0xE3630B12L, 0x94643B84L, 0x0D6D6A3EL, 0x7A6A5AA8L, 0xE40ECF0BL, 0x9309FF9DL, 0x0A00AE27L, 0x7D079EB1L,
	0xF00F9344L, 0x8708A3D2L, 0x1E01F268L, 0x6906C2FEL, 0xF762575DL, 0x806567CBL, 0x196C3671L, 0x6E6B06E7L,
	0xFED41B76L, 0x89D32BE0L, 0x10DA7A5AL, 0x67DD4ACCL, 0xF9B9DF6FL, 0x8EBEEFF9L, 0x17B7BE43L, 0x60B08ED5L,
	0xD6D6A3E8L, 0xA1D1937EL, 0x38D8C2C4L, 0x4FDFF252L, 0xD1BB67F1L, 0xA6BC5767L, 0x3FB506DDL, 0x48B2364BL,
	0xD80D2BDAL, 0xAF0A1B4CL, 0x36034AF6L, 0x41047A60L, 0xDF60EFC3L, 0xA867DF55L, 0x316E8EEFL, 0x4669BE79L,
	0xCB61B38CL, 0xBC66831AL, 0x256FD2A0L, 0x5268E236L, 0xCC0C7795L, 0xBB0B4703L, 0x220216B9L, 0x5505262FL,
	0xC5BA3BBEL, 0xB2BD0B28L, 0x2BB45A92L, 0x5CB36A04L, 0xC2D7FFA7L, 0xB5D0CF31L, 0x2CD99E8BL, 0x5BDEAE1DL,
	0x9B64C2B0L, 0xEC63F226L, 0x756AA39CL, 0x026D930AL, 0x9C0906A9L, 0xEB0E363FL, 0x72076785L, 0x05005713L,
	0x95BF4A82L, 0xE2B87A14L, 0x7BB12BAEL, 0x0CB61B38L, 0x92D28E9BL, 0xE5D5BE0DL, 0x7CDCEFB7L, 0x0BDBDF21L,
	0x86D3D2D4L, 0xF1D4E242L, 0x68DDB3F8L, 0x1FDA836EL, 0x81BE16CDL, 0xF6B9265BL, 0x6FB077E1L, 0x18B74777L,
	0x88085AE6L, 0xFF0F6A70L, 0x66063BCAL, 0x11010B5CL, 0x8F659EFFL, 0xF862AE69L, 0x616BFFD3L, 0x166CCF45L,
	0xA00AE278L, 0xD70DD2EEL, 0x4E048354L, 0x3903B3C2L, 0xA7672661L, 0xD06016F7L, 0x4969474DL, 0x3E6E77DBL,
	0xAED16A4AL, 0xD9D65ADCL, 0x40DF0B66L, 0x37D83BF0L, 0xA9BCAE53L, 0xDEBB9EC5L, 0x47B2CF7FL, 0x30B5FFE9L,
	0xBDBDF21CL, 0xCABAC28AL, 0x53B39330L, 0x24B4A3A6L, 0xBAD03605L, 0xCDD70693L, 0x54DE5729L, 0x23D967BFL,
	0xB3667A2EL, 0xC4614AB8L, 0x5D681B02L, 0x2A6F2B94L, 0xB40BBE37L, 0xC30C8EA1L, 0x5A05DF1BL, 0x2D02EF8DL
};

unsigned int crc32file(unsigned int *crc32, char *fname)
{
	unsigned int size = 0;
	unsigned int crc = 0;
	FILE *fp;

	crc ^= 0xffffffffL;

	fp = fopen(fname, "rb");
	if(!fp)
	{
		*crc32 = 0;
		return 0;
	}

 	while(1)
	{
		char buf[0x2000] = {0};
		int len;
		int i;

		len = fread(buf, 1, sizeof(buf), fp);
		if(len > 0)
		{
			for(i=0; i<len; i++)
			{
				crc = UPDC32(buf[i], crc);
			}
			size += len;
		}
		else
		{
			break;
		}
	}
	*crc32 = (crc ^ 0xffffffffL);

	fclose(fp);

    return size;
}

unsigned int crc32b(char *buf, int len)
{
	unsigned int crc32 = 0;

	int i;

	crc32 ^= 0xffffffffL;

	for(i=0; i<len; i++)
	{
		crc32 = UPDC32(buf[i], crc32);
	}

	return
		(crc32 ^ 0xffffffffL);
}

unsigned int crc32buffer_ex(unsigned int crc32, unsigned int crc32_ex)
{
	unsigned int i;

	crc32_ex ^= 0xffffffffL;

	for(i=1; i<0xffffffff; i++)
	{
		unsigned int xcrc;
		unsigned int j;
		char buf[4];

		xcrc = crc32_ex;
		memcpy(buf, (char *)&i, 4);

		for(j=0; j<4; j++)
		{
			xcrc = UPDC32(buf[j], xcrc);
		}

		xcrc ^= 0xffffffffL;
		if(xcrc == crc32)
		{
//			printf("== %8.8x: %8.8x, %8.8x\n", i, crc32, xcrc);
			break;
		}

		if((i % 0x01000000) == 0)
		{
//			printf("-- %8.8x: %8.8x, %8.8x\n", i, crc32, xcrc);
		}
	}

	return i;
}

//**************************************************************************************************************************************************
//	adler32
//**************************************************************************************************************************************************
unsigned int update_adler32(unsigned int adler, unsigned char *buf, int len)
{
    unsigned int x1 = adler & 0xffff;
    unsigned int x2 = (adler >> 16) & 0xffff;
    int n;

//	#define BASE 65521 = 0xfff1 /* largest prime smaller than 65536 */

    for(n=0; n<len; n++)
	{
        x1 = (x1 + buf[n]) % 0xfff1;	//BASE;
        x2 = (x2 + x1)     % 0xfff1;	//BASE;
    }
    return (x2 << 16) + x1;
}

unsigned int adler32(char *buf, int len)
{
    return update_adler32(1L, (unsigned char *)buf, len);
}

//**************************************************************************************************************************************************
//	SHA1
//**************************************************************************************************************************************************
class SHA1
{
	public:

		SHA1();
		virtual ~SHA1();

		/*
		 *	Re-initialize the class
		 */
		void Reset();

		/*
		 *	Returns the message digest
		 */
		bool Result(unsigned *message_digest_array);

		/*
		 *	Provide input to SHA1
		 */
		void Input(	const unsigned char	*message_array,
					unsigned			length);
		void Input(	const char	*message_array,
					unsigned	length);
		void Input(unsigned char message_element);
		void Input(char message_element);
		SHA1& operator<<(const char *message_array);
		SHA1& operator<<(const unsigned char *message_array);
		SHA1& operator<<(const char message_element);
		SHA1& operator<<(const unsigned char message_element);

	private:

		/*
		 *	Process the next 512 bits of the message
		 */
		void ProcessMessageBlock();

		/*
		 *	Pads the current message block to 512 bits
		 */
		void PadMessage();

		/*
		 *	Performs a circular left shift operation
		 */
		inline unsigned CircularShift(int bits, unsigned word);

		unsigned H[5];						// Message digest buffers

		unsigned Length_Low;				// Message length in bits
		unsigned Length_High;				// Message length in bits

		unsigned char Message_Block[64];	// 512-bit message blocks
		int Message_Block_Index;			// Index into message block array

		bool Computed;						// Is the digest computed?
		bool Corrupted;						// Is the message digest corruped?
	
};

SHA1::SHA1()
{
	Reset();
}

/*	
 *	~SHA1
 *
 *	Description:
 *		This is the destructor for the sha1 class
 *
 *	Parameters:
 *		None.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
SHA1::~SHA1()
{
	// The destructor does nothing
}

/*	
 *	Reset
 *
 *	Description:
 *		This function will initialize the sha1 class member variables
 *		in preparation for computing a new message digest.
 *
 *	Parameters:
 *		None.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
void SHA1::Reset()
{
	Length_Low			= 0;
	Length_High			= 0;
	Message_Block_Index	= 0;

	H[0]		= 0x67452301;
	H[1]		= 0xEFCDAB89;
	H[2]		= 0x98BADCFE;
	H[3]		= 0x10325476;
	H[4]		= 0xC3D2E1F0;

	Computed	= false;
	Corrupted	= false;
}

/*	
 *	Result
 *
 *	Description:
 *		This function will return the 160-bit message digest into the
 *		array provided.
 *
 *	Parameters:
 *		message_digest_array: [out]
 *			This is an array of five unsigned integers which will be filled
 *			with the message digest that has been computed.
 *
 *	Returns:
 *		True if successful, false if it failed.
 *
 *	Comments:
 *
 */
bool SHA1::Result(unsigned *message_digest_array)
{
	int i;									// Counter

	if (Corrupted)
	{
		return false;
	}

	if (!Computed)
	{
		PadMessage();
		Computed = true;
	}

	for(i = 0; i < 5; i++)
	{
		message_digest_array[i] = H[i];
	}

	return true;
}

/*	
 *	Input
 *
 *	Description:
 *		This function accepts an array of octets as the next portion of
 *		the message.
 *
 *	Parameters:
 *		message_array: [in]
 *			An array of characters representing the next portion of the
 *			message.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
void SHA1::Input(	const unsigned char	*message_array,
					unsigned 			length)
{
	if (!length)
	{
		return;
	}

	if (Computed || Corrupted)
	{
		Corrupted = true;
		return;
	}

	while(length-- && !Corrupted)
	{
		Message_Block[Message_Block_Index++] = (*message_array & 0xFF);

		Length_Low += 8;
		Length_Low &= 0xFFFFFFFF;				// Force it to 32 bits
		if (Length_Low == 0)
		{
			Length_High++;
			Length_High &= 0xFFFFFFFF;			// Force it to 32 bits
			if (Length_High == 0)
			{
				Corrupted = true;				// Message is too long
			}
		}

		if (Message_Block_Index == 64)
		{
			ProcessMessageBlock();
		}

		message_array++;
	}
}

/*	
 *	Input
 *
 *	Description:
 *		This function accepts an array of octets as the next portion of
 *		the message.
 *
 *	Parameters:
 *		message_array: [in]
 *			An array of characters representing the next portion of the
 *			message.
 *		length: [in]
 *			The length of the message_array
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
void SHA1::Input(	const char	*message_array,
					unsigned 	length)
{
	Input((unsigned char *) message_array, length);
}

/*	
 *	Input
 *
 *	Description:
 *		This function accepts a single octets as the next message element.
 *
 *	Parameters:
 *		message_element: [in]
 *			The next octet in the message.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
void SHA1::Input(unsigned char message_element)
{
	Input(&message_element, 1);
}

/*	
 *	Input
 *
 *	Description:
 *		This function accepts a single octet as the next message element.
 *
 *	Parameters:
 *		message_element: [in]
 *			The next octet in the message.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
void SHA1::Input(char message_element)
{
	Input((unsigned char *) &message_element, 1);
}

/*	
 *	operator<<
 *
 *	Description:
 *		This operator makes it convenient to provide character strings to
 *		the SHA1 object for processing.
 *
 *	Parameters:
 *		message_array: [in]
 *			The character array to take as input.
 *
 *	Returns:
 *		A reference to the SHA1 object.
 *
 *	Comments:
 *		Each character is assumed to hold 8 bits of information.
 *
 */
SHA1& SHA1::operator<<(const char *message_array)
{
	const char *p = message_array;

	while(*p)
	{
		Input(*p);
		p++;
	}

	return *this;
}

/*	
 *	operator<<
 *
 *	Description:
 *		This operator makes it convenient to provide character strings to
 *		the SHA1 object for processing.
 *
 *	Parameters:
 *		message_array: [in]
 *			The character array to take as input.
 *
 *	Returns:
 *		A reference to the SHA1 object.
 *
 *	Comments:
 *		Each character is assumed to hold 8 bits of information.
 *
 */
SHA1& SHA1::operator<<(const unsigned char *message_array)
{
	const unsigned char *p = message_array;

	while(*p)
	{
		Input(*p);
		p++;
	}

	return *this;
}

/*	
 *	operator<<
 *
 *	Description:
 *		This function provides the next octet in the message.
 *
 *	Parameters:
 *		message_element: [in]
 *			The next octet in the message
 *
 *	Returns:
 *		A reference to the SHA1 object.
 *
 *	Comments:
 *		The character is assumed to hold 8 bits of information.
 *
 */
SHA1& SHA1::operator<<(const char message_element)
{
	Input((unsigned char *) &message_element, 1);

	return *this;
}

/*	
 *	operator<<
 *
 *	Description:
 *		This function provides the next octet in the message.
 *
 *	Parameters:
 *		message_element: [in]
 *			The next octet in the message
 *
 *	Returns:
 *		A reference to the SHA1 object.
 *
 *	Comments:
 *		The character is assumed to hold 8 bits of information.
 *
 */
SHA1& SHA1::operator<<(const unsigned char message_element)
{
	Input(&message_element, 1);

	return *this;
}

/*	
 *	ProcessMessageBlock
 *
 *	Description:
 *		This function will process the next 512 bits of the message
 *		stored in the Message_Block array.
 *
 *	Parameters:
 *		None.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *		Many of the variable names in this function, especially the single
 *	 	character names, were used because those were the names used
 *	  	in the publication.
 *
 */
void SHA1::ProcessMessageBlock()
{
	const unsigned K[] = 	{ 				// Constants defined for SHA-1
								0x5A827999,
								0x6ED9EBA1,
								0x8F1BBCDC,
								0xCA62C1D6
							};
	int 		t;							// Loop counter
	unsigned 	temp;						// Temporary word value
	unsigned	W[80];						// Word sequence
	unsigned	A, B, C, D, E;				// Word buffers

	/*
	 *	Initialize the first 16 words in the array W
	 */
	for(t = 0; t < 16; t++)
	{
		W[t] = ((unsigned) Message_Block[t * 4]) << 24;
		W[t] |= ((unsigned) Message_Block[t * 4 + 1]) << 16;
		W[t] |= ((unsigned) Message_Block[t * 4 + 2]) << 8;
		W[t] |= ((unsigned) Message_Block[t * 4 + 3]);
	}

	for(t = 16; t < 80; t++)
	{
	   W[t] = CircularShift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);
	}

	A = H[0];
	B = H[1];
	C = H[2];
	D = H[3];
	E = H[4];

	for(t = 0; t < 20; t++)
	{
		temp = CircularShift(5,A) + ((B & C) | ((~B) & D)) + E + W[t] + K[0];
		temp &= 0xFFFFFFFF;
		E = D;
		D = C;
		C = CircularShift(30,B);
		B = A;
		A = temp;
	}

	for(t = 20; t < 40; t++)
	{
		temp = CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[1];
		temp &= 0xFFFFFFFF;
		E = D;
		D = C;
		C = CircularShift(30,B);
		B = A;
		A = temp;
	}

	for(t = 40; t < 60; t++)
	{
		temp = CircularShift(5,A) +
		 	   ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
		temp &= 0xFFFFFFFF;
		E = D;
		D = C;
		C = CircularShift(30,B);
		B = A;
		A = temp;
	}

	for(t = 60; t < 80; t++)
	{
		temp = CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[3];
		temp &= 0xFFFFFFFF;
		E = D;
		D = C;
		C = CircularShift(30,B);
		B = A;
		A = temp;
	}

	H[0] = (H[0] + A) & 0xFFFFFFFF;
	H[1] = (H[1] + B) & 0xFFFFFFFF;
	H[2] = (H[2] + C) & 0xFFFFFFFF;
	H[3] = (H[3] + D) & 0xFFFFFFFF;
	H[4] = (H[4] + E) & 0xFFFFFFFF;

	Message_Block_Index = 0;
}

/*	
 *	PadMessage
 *
 *	Description:
 *		According to the standard, the message must be padded to an even
 *		512 bits.  The first padding bit must be a '1'.  The last 64 bits
 *		represent the length of the original message.  All bits in between
 *		should be 0.  This function will pad the message according to those
 *		rules by filling the message_block array accordingly.  It will also
 *		call ProcessMessageBlock() appropriately.  When it returns, it
 *		can be assumed that the message digest has been computed.
 *
 *	Parameters:
 *		None.
 *
 *	Returns:
 *		Nothing.
 *
 *	Comments:
 *
 */
void SHA1::PadMessage()
{
	/*
	 *	Check to see if the current message block is too small to hold
	 *	the initial padding bits and length.  If so, we will pad the
	 *	block, process it, and then continue padding into a second block.
	 */
	if (Message_Block_Index > 55)
	{
		Message_Block[Message_Block_Index++] = 0x80;
		while(Message_Block_Index < 64)
		{
			Message_Block[Message_Block_Index++] = 0;
		}

		ProcessMessageBlock();

		while(Message_Block_Index < 56)
		{
			Message_Block[Message_Block_Index++] = 0;
		}
	}
	else
	{
		Message_Block[Message_Block_Index++] = 0x80;
		while(Message_Block_Index < 56)
		{
			Message_Block[Message_Block_Index++] = 0;
		}

	}

	/*
	 *	Store the message length as the last 8 octets
	 */
	Message_Block[56] = (Length_High >> 24) & 0xFF;
	Message_Block[57] = (Length_High >> 16) & 0xFF;
	Message_Block[58] = (Length_High >> 8) & 0xFF;
	Message_Block[59] = (Length_High) & 0xFF;
	Message_Block[60] = (Length_Low >> 24) & 0xFF;
	Message_Block[61] = (Length_Low >> 16) & 0xFF;
	Message_Block[62] = (Length_Low >> 8) & 0xFF;
	Message_Block[63] = (Length_Low) & 0xFF;

	ProcessMessageBlock();
}


/*	
 *	CircularShift
 *
 *	Description:
 *		This member function will perform a circular shifting operation.
 *
 *	Parameters:
 *		bits: [in]
 *			The number of bits to shift (1-31)
 *		word: [in]
 *			The value to shift (assumes a 32-bit integer)
 *
 *	Returns:
 *		The shifted value.
 *
 *	Comments:
 *
 */
unsigned SHA1::CircularShift(int bits, unsigned word)
{
	return ((word << bits) & 0xFFFFFFFF) | ((word & 0xFFFFFFFF) >> (32-bits));
}

int GetSHA1Buffer(char *szHashCode, char *buffer, int len)
{
	unsigned int dgt[5] = {0};

	SHA1 sha1;
	sha1.Reset();
	sha1.Input(buffer, len);
	if(sha1.Result(dgt))
	{
		char szHashCodeEx[64] = {0};
		int i;

		sprintf(szHashCodeEx, "%08x%08x%08x%08x%08x", dgt[0], dgt[1], dgt[2], dgt[3], dgt[4]);

		for(i=0; i<20; i++)
		{
			int j;

			j = 20 - i - 1;
			szHashCodeEx[j*2+2] = 0;
			szHashCode[20-i-1] = (char)strtoul(szHashCodeEx+(j*2), 0, 16);
		}
		return 1;
	}

	return 0;
}
