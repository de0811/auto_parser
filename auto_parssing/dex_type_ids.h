#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_TYPE_IDS
#define _IN_TYPE_IDS

class Type_ids : public Base_dex_parssing, public Base_dex_reference{
private:
    std::vector<unsigned int>   m_type_ids;
protected:
    unsigned int __parssing(unsigned int _command_flag);
public:
    Type_ids(){init_reference(eTYPE_TYPE_ID_ITEM, this);}
    //string_ids 의 모든 정보를 출력
    std::string as_string(unsigned int idx){
        //if( (int)idx == -1 ) return (unsigned char*)"NO_INDEX";
        // if( m_type_ids.size() < idx || m_string_ids == NULL ) return std::string("");
        if(m_type_ids.size() <= idx || idx < 0){
            Log::print(Log::LOG_ERROR, "ERROR ! Type_ids.as_string(idx[%d])\n", idx);
        }
        unsigned int str_idx = m_type_ids[idx];
        return get_reference(eTYPE_STRING_ID_ITEM)->as_string(str_idx);
    }

};

unsigned int Type_ids::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    if(pstr_ids == NULL){
        Log::print(Log::LOG_ERROR, "TYPE_IDS sub Reference ERROR !!");
        return m_start_offset;
    }
    if(get_command_flag(eFLAG_LOG)){
        // Log::print(Log::LOG_INFO, "**Type ids Full\n");
        printf("**Type ids Full\n");
        // Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t STRING_IDX\t STRING\n");
        printf("OFFSET\t\t COUNT\t STRING_IDX\t STRING\n");
        // Log::print(Log::LOG_INFO, "-------------------------------------------------------------------------\n");
        printf("-------------------------------------------------------------------------\n");
    } 

    unsigned int tmp_type_id = 0;
    for(unsigned int i = 0; i < m_offset_count; i++){
        tmp_type_id = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t %05d\t\t %s\n", m_start_offset + offset_revision, i, tmp_type_id, pstr_ids->as_string(tmp_type_id).c_str());
        offset_revision += sizeof(u4);
        m_type_ids.push_back(tmp_type_id);
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t %05d\t\t %s\n", m_start_offset + offset_revision, i, tmp_type_id, pstr_ids->as_string(tmp_type_id).c_str());
    }
    // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n\n");   
    if(get_command_flag(eFLAG_LOG)) printf("\n\n");   
    return m_start_offset + offset_revision;
}

#endif  /* _IN_TYPE_IDS */