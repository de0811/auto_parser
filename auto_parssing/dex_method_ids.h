#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_METHOD_IDS
#define _IN_METHOD_IDS

class Method_ids : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Method_id_item{

                u2          class_idx;                  //2
                u2          proto_idx;                  //4
                u4            name_idx;                   //8
    };
#pragma pack(pop)
    std::vector<Method_id_item>                 m_method_ids;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);

public:
    Method_ids(){init_reference(eTYPE_METHOD_ID_ITEM, this);}
    virtual std::string as_string(unsigned int idx);
};


unsigned int Method_ids::__parssing(unsigned int _command_flag){
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    Base_dex_reference* pproto_ids = get_reference(eTYPE_PROTO_ID_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        if(pstr_ids == NULL || ptype_ids == NULL || pproto_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Method_ids print_full() reference NULL\n");
            return m_start_offset;
        }
        // Log::print(Log::LOG_INFO, "**Method ids Full\n");
        printf("**Method ids Full\n");
        // Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t IDX\t STRING\n");
        printf("OFFSET\t\t COUNT\t NAME\t\t IDX\t STRING\n");
        // Log::print(Log::LOG_INFO, "-----------------------------------------------------------------------------------------\n");
        printf("-----------------------------------------------------------------------------------------\n");
    }

    int offset_revision = 0;

    m_method_ids.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        Method_ids::Method_id_item method;
        method.class_idx = *((u2*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t class_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method.class_idx, ptype_ids->as_string(method.class_idx).c_str());
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t class_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method.class_idx, ptype_ids->as_string(method.class_idx).c_str());
        offset_revision += sizeof(u2);
        method.proto_idx = *((u2*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t proto_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method.proto_idx, pproto_ids->as_string(method.proto_idx).c_str());
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t proto_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method.proto_idx, pproto_ids->as_string(method.proto_idx).c_str());
        offset_revision += sizeof(u2);
        method.name_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t name_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method.name_idx, pstr_ids->as_string(method.name_idx).c_str());
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t name_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method.name_idx, pstr_ids->as_string(method.name_idx).c_str());
        offset_revision += sizeof(u4);

        m_method_ids.push_back(method);
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
        if(get_command_flag(eFLAG_LOG)) printf("\n");
    }
    if(get_command_flag(eFLAG_LOG)) printf("\n\n");   
    return m_start_offset + offset_revision;
}


std::string Method_ids::as_string(unsigned int idx){
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    Base_dex_reference* pproto_ids = get_reference(eTYPE_PROTO_ID_ITEM);
    if(pstr_ids == NULL || ptype_ids == NULL || pproto_ids == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Method_ids.as_string(%d) reference is NULL\n", idx);
        return std::string("");
    }
    if( m_method_ids.size() <= idx ){
        Log::print(Log::LOG_ERROR, "ERROR !! Method_ids.as_string(%d) idx Over\n", idx);
        return std::string("");
    }
    std::string result("");
    result.append( ptype_ids->as_string(m_method_ids[idx].class_idx) );
    result.append("->");
    result.append( pstr_ids->as_string(m_method_ids[idx].name_idx) );
    result.append( pproto_ids->as_string(m_method_ids[idx].proto_idx) );
    return result;
}

#endif  /* _IN_METHOD_IDS */