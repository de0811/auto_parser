#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>
#include <list>
#include <string>
#include "leb128.h"
#include "dex_type_ids.h"

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_TYPE_LIST
#define _IN_TYPE_LIST

//UTF-16 사용

class Type_lists : public Base_dex_parssing, public Base_dex_reference{
private:
    struct Type_list{
        u4                     size;
        std::vector<u2>        list;
    };
    //해당 offset이 몇번째 값인지 확인하기 위해서 저장
    std::vector<unsigned int>       m_type_offsets;
    //type_list 의 모든 데이터
    std::vector<Type_list>          m_type_lists;
    //변조 되기 이전의 값을 저장 __ 변조가 없다면 존재하지 않음
    std::vector<unsigned int>       m_origin_type_offsets;
    //어떤 offset 때문에 바뀌엇는지 저장해야할듯
    std::list<unsigned int>         m_convert_type_offsets;

    virtual unsigned int __parssing(unsigned int _command_flag);
    unsigned int __convert();
public:
    Type_lists(){init_reference(eTYPE_TYPE_LIST, this);}
    //string_ids 의 모든 정보를 출력
    void print_full();
    //해당하는 index 값에 맞는 문자열을 반환
    virtual std::string as_string(unsigned int idx);
    //offset 값에 해당하는 문자열을 반환
    virtual std::string as_off_string(unsigned int off);
    //limit_size를 넘어서는 값은 강제로 limit_size의 크기로 값을 조정함
    void convert(u4 limit_size);
    //변경된 offset에 대칭되는 offset을 반환
    unsigned int get_symmetry_offset(unsigned int off);
    //어떤 offset이 바뀌게 되었는지 반환
    std::list<unsigned int> get_convert_offsets(){ return m_convert_type_offsets; }
};

unsigned int Type_lists::get_symmetry_offset(unsigned int off){
    bool cCheck = false;
    unsigned int off_idx = 0;
    if(off <= 0){
        return off;
    }
    for(off_idx = 0; off_idx < m_origin_type_offsets.size(); off_idx++){
        if( m_origin_type_offsets[off_idx] == off){
            cCheck = true;
            break;
        }
    }
    if(cCheck == false){
        Log::print(Log::LOG_ERROR, "ERROR !! Type_lists.get_symmetry_offset(%d) None Find idx\n", off);
        return -1;
    }
    else{
        if(m_type_offsets.size() <= off_idx){
            Log::print(Log::LOG_ERROR, "ERROR !! Type_lists.get_symmetry_offset(%d) over_idx\n", off);
            return -1;
        }
    }
    return m_type_offsets[off_idx];
}

unsigned int Type_lists::__parssing(unsigned int _command_flag){
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);

    if(get_command_flag(eFLAG_LOG)){
        if(ptype_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR!! Type_lists.print_full() None ptype_ids\n");
            return m_start_offset;
        }
        // Log::print(Log::LOG_INFO, "**type list Full\n");
        printf("**type list Full\n");
        // Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t\t\t SIZE/OFF\t STRING\n");
        printf("OFFSET\t\t COUNT\t NAME\t\t\t\t\t\t SIZE/OFF\t STRING\n");
        // Log::print(Log::LOG_INFO, "---------------------------------------------------------------------------------------\n");
        printf("---------------------------------------------------------------------------------------\n");
    }

    int offset_revision = 0;
    m_type_lists.reserve(m_offset_count);
    m_type_offsets.reserve(m_offset_count);

    for(unsigned int i = 0; i < m_offset_count; i++){
        Type_list type_list;
        type_list.size = *((u4*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t type_list.size          \t %d\n", m_start_offset + offset_revision, i, type_list.size);
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t type_list.size          \t %d\n", m_start_offset + offset_revision, i, type_list.size);
        m_type_offsets.push_back(m_start_offset + offset_revision);
        offset_revision += sizeof(u4);
        
        type_list.list.reserve(type_list.size);
        for(unsigned int k = 0; k < type_list.size; k++){
            u2 type_idx;
            type_idx = *((u2*)(m_dex + m_start_offset + offset_revision));
            // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t type_list.list.type_idx\t %04d\t\t %s\n", m_start_offset + offset_revision, i, type_idx, ptype_ids->as_string(type_idx).c_str());
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t type_list.list.type_idx\t %04d\t\t %s\n", m_start_offset + offset_revision, i, type_idx, ptype_ids->as_string(type_idx).c_str());
            offset_revision += sizeof(u2);
            type_list.list.push_back(type_idx);
        }
        m_type_lists.push_back(type_list);
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
        if(get_command_flag(eFLAG_LOG)) printf("\n");
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
    }

    return m_start_offset + offset_revision;
}

unsigned int Type_lists::__convert(){
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);

    if(get_command_flag(eFLAG_LOG)){
        if(ptype_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR!! Type_lists.print_full() None ptype_ids\n");
            return m_start_offset;
        }
        // Log::print(Log::LOG_INFO, "**type list Full\n");
        printf("**Convert type list Full\n");
        // Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t\t\t SIZE/OFF\t STRING\n");
        printf("OFFSET\t\t COUNT\t NAME\t\t\t\t\t\t SIZE/OFF\t STRING\n");
        // Log::print(Log::LOG_INFO, "---------------------------------------------------------------------------------------\n");
        printf("---------------------------------------------------------------------------------------\n");
    }

    int offset_revision = 0;
    // m_type_lists.reserve(m_offset_count);
    m_type_offsets.reserve(m_offset_count);
    m_type_offsets.clear();

    // for(unsigned int i = 0; i < m_offset_count; i++){
    for(unsigned int i = 0; i < m_type_lists.size(); i++){
        Type_list &type_list = m_type_lists[i];
        *((u4*)(m_dex + m_start_offset + offset_revision)) = type_list.size;
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t type_list.size          \t %d\n", m_start_offset + offset_revision, i, type_list.size);
        m_type_offsets.push_back(m_start_offset + offset_revision);
        offset_revision += sizeof(u4);
        
        // type_list.list.reserve(type_list.size);
        for(unsigned int k = 0; k < type_list.size; k++){
            u2 &type_idx = type_list.list[k];
            *((u2*)(m_dex + m_start_offset + offset_revision)) = type_idx;
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %05d\t type_list.list.type_idx\t %04d\t\t %s\n", m_start_offset + offset_revision, i, type_idx, ptype_ids->as_string(type_idx).c_str());
            offset_revision += sizeof(u2);
            // type_list.list.push_back(type_idx);
        }
        // m_type_lists.push_back(type_list);
        if(get_command_flag(eFLAG_LOG)) printf("\n");
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
    }

    return m_start_offset + offset_revision;
}

std::string Type_lists::as_string(unsigned int idx){
    std::string type_list_str;
        // std::string tmp_string = get_reference(eTYPE_STRING_ID_ITEM)->as_string(tmp_type_id);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    if(m_type_lists.size() <= idx || ptype_ids == NULL){
        // Log::print(Log::LOG_ERROR, "ERROR!! Type_lists.as_string(idx[%d])\n", idx);
        printf("ERROR!! Type_lists.as_string(idx[%d])\n", idx);
        return std::string("");
    }
    for( unsigned int i = 0; i < m_type_lists[idx].size; i++ ){
        type_list_str.append(  ptype_ids->as_string(m_type_lists[idx].list[i])  );
    }

    return type_list_str;
}

std::string Type_lists::as_off_string(unsigned int off){
    bool check = false;
    if(off == 0) return std::string("");
    unsigned int i = 0;
    for(i = 0; i < m_type_offsets.size(); i++){
        if( m_type_offsets[i] == off ){
            check = true;
            break;
        }
    }
    if(check){
        return as_string(i);
    }
    else{
        Log::print(Log::LOG_ERROR, "ERROR !! Annotation(0x%08X) None Offset !\n", off);
        return std::string("");
    }
}


void Type_lists::convert(u4 limit_size){
    if(! get_command_flag(eFLAG_NORMALIZATION)){
        return;
    }

    for(unsigned int i = 0; i < m_type_lists.size(); i++){
        Log::print(Log::LOG_DEBUG, "__DD__CONVERT__LOG 0x%08X --- size[%d]\n", m_type_offsets[i], m_type_lists[i].size);
        if(m_type_lists[i].size > limit_size){
            m_convert_type_offsets.push_back(m_type_offsets[i]);
            m_type_lists[i].size = limit_size;
            Log::print(Log::LOG_DEBUG, "__DD__CONVERT 0x%08X\n", m_type_offsets[i]);
        }
    }

    if(m_convert_type_offsets.empty()){
        return;
    }

    // m_convert_type_offsets.sort(std::greater<unsigned int>());

    //다른데서 offset 이거였는데 뭘로 바꼇냐고 물어보면 대답해주기 위해 이전의 offset의 위치를 저장해 둔다
    m_origin_type_offsets = m_type_offsets;
    __convert();
}

#endif  /* _IN_TYPE_LIST */