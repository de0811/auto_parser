#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_BASE_REFERENCE
#define _IN_BASE_REFERENCE

#include <string>

class Base_dex_reference{
public:
    //해당하는 index 값에 맞는 문자열을 반환
    virtual std::string as_string(unsigned int idx) {return std::string("");}
    //offset 값에 해당하는 문자열을 반환
    virtual std::string as_off_string(unsigned int off){return std::string("");}
};

#endif  /* _IN_BASE_REFERENCE */