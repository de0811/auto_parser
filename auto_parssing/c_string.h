

#ifndef _IN_STRING
#define _IN_STRING

#ifdef  __cplusplus
extern "C" {
#endif


#ifndef IN
#define IN  
#endif
#ifndef OUT
#define OUT  
#endif
#ifndef NULL
    #ifdef __cplusplus
        #define NULL 0
    #else
        #define NULL ((void *)0)
    #endif
#endif

#include "math.h"

 //대상의 길이를 구함
unsigned int __strlen(IN const char *cStr);
unsigned int __strlen_s(IN const char *cStr, IN unsigned int buf_size);

//rh -> lh 덮어씌우기
char* __strcpy(OUT char *lh, IN const char *rh);
char* __strcpy_s(OUT char *lh, IN const unsigned int lh_buf_size, IN const char *rh);
char* __strcpy_ss(OUT char *lh, IN const unsigned int lh_buf_size, IN const char *rh, IN const unsigned int rh_buf_size);

//lh + rh 뒤에 붙이기
char* __strcat(OUT char *lh, IN const char *rh);
char* __strcat_s(OUT char *const lh, IN const unsigned int lh_buf_size, IN const char *rh);

//lh == rh 동일한 내용인지 확인
int __strcmp(IN const char *lh, IN const char *rh);
int __strcmp_ss(IN const char *lh, IN const unsigned int lh_buf_size, IN const char *rh, IN unsigned int rh_buf_size);

//lh에 rh 내용이 어디서 시작인지 반환
char* __strstr(IN char *lh, IN const char *rh);
char* __strstr_s(IN char *lh, IN const unsigned int lh_buf_size, IN const char *rh);
char* __strstr_ss(IN char *lh, IN const unsigned int lh_buf_size, IN const char *rh, IN const unsigned int rh_buf_size);

//lh에 rh문자가 어디서 시작인지 반환
char* __strchr(IN char *const lh, IN int const rh);
char* __strchr_s(IN char *const lh, IN unsigned int lh_buf_size, IN int const rh);

//rh가 포함된 문자의 갯수를 반환
unsigned int __strFindCount(IN char *lh, IN char *rh);
unsigned int __strFindCount_s(IN char *lh, IN const unsigned int lh_buf_size, IN char *rh);

//공백 문자인지 확인
int __isspace(IN int c);

//공백 제거
char* __rtrim(IN char* s);
char* __ltrim(IN char* s);
char* __trim(IN char* s);

//포함 문자 확인
int start_with(IN char* lh, IN char* rh);
int end_with(IN char* lh, IN char* rh);

//숫자인지 판별
int __isnumber(IN int coded, IN char* str);

//string to int
int __atoi(IN char* str);

//문자 자르기
char*** __strSplit(IN char *pstr, IN char *delim, OUT char ***ppbuf, OUT unsigned int *buf_size);

//특정 문자열 변경
char** __strReplace(IN char* target, IN char* old, IN char* nnew, OUT char** ppbuf, OUT unsigned int* ppbuf_len_size);




unsigned int __strlen(const char *cStr)
{
    /**
     * 대상의 길이 구함
    @param:
        CStr: 문자열 길이를 확인할 대상
    @return:
        CStr의 문자열 길이
    */
    int nLen = 0;
   if(cStr == NULL) return nLen;
    while (cStr[nLen] != '\0')
    {           //’\0’까지
        nLen++; // 카운터
    }
    return nLen;
}
unsigned int __strlen_s(const char *cStr, unsigned int buf_size)
{
    /**
     * 대상의 길이 구함
    @param:
        CStr: 문자열 길이를 확인할 대상
        buf_size: 문자열 버퍼의 총 길이
    @return:
        CStr의 문자열 길이
        buf_size가 0일 경우 0 반환
    */
    unsigned int nLen = 0;
    // if(cStr == NULL || buf_size == 0) return NULL;
    if(cStr == NULL || buf_size == 0) return 0;

    for(; nLen < buf_size; nLen++){
       if(cStr[nLen] == '\0'){
           break;
       }
    }
    return nLen;
}

char* __strcpy(OUT char *lh, IN const char *rh)
{
    /**
     * rh의 내용을 lh에 덮어 씌우기
     * lh, rh에 대한 보호 없음
     * lh <- rh
     * @param:
     * lh: 저장시킬 대상
     * rh: 입력 대상
     * @return:
     *  lh를 반환
     */
    char *tmp = lh;
    if(lh == NULL || rh == NULL) return NULL;
    while (*rh != '\0')
    {
        *lh++ = *rh++;
    }
    *lh = '\0';

    return tmp;
}

char* __strcpy_s(OUT char *lh, IN const unsigned int lh_buf_size, IN const char *rh)
{
    /**
     * rh의 내용을 lh에 덮어 씌우기
     * rh에 대한 보호 없음
     * lh <- rh
     * @param:
     * lh: 저장시킬 대상
     * lh_buf_size: lh의 버퍼 사이즈
     * rh: 입력 대상
     * @return:
     *  lh를 반환
     *
     */
    if(lh == NULL || rh == NULL) return NULL;
    unsigned int rh_len_size = __strlen(rh);
    if(rh_len_size == 0) return NULL;
    if(lh_buf_size < rh_len_size) return NULL;


    for(unsigned int i = 0; i < rh_len_size; i++){
        lh[i] = rh[i];
    }
    lh[rh_len_size] = '\0';
    
    return lh;
}

char* __strcpy_ss(OUT char *lh, IN const unsigned int lh_buf_size, IN const char *rh, IN const unsigned int rh_buf_size)
{
    /**
     * rh의 내용을 lh에 덮어 씌우기
     * lh <- rh
     * @param:
     * lh: 저장시킬 대상
     * lh_buf_size: lh의 버퍼 사이즈
     * rh: 입력 대상
     * rh_buf_size: rh의 버퍼 사이즈
     * @return:
     *  lh를 반환
     *
     */
    if(lh == NULL || rh == NULL) return NULL;
    unsigned int rh_len_size = __strlen_s(rh, rh_buf_size);
    if(rh_len_size == 0) return NULL;
    if(lh_buf_size < rh_len_size + 1) return NULL;


    for(unsigned int i = 0; i < rh_len_size; i++){
        lh[i] = rh[i];
    }
    lh[rh_len_size] = '\0';
    
    return lh;
}

char* __strcat(OUT char *lh, IN const char *rh)
{
    /**
     * rh의 내용을 lh에 붙여 넣음
     * lh <- rh
     * @param:
     * lh: 저장시킬 대상
     * rh: 입력 대상
     * @return:
     *  원본 lh를 반환
     */
    if(lh == NULL && rh == NULL) return lh;
    char *ret = lh;
    ret += __strlen(ret);
    while (*rh != '\0')
    {
        *ret++ = *rh++;
    }
    *ret = '\0';
    return lh;
}

char* __strcat_s(OUT char *const lh, IN const unsigned int lh_buf_size, IN const char *rh)
{
    /**
     * rh의 내용을 lh에 붙여 넣음
     * lh <- rh
     * @param:
     * lh: 저장시킬 대상
     * lh_buf_size: 저장 시킬 대상의 버퍼 사이즈
     * rh: 입력 대상
     * @return:
     *  원본 lh를 반환
     *  실패시 원본 반환
     */
    if(lh == NULL && rh == NULL) return NULL;
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    if(lh_len_size == 0) return NULL;
    unsigned int full_size = lh_len_size + __strlen(rh) + 1;
    if(lh_buf_size < full_size) return NULL;

    for(unsigned int nLen = lh_len_size; nLen < full_size; nLen++){
        lh[nLen] = rh[nLen - lh_len_size];
    }

    return lh;
}

char* __strcat_ss(OUT char *const lh, IN const unsigned int lh_buf_size, IN const char *rh, unsigned int rh_buf_size)
{
    /**
     * rh의 내용을 lh에 붙여 넣음
     * lh <- rh
     * @param:
     * lh: 저장시킬 대상
     * lh_buf_size: 저장 시킬 대상의 버퍼 사이즈
     * rh: 입력 대상
     * @return:
     *  원본 lh를 반환
     *  실패시 원본 반환
     */
    if(lh == NULL && rh == NULL) return NULL;
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    unsigned int rh_len_size = __strlen_s(rh, rh_buf_size);
    //if(lh_len_size == 0) return NULL;
    unsigned int full_size = lh_len_size + (rh_buf_size > rh_len_size ? rh_len_size : rh_buf_size);
    if(lh_buf_size < full_size) return NULL;

    for(unsigned int nLen = lh_len_size; nLen < full_size; nLen++){
        lh[nLen] = rh[nLen - lh_len_size];
    }
    lh[full_size] = '\0';

    return lh;
}

int __strcmp(IN const char *lh, IN const char *rh)
{
    /**
     * 완전히 같으면 0
     * lh가 크면 1
     * rh가 크면 -1
     * @param:
     *  lh: 비교 대상
     *  rh: 비교 대상
     * @return:
     *  완전히 같으면 0
     *  lh가 크면 1
     *  rh가 크면 -1
     *  인자 문제 발생 시 -2
     */
    if(lh == NULL || rh == NULL) return -2;
    while ((*lh != '\0') || (*rh != '\0'))
    {
        if (*lh != *rh)
        {
            return *lh > *rh ? 1 : -1;
        }
        ++lh;
        ++rh;
    }
    return 0;
}

int __strcmp_ss(IN const char *lh, IN const unsigned int lh_buf_size, IN const char *rh, IN unsigned int rh_buf_size)   //lh == rh 동일한 내용인지 확인
{
    /**
     * 완전히 같으면 0
     * lh가 크면 1
     * rh가 크면 -1
     * @param:
     *  lh: 비교 대상
     *  lh_buf_size: lh의 버퍼 사이즈
     *  rh: 비교 대상
     *  rh_buf_size: rh의 버퍼 사이즈
     * @return:
     *  완전히 같으면 0
     *  lh가 크면 1
     *  rh가 크면 -1
     *  인자 문제 발생 시 -2
     */

    if(lh == NULL || rh == NULL) return -2;
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    unsigned int rh_len_size = __strlen_s(rh, rh_buf_size);
    if(lh_len_size == 0 || rh_len_size == 0) return -2;
    if(lh_len_size != rh_len_size){
        return lh_len_size > rh_len_size ? 1 : -1;
    }
    for(unsigned int i = 0; i < lh_len_size + 1; i++){
        if(lh[i] != rh[i]) return lh[i] > rh[i] ? 1 : -1;
    }
    return 0;
}

char* __strstr(IN char *lh, IN const char *rh)
{
    /**
     * 문자열의 임의의 위치를 찾음
     * lh에 rh와 같은 부분을 찾아 반환
     * @param:
     *   lh: 찾을 위치
     *   rh: 찾을 문자열
     * @return:
     * 찾은 위치 주소 반환
     */
    if(lh == NULL || rh == NULL) return NULL;
    while (*lh != '\0')
    {
        if (*lh == rh[0])
        {
            for (unsigned int i = 0;; i++)
            {
                if (rh[i] == '\0')
                    return lh;
                if ((lh[i] != rh[i]) || (lh[i] == '\0'))
                    break;
            }
        }
        ++lh;
    }
    return NULL;
}

char* __strstr_s(IN char *lh, IN const unsigned int lh_buf_size, IN const char *rh)
{
    /**
     * 문자열의 임의의 위치를 찾음
     * lh에 rh와 같은 부분을 찾아 반환
     * @param:
     *   lh: 찾을 위치
     *   lh_buf_size: lh의 버퍼 사이즈
     *   rh: 찾을 문자열
     * @return:
     * 찾은 위치 주소 반환
     */
    if(lh == NULL || rh == NULL) return NULL;
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    unsigned int rh_len_size = __strlen(rh);
    if(lh_len_size == 0 || rh_len_size == 0) return NULL;
    if(lh_len_size < rh_len_size) return NULL;

    while (*lh != '\0')
    {
        if (*lh == rh[0])
        {
            for (unsigned int i = 0;; i++)
            {
                if (rh[i] == '\0')
                    return lh;
                if ((lh[i] != rh[i]) || (lh[i] == '\0'))
                    break;
            }
        }
        ++lh;
    }
    return NULL;
}

char* __strstr_ss(IN char *lh, IN const unsigned int lh_buf_size, IN const char *rh, IN const unsigned int rh_buf_size)
{
    /**
     * 문자열의 임의의 위치를 찾음
     * lh에 rh와 같은 부분을 찾아 반환
     * @param:
     *   lh: 찾을 위치
     *   lh_buf_size: lh의 버퍼 사이즈
     *   rh: 찾을 문자열
     *   rh_buf_size: rh의 버퍼 사이즈
     * @return:
     * 찾은 위치 주소 반환
     */
    if(lh == NULL || rh == NULL) return NULL;
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    unsigned int rh_len_size = __strlen_s(rh, rh_buf_size);
    if(lh_len_size == 0 || rh_len_size == 0) return NULL;
    if(lh_len_size < rh_len_size) return NULL;

    while (*lh != '\0')
    {
        if (*lh == rh[0])
        {
            for (unsigned int i = 0;; i++)
            {
                if (rh[i] == '\0')
                    return lh;
                if ((lh[i] != rh[i]) || (lh[i] == '\0'))
                    break;
            }
        }
        ++lh;
    }
    return NULL;
}

char* __strchr(IN char *const lh, IN int const rh)
{
    /**
     * 특정 문자가 처음 나온 위치를 반환(포인터를 반환)
     * @param:
     *  lh: 찾을 문자열
     *  rh: 찾는 내용 문자
     * @return:
     *  최초 찾은 위치
     */
    if(lh == NULL || rh == 0) return NULL;
    int nLen = 0;
    while (lh[nLen] != rh)
    {
        if (lh[nLen] == '\0')
            return NULL;
        nLen++;
    }
    return lh + nLen;
}

char* __strchr_s(IN char *const lh, IN unsigned int lh_buf_size, IN int const rh)
{
    /**
     * 특정 문자가 처음 나온 위치를 반환(포인터를 반환)
     * @param:
     *  lh: 찾을 문자열
     *  rh: 찾는 내용 문자
     * @return:
     *  최초 찾은 위치
     */
    if(lh == NULL || rh == 0) return NULL;
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    if(lh_len_size == 0) return NULL;
    int nLen = 0;
    while (lh[nLen] != rh)
    {
        if (lh[nLen] == '\0')
            return NULL;
        nLen++;
    }
    return lh + nLen;
}

unsigned int __strFindCount(IN char *lh, IN char *rh)
{
    /**
     * rh가 포함된 문자열의 개수를 반환
     * @param:
     * lh: 대상이 될 문자열
     * lh_buf_size: lh의 버퍼 사이즈
     * rh: 찾을 문자열
     * @return:
     *  찾은 갯수
     */
    if(lh == NULL || rh == NULL) return 0;
    
    unsigned int lh_len_size = __strlen(lh);
    unsigned int rh_len_size = __strlen(rh);
    if(lh_len_size == 0 || rh_len_size == 0) return 0;

    char *lh_tmp = lh;
    unsigned int count = 0;

    while( 1 )
    {
        lh_tmp = __strstr(lh_tmp, rh);
        if(lh_tmp == NULL) return count;
        count++;
        lh_tmp += rh_len_size;
    }

    return count;
}

unsigned int __strFindCount_s(IN char *lh, IN const unsigned int lh_buf_size, IN char *rh)
{
    /**
     * rh가 포함된 문자열의 개수를 반환
     * @param:
     * lh: 대상이 될 문자열
     * lh_buf_size: lh의 버퍼 사이즈
     * rh: 찾을 문자열
     * @return:
     *  찾은 갯수
     */
    if(lh == NULL || rh == NULL) return 0;
    
    unsigned int lh_len_size = __strlen_s(lh, lh_buf_size);
    unsigned int rh_len_size = __strlen(rh);
    if(lh_len_size == 0 || rh_len_size == 0) return 0;

    char *lh_tmp = lh;
    unsigned int count = 0;

    while( 1 )
    {
        lh_tmp = __strstr_s(lh_tmp, lh_buf_size, rh);
        if(lh_tmp == NULL) return count;
        count++;
        lh_tmp += rh_len_size;
    }

    return count;
}

int __isspace(IN int c){
    /**
     * 공백 문자인지 확인
     * @Param
     *   c: 확인할 문자
     * @Return
     *   공백 문자라면 1 반환
     *   공백 문자가 아니라면 0 반환
     * 
     */
    switch(c){
        case ' '  :
        case '\t' :
        case '\n' :
        case '\v' :
        case '\f' :
        case '\r' :
        //case '\b' :   //backspace
        return 1;
        default   :
        return 0;
    }
    return 0;
}

// 문자열 우측 공백문자 삭제 함수
char* __rtrim(IN char* s) {
    /**
     * 오른쪽 공백 문자 제거
     * @Param
     *   s: 공백 제거할 문자열
     * @Return
     *   s를 반환
     */
    if(s == NULL) return 0;
    char *end = s + __strlen(s) - 1;
    if(__isspace(*end)){
        *end = '\0';
    }

    return s;
}

// 문자열 좌측 공백문자 삭제 함수
char* __ltrim(IN char *s) 
{
    /**
     * 왼쪽 공백 문자 제거
     * @Param
     *   s: 공백 제거할 문자열
     * @Return
     *   s를 반환
     */
    if(s == NULL) return 0;
    if(__isspace(*s)){
        __strcpy(s, s+1);
    }

    return s;
}


// 문자열 앞뒤 공백 모두 삭제 함수
char* __trim(IN char *s) 
{
    /**
     * 앞뒤 공백을 모두 제거
     * @Param
     * s: 제거할 문자열
     * @Return
     *   s를 반환
     */
    return __rtrim(__ltrim(s));
}

int start_with(IN char* lh, IN char* rh){
    /**
     * @Param
     *   lh: 대상 문자열
     *   rh: 찾을 문자열
     * @Return
     *   같지 않다면 0 반환
     *   같다면 1 반환
     */
    if(lh == NULL || rh == NULL) return 0;

    unsigned int lh_len_size = __strlen(lh);
    unsigned int rh_len_size = __strlen(rh);

    if(rh_len_size > lh_len_size) return 0;

    for(unsigned int i = 0; i < rh_len_size; i++){
        if( lh[i] != rh[i]) return 0;
    }
    return 1;
}

int end_with(IN char* lh, IN char* rh){
    /**
     * @Param
     *   lh: 대상 문자열
     *   rh: 찾을 문자열
     * @Return
     *   같지 않다면 0 반환
     *   같다면 1 반환
     */
    if(lh == NULL || rh == NULL) return 0;
    unsigned int lh_len_size = __strlen(lh);
    unsigned int rh_len_size = __strlen(rh);

    if(rh_len_size > lh_len_size) return 0;

    while(rh_len_size != 0){
        if(lh[--lh_len_size] != rh[--rh_len_size]) return 0;
    }
    return 1;
}

static char g_integerList_u[36] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
static char g_integerList_l[36] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

int __isnumber(IN int coded, IN char* str){
    /**
    * 해당 문자가 정수인지 아닌지 식별
    * @Param
    *   coded: 몇진수 인지 표기
    *   str: 확인할 문자열
    * @Return
    *   숫자라면 true
    *   아니라면 false
    */
   int isNum = 0;
   int str_len_size = __strlen(str);
    if(str_len_size == 0) return 0;
    for(int i = 0; i < str_len_size; i++){
        isNum = 0;
        if(str[i] == '-' || str[i] == '+'){
            continue;
        }
        for(int x = 0; x < coded ; x++){
            //printf("%c:::%c::%c\n", str[i], g_integerList_u[x], g_integerList_l[x]);
            if(str[i] == g_integerList_u[x] || str[i] == g_integerList_l[x]){
                isNum = 1;
                break;
            }
        }
        if(isNum == 0) return 0;
    }

    return 1;
}

//////////////int __

int __atoi(IN char* str){
    int ret = 0;
    int str_len_size = __strlen(str);
    if(str_len_size == 0) return 0;

    for(int i = 0; i < str_len_size; i++){
        int num = str[i] - '0';
        for(int k = 0; k < str_len_size - i-1; k++) num *= 10;
        ret += num;
    }

    return ret;
}

#include <stdlib.h>

/**
 * __strSplit Example
 * 
    char **pbuf = NULL;
    unsigned int size = 0;
    char arr[100] = "aanabbnaccnadd";
    char brr[100] = "na";
    printf("--------------------------------------------------\n");
    __strSplit(arr, brr, &pbuf, &size);
    printf("--------------------------------------------------\n");
    printf("pbuf :: %d, size:%d\n", pbuf, size);
    if( pbuf != NULL ){
        for(int i = 0; i < size; i++){
            printf("end[%d]::%s::size[%d]\n", i, pbuf[i], __strlen(pbuf[i]));
        }
    }

    //delete
    if( pbuf != NULL ){
        for(unsigned int i = 0; i < size; i++){
            if(pbuf[i] != NULL) free(pbuf[i]);
        }
        free(pbuf);
    }
*/
char*** __strSplit(IN char *pstr, IN char *delim, OUT char ***ppbuf, OUT unsigned int *buf_size)
{
    /**
     * 문자열 pstr을 delim의 단위로 잘라서 ppbuf인자에 반환
     * buf_size는 잘린 문자열의 갯수를 나태냄
     * @Param
     *   pstr: 대상이 되는 문자열
     *   delim: 자를 내용이 될 문자열
     *   ppbuf: 받아서 가져갈 버퍼
     *   buf_size: 반환될 버퍼의 사이즈
     * @return
     *   문제가 있을 경우 NULL을 반환
     *   문제가 없을 경우 ppbuf를 반환
     */
    if(pstr == NULL || delim == NULL) return NULL;

    unsigned int pstr_len_size = __strlen(pstr);
    unsigned int delim_len_size = __strlen(delim);
    if(pstr_len_size == 0 || delim_len_size == 0) return NULL;

    //앞에 정리 부분
    char* start_point = pstr; //자를 시작 부분
    char *end_point = __strstr_ss(pstr, pstr_len_size, delim, delim_len_size);    //end_point 위치

    //pstr과 delim이 같은 내용이라면 NULL 반환
    if(start_point == end_point && __strlen(start_point + delim_len_size) == 0) return NULL;

    int count = -1;
    do
    {
        //앞부분만 처리
        if (start_point != end_point)
        {
            if (end_point == NULL) //다 돌았다면
            {
                end_point = start_point + __strlen(start_point);
                if(end_point - start_point == 0)    break;

                count++;


                *ppbuf = (char**)realloc(*ppbuf, sizeof(char**) * (count + 1));
                if( *ppbuf == NULL )    break;

                //할당이 안되어 있다면
                (*ppbuf)[count] = (char*)malloc(sizeof(char) * (end_point - start_point + 1));
                if( (*ppbuf)[count] == NULL ){
                    for(int i = 0; i < count; i++){
                        free((*ppbuf)[i]);
                    }
                    break;
                }
                __strcpy_ss((*ppbuf)[count], end_point - start_point + 1, start_point, end_point - start_point);

                break;
            }
            else
            {
                count++;

                *ppbuf = (char**)realloc(*ppbuf, sizeof(char**) * (count + 1));
                if( *ppbuf == NULL )    break;

                //할당이 안되어 있다면
                (*ppbuf)[count] = (char*)malloc(sizeof(char) * (end_point - start_point + 1));
                if( (*ppbuf)[count] == NULL ){
                    for(int i = 0; i < count; i++){
                        free((*ppbuf)[i]);
                    }
                    break;
                }
                __strcpy_ss((*ppbuf)[count], end_point - start_point + 1, start_point, end_point - start_point);
            }
        }

        //마지막 정리
        end_point += delim_len_size;
        start_point = end_point;
        end_point = __strstr(end_point, delim);
    } while (1);

    *buf_size = count + 1;
    return ppbuf;
}

/*
 * __strReplace Example
 * 
    char *like_buf = NULL;
    unsigned int like_buf_size = 0;
    printf("arr::%s\n", arr);
    __strReplace("/data/dalvik-cache/arm/system@framework@GoogleBridge.jar@classes.dex", "/", "_", &like_buf, &like_buf_size);
    printf("LIKE::%s\n", like_buf);
    free(like_buf);

*/
char** __strReplace(IN char* target, IN char* old, IN char* nnew, OUT char** ppbuf, OUT unsigned int* ppbuf_len_size)
{
    /**
     * old 문자열을 nnew 문자로 모두 대체
     * 길이가 다르더라도 그 길이에 맞게 조절 진행
     * @Param
     *   target: 대상이 될 문자열
     *   old: 바꿀 문자열
     *   new: 새로이 바꿀 문자열
     *   ppbuf: 반환할 주소
     *   ppbuf_len_size: 반환할 문자열의 길이
     * @return
     *   문제 발생시 NULL 반환
     *   이상 없을 시 ppbuf 반환
     */
    if(target == NULL || old == NULL || nnew == NULL || ppbuf == NULL || ppbuf_len_size == NULL) return NULL;
    unsigned int target_len_size = __strlen(target);
    unsigned int old_len_size = __strlen(old);
    unsigned int nnew_len_size = __strlen(nnew);

    unsigned int old_count = __strFindCount(target, old);
    unsigned int interval = nnew_len_size - old_len_size;
    //interval이 - 라면 old의 길이가 더 김
    unsigned int memory_size = __strlen(target) + interval * old_count + 1; //'/0'때문에 하나 더 추가
    *ppbuf = (char*)malloc(memory_size);
    if(*ppbuf == NULL) return NULL;
    **ppbuf = 0; //처음 복사의 안정성을 위해서

    char *start_point = target;
    char *end_point = NULL;

    while(1){
        end_point = __strstr(start_point, old);
        //if(start_point != NULL) printf("start_point::%s\n", start_point);
        //if(end_point != NULL) printf("end_point::%s\n", end_point);
        //printf("ppbuf::%s\n", *ppbuf);
        if(end_point == NULL){  //끝처리
            end_point = start_point + __strlen(start_point);
            if(start_point == end_point)    break;
            __strcat_ss(*ppbuf, memory_size, start_point, end_point-start_point);
            break;
        }
        __strcat_ss(*ppbuf, memory_size, start_point, end_point-start_point);
        __strcat_ss(*ppbuf, memory_size, nnew, nnew_len_size);
        start_point = end_point + old_len_size;

    }
    *ppbuf_len_size = memory_size;

    return ppbuf;
}







#ifdef  __cplusplus
}
#endif

#endif  /* _IN_STRING */