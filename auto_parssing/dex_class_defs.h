#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>
#include <string>
#include "dex_access_flags.h"

//convert 때문에.. 하.... 진짜 더럽구나 하하하하하핳
#include "dex_type_list.h"

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_CLASS_DEFS
#define _IN_CLASS_DEFS

class Class_defs : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Class_def_item{
                u4            class_idx;                     //2
                u4            access_flags;                   //
                u4            superclass_idx;
                u4            interfaces_off;
                u4            source_file_idx;
                u4            annotations_off;
                u4            class_data_off;
                u4            static_values_off;
    };
#pragma pack(pop)
    std::vector<Class_def_item>             m_class_defs;
    // Access_flags                            *m_access_flags;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
    unsigned int __convert();

public:
    Class_defs(){init_reference(eTYPE_CLASS_DEF_ITEM, this);}
    void convert();
};

void Class_defs::convert(){
    if(! get_command_flag(eFLAG_NORMALIZATION) ){
        return;
    }

    Type_lists* ptype_lists = (Type_lists*)get_reference(eTYPE_TYPE_LIST);
    if(ptype_lists == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Class_defs.convert() reference NULL Error !\n");
        return;
    }
    if( ptype_lists->get_convert_offsets().size() == 0 ) return;
    for(unsigned int i = 0; i < m_class_defs.size(); i++){
        m_class_defs[i].interfaces_off = ptype_lists->get_symmetry_offset(m_class_defs[i].interfaces_off);
    }
    __convert();
}

unsigned int Class_defs::__convert(){
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    Base_dex_reference* ptype_lists = get_reference(eTYPE_TYPE_LIST);
    if(get_command_flag(eFLAG_LOG)){
        if(pstr_ids == NULL || ptype_ids == NULL || ptype_lists == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Class_defs.print_full() reference NULL\n");
            return m_start_offset;
        }
        Log::print(Log::LOG_INFO, "**Convert class def Full\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t IDX\t\t STRING\n");
        Log::print(Log::LOG_INFO, "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    }
    //초기화가 필요해서 일단 임시로 만들어 놓음
    //추후에 나머지 것들에 필요한 모습을 보고 적절히 바꿀 필요 있음
    Access_flags access_flags;

    // m_class_defs.reserve(m_offset_count);
    unsigned int offset_revision = 0;
    for(unsigned int i = 0; i < m_class_defs.size(); i++){
        Class_defs::Class_def_item &_class = m_class_defs[i];
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.class_idx;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t class_idx         \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.class_idx, ptype_ids->as_string(_class.class_idx).c_str());
        offset_revision += sizeof(u4);

        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.access_flags;
        std::vector<unsigned char*> access_strings = Access_flags::get_strings( _class.access_flags );
        std::string str_access_flags = "";
        for(unsigned int k = 0; k < access_strings.size(); k++){
            str_access_flags.append( (char*)access_strings[k] );
            if( k != access_strings.size() - 1 ) str_access_flags.append("|");
        }
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t access_flags     \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.access_flags, str_access_flags.c_str());

        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.superclass_idx;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t upserclass_idx   \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.superclass_idx, ptype_ids->as_string(_class.superclass_idx).c_str());
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.interfaces_off;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t interfaces_off   \t 0x%08X\t %s\n", m_start_offset + offset_revision, i, _class.interfaces_off, ptype_lists->as_off_string(_class.interfaces_off).c_str());
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.source_file_idx;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t source_file_idx  \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.source_file_idx, pstr_ids->as_string(_class.source_file_idx).c_str());
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.annotations_off;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t annotations_off  \t 0x%08X\t \n", m_start_offset + offset_revision, i, _class.annotations_off);
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.class_data_off;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t class_data_off   \t 0x%08X\t \n", m_start_offset + offset_revision, i, _class.class_data_off);
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = _class.static_values_off;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t static_values_off\t 0x%08X\t \n", m_start_offset + offset_revision, i, _class.static_values_off);
        offset_revision += sizeof(u4);

        // m_class_defs.push_back(_class);
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n\n");   
    return m_start_offset + offset_revision;
}

unsigned int Class_defs::__parssing(unsigned int _command_flag){
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    Base_dex_reference* ptype_lists = get_reference(eTYPE_TYPE_LIST);
    if(get_command_flag(eFLAG_LOG)){
        if(pstr_ids == NULL || ptype_ids == NULL || ptype_lists == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Class_defs.print_full() reference NULL\n");
            return m_start_offset;
        }
        Log::print(Log::LOG_INFO, "**class def Full\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t IDX\t\t STRING\n");
        Log::print(Log::LOG_INFO, "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    }
    //초기화가 필요해서 일단 임시로 만들어 놓음
    //추후에 나머지 것들에 필요한 모습을 보고 적절히 바꿀 필요 있음
    Access_flags access_flags;

    m_class_defs.reserve(m_offset_count);
    unsigned int offset_revision = 0;
    for(unsigned int i = 0; i < m_offset_count; i++){
        Class_defs::Class_def_item _class;
        _class.class_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t class_idx         \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.class_idx, ptype_ids->as_string(_class.class_idx).c_str());
        offset_revision += sizeof(u4);

        _class.access_flags = *((u4*)(m_dex + m_start_offset + offset_revision));
        std::vector<unsigned char*> access_strings = Access_flags::get_strings( _class.access_flags );
        std::string str_access_flags = "";
        for(unsigned int k = 0; k < access_strings.size(); k++){
            str_access_flags.append( (char*)access_strings[k] );
            if( k != access_strings.size() - 1 ) str_access_flags.append("|");
        }
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t access_flags     \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.access_flags, str_access_flags.c_str());

        offset_revision += sizeof(u4);
        _class.superclass_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t upserclass_idx   \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.superclass_idx, ptype_ids->as_string(_class.superclass_idx).c_str());
        offset_revision += sizeof(u4);
        _class.interfaces_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t interfaces_off   \t 0x%08X\t %s\n", m_start_offset + offset_revision, i, _class.interfaces_off, ptype_lists->as_off_string(_class.interfaces_off).c_str());
        offset_revision += sizeof(u4);
        _class.source_file_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t source_file_idx  \t %10d\t %s\n", m_start_offset + offset_revision, i, _class.source_file_idx, pstr_ids->as_string(_class.source_file_idx).c_str());
        offset_revision += sizeof(u4);
        _class.annotations_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t annotations_off  \t 0x%08X\t \n", m_start_offset + offset_revision, i, _class.annotations_off);
        offset_revision += sizeof(u4);
        _class.class_data_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t class_data_off   \t 0x%08X\t \n", m_start_offset + offset_revision, i, _class.class_data_off);
        offset_revision += sizeof(u4);
        _class.static_values_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t static_values_off\t 0x%08X\t \n", m_start_offset + offset_revision, i, _class.static_values_off);
        offset_revision += sizeof(u4);

        m_class_defs.push_back(_class);
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n\n");   
    return m_start_offset + offset_revision;
}

#endif /* __IN_CLASS_DEFS */