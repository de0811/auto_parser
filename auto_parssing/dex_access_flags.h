#include <map>
#include <vector>

//class_def_item, encoded_field, encoded_method, InnerClass 포함
//Base_dex_reference로 궂이 나눌 필요가 있을까에 대해서 끝까지 만들어봐야 알 수 있을듯

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ACCESS_FLAGS
#define _IN_ACCESS_FLAGS




class Access_flags{
public:
    enum{
        ACC_PUBLIC                  = 0x00001,
        ACC_PRIVATE                 = 0x00002,
        ACC_PROTECTED               = 0x00004,
        ACC_STATIC                  = 0x00008,
        ACC_FINAL                   = 0x00010,
        ACC_SYNCHRONIZED            = 0x00020,
        ACC_VOLATILE                = 0x00040,
        ACC_BRIDGE                  = 0x00040,
        ACC_TRANSIENT               = 0x00080,
        ACC_VARARGS                 = 0x00080,
        ACC_NATIVE                  = 0x00100,
        ACC_INTERFACE               = 0x00200,
        ACC_ABSTRACT                = 0x00400,
        ACC_STRICT                  = 0x00800,
        ACC_SYNTHETIC               = 0x01000,
        ACC_ANNOTATION              = 0x02000,
        ACC_ENUM                    = 0x04000,
        ACC_UNUSED                  = 0x08000,
        ACC_CONSTRUCTOR             = 0x10000,
        ACC_DECLARED_SYNCHRONIZED   = 0x20000,
    };

    struct FlagInfo{
        unsigned int        type;
        unsigned char*      name;
        FlagInfo(){}
        FlagInfo(unsigned int type, unsigned char* name){
            this->type = type;
            this->name = name;
        }
    };

    Access_flags(){  init();  }
    void init();
    static std::vector<unsigned char*> get_strings(unsigned int flags);
private:
    static std::map<unsigned int, FlagInfo>         *m_access_flags;
    static std::vector<unsigned int>                *m_vec_access_flags;
};
std::map<unsigned int, Access_flags::FlagInfo>         *Access_flags::m_access_flags        = new std::map<unsigned int, Access_flags::FlagInfo>();
std::vector<unsigned int>                              *Access_flags::m_vec_access_flags    = new std::vector<unsigned int>();

std::vector<unsigned char*> Access_flags::get_strings(unsigned int flags){
    unsigned int size = 0;
    for(unsigned int i = 0; i < m_vec_access_flags->size(); i++){
        if( (*m_vec_access_flags)[i] & flags ) {
            size++;
        }
    }

    std::vector<unsigned char*> result;
    result.reserve(size);
    for(unsigned int i = 0; i < m_vec_access_flags->size(); i++){
        if( (*m_vec_access_flags)[i] & flags ) {
            result.push_back( (*m_access_flags)[ (*m_vec_access_flags)[i] ].name );
        }
    }
    return result;
}

void Access_flags::init(){
    if(m_access_flags == NULL || m_vec_access_flags == NULL) return;
    if( m_access_flags->empty() == 0 )    return;
    m_access_flags->insert( std::make_pair( ACC_PUBLIC                     , FlagInfo(ACC_PUBLIC                   , (unsigned char*) "public"                   ) ) );
    m_access_flags->insert( std::make_pair( ACC_PRIVATE                    , FlagInfo(ACC_PRIVATE                  , (unsigned char*) "private"                  ) ) );
    m_access_flags->insert( std::make_pair( ACC_PROTECTED                  , FlagInfo(ACC_PROTECTED                , (unsigned char*) "protected"                ) ) );
    m_access_flags->insert( std::make_pair( ACC_STATIC                     , FlagInfo(ACC_STATIC                   , (unsigned char*) "static"                   ) ) );
    m_access_flags->insert( std::make_pair( ACC_FINAL                      , FlagInfo(ACC_FINAL                    , (unsigned char*) "final"                    ) ) );
    m_access_flags->insert( std::make_pair( ACC_SYNCHRONIZED               , FlagInfo(ACC_SYNCHRONIZED             , (unsigned char*) "synchronized"             ) ) );
    m_access_flags->insert( std::make_pair( ACC_VOLATILE                   , FlagInfo(ACC_VOLATILE                 , (unsigned char*) "volatile"                 ) ) );
    m_access_flags->insert( std::make_pair( ACC_BRIDGE                     , FlagInfo(ACC_BRIDGE                   , (unsigned char*) "bridge"                   ) ) );
    m_access_flags->insert( std::make_pair( ACC_TRANSIENT                  , FlagInfo(ACC_TRANSIENT                , (unsigned char*) "transient"                ) ) );
    m_access_flags->insert( std::make_pair( ACC_VARARGS                    , FlagInfo(ACC_VARARGS                  , (unsigned char*) "varargs"                  ) ) );
    m_access_flags->insert( std::make_pair( ACC_NATIVE                     , FlagInfo(ACC_NATIVE                   , (unsigned char*) "native"                   ) ) );
    m_access_flags->insert( std::make_pair( ACC_INTERFACE                  , FlagInfo(ACC_INTERFACE                , (unsigned char*) "interface"                ) ) );
    m_access_flags->insert( std::make_pair( ACC_ABSTRACT                   , FlagInfo(ACC_ABSTRACT                 , (unsigned char*) "abstract"                 ) ) );
    m_access_flags->insert( std::make_pair( ACC_STRICT                     , FlagInfo(ACC_STRICT                   , (unsigned char*) "strictfp"                 ) ) );
    m_access_flags->insert( std::make_pair( ACC_SYNTHETIC                  , FlagInfo(ACC_SYNTHETIC                , (unsigned char*) "synthetic"                ) ) );
    m_access_flags->insert( std::make_pair( ACC_ANNOTATION                 , FlagInfo(ACC_ANNOTATION               , (unsigned char*) "annotation"               ) ) );
    m_access_flags->insert( std::make_pair( ACC_ENUM                       , FlagInfo(ACC_ENUM                     , (unsigned char*) "enum"                     ) ) );
    m_access_flags->insert( std::make_pair( ACC_UNUSED                     , FlagInfo(ACC_UNUSED                   , (unsigned char*) ""                         ) ) );
    m_access_flags->insert( std::make_pair( ACC_CONSTRUCTOR                , FlagInfo(ACC_CONSTRUCTOR              , (unsigned char*) "constructor"              ) ) );
    m_access_flags->insert( std::make_pair( ACC_DECLARED_SYNCHRONIZED      , FlagInfo(ACC_DECLARED_SYNCHRONIZED    , (unsigned char*) "declared-synchronized"    ) ) );


    m_vec_access_flags->reserve(20);
    m_vec_access_flags->push_back(    ACC_PUBLIC                     );
    m_vec_access_flags->push_back(    ACC_PRIVATE                    );
    m_vec_access_flags->push_back(    ACC_PROTECTED                  );
    m_vec_access_flags->push_back(    ACC_STATIC                     );
    m_vec_access_flags->push_back(    ACC_FINAL                      );
    m_vec_access_flags->push_back(    ACC_SYNCHRONIZED               );
    m_vec_access_flags->push_back(    ACC_VOLATILE                   );
    m_vec_access_flags->push_back(    ACC_BRIDGE                     );
    m_vec_access_flags->push_back(    ACC_TRANSIENT                  );
    m_vec_access_flags->push_back(    ACC_VARARGS                    );
    m_vec_access_flags->push_back(    ACC_NATIVE                     );
    m_vec_access_flags->push_back(    ACC_INTERFACE                  );
    m_vec_access_flags->push_back(    ACC_ABSTRACT                   );
    m_vec_access_flags->push_back(    ACC_STRICT                     );
    m_vec_access_flags->push_back(    ACC_SYNTHETIC                  );
    m_vec_access_flags->push_back(    ACC_ANNOTATION                 );
    m_vec_access_flags->push_back(    ACC_ENUM                       );
    m_vec_access_flags->push_back(    ACC_UNUSED                     );
    m_vec_access_flags->push_back(    ACC_CONSTRUCTOR                );
    m_vec_access_flags->push_back(    ACC_DECLARED_SYNCHRONIZED      );
}


#endif  /* _IN_ACCESS_FLAGS */