#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_DEX_COMMON
#define _IN_DEX_COMMON

#ifdef  __cplusplus
extern "C" {
#endif



#include <stdio.h>
#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#define u1 unsigned char
#define u2 unsigned short
#define u4 unsigned int
#define u8 unsigned long long

#define s1 signed char
#define s2 signed short
#define s4 signed int
#define s8 signed long long

//#define OFFSET unsigned int

/*
//-----------------------------------------------------------------
// 테스트용 프린트
//-----------------------------------------------------------------
void tprint(unsigned char* pBuf, unsigned int max_size, unsigned int idx) {
	unsigned char asci[17] = { 0, };

	printf("            00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F    ASCI\n");

	//if (idx != 0) printf("0x%08X  ", idx);
	for (unsigned int i = 0; i < idx % 16; i++) printf("   ");

	for (; idx < max_size; idx++) {
		if (idx % 16 == 0) printf("0x%08X  ", idx);
		printf("%02X ", (unsigned char)pBuf[idx]);
		asci[idx % 16] = pBuf[idx];

		if ((idx + 1) % 16 == 0) {
			for (int i = 0; i < 16; i++) {
				if (asci[i] >= 0x20 && asci[i] <= 0x7E) // 특수 문자 아니면 출력
					printf("%c", asci[i]);
				else printf("."); // 특수문자, 그래픽문자 등은 마침표로 출력
			}
			printf("\n");
		}
	}
	printf("\n");
}

*/


#ifdef  __cplusplus
}
#endif

#endif  /* _IN_DEX_COMMON */
