#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>
#include <map>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_MAP_LIST
#define _IN_MAP_LIST

class Map_list : public Base_dex_parssing{
private:
#pragma pack(push, 1)
    struct Map_item{
                u2              type;                     //2
                u2            unused;                     //4
                u4              size;                       //8
                u4            offset;                     //12
    };
#pragma pack(pop)

    std::map<unsigned short, Map_item> m_map_list;
    //unsigned char* m_dex;

protected:
    virtual void __init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count);
    virtual unsigned int __parssing(unsigned int _command_flag);
public:
    Map_list(){}

    std::pair<unsigned int, unsigned int> get(unsigned int type);

    const char* get_type_name(unsigned short);
};

void Map_list::__init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count){
    m_offset_count = *((u4*)(dex + start_offset));
    m_start_offset += sizeof(u4);
}

const char* Map_list::get_type_name(unsigned short t){
    switch(t){
        case eTYPE_HEADER_ITEM :
        return "eTYPE_HEADER_ITEM";
        case eTYPE_STRING_ID_ITEM :
        return "eTYPE_STRING_ID_ITEM";
        case eTYPE_TYPE_ID_ITEM :
        return "eTYPE_TYPE_ID_ITEM";
        case eTYPE_PROTO_ID_ITEM :
        return "eTYPE_PROTO_ID_ITEM";
        case eTYPE_FIELD_ID_ITEM :
        return "eTYPE_FIELD_ID_ITEM";
        case eTYPE_METHOD_ID_ITEM :
        return "eTYPE_METHOD_ID_ITEM";
        case eTYPE_CLASS_DEF_ITEM :
        return "eTYPE_CLASS_DEF_ITEM";
        case eTYPE_CALL_SITE_ID_ITEM :
        return "eTYPE_CALL_SITE_ID_ITEM";
        case eTYPE_METHOD_HANDLE_ITEM :
        return "eTYPE_METHOD_HANDLE_ITEM";
        
        case eTYPE_MAP_LIST :
        return "eTYPE_MAP_LIST";
        case eTYPE_TYPE_LIST :
        return "eTYPE_TYPE_LIST";
        case eTYPE_ANNOTATION_SET_REF_LIST :
        return "eTYPE_ANNOTATION_SET_REF_LIST";
        case eTYPE_ANNOTATION_SET_ITEM :
        return "eTYPE_ANNOTATION_SET_ITEM";

        case eTYPE_CLASS_DATA_ITEM :
        return "eTYPE_CLASS_DATA_ITEM";
        case eTYPE_CODE_ITEM :
        return "eTYPE_CODE_ITEM";
        case eTYPE_STRING_DATA_ITEM :
        return "eTYPE_STRING_DATA_ITEM";
        case eTYPE_DEBUG_INFO_ITEM :
        return "eTYPE_DEBUG_INFO_ITEM";
        case eTYPE_ANNOTATION_ITEM :
        return "eTYPE_ANNOTATION_ITEM";
        case eTYPE_ENCODED_ARRAY_ITEM :
        return "eTYPE_ENCODED_ARRAY_ITEM";
        case eTYPE_ANNOTATIONS_DIRECTORY_ITEM :
        return "eTYPE_ANNOTATIONS_DIRECTORY_ITEM";
        default :
        return "eTYPE_ERROR";
    }
    return NULL;
}

unsigned int Map_list::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    if(get_command_flag(eFLAG_LOG)){
        // Log::print(Log::LOG_INFO, "**MAP list Full\n");
        printf("**MAP list Full\n");
        // Log::print(Log::LOG_INFO, "OFFSET\t\t TYPE\t UNUSED\t SIZE\t OFFSET\t\t COUNT\t TYPE_NAME\n");
        printf("OFFSET\t\t TYPE\t UNUSED\t SIZE\t OFFSET\t\t COUNT\t TYPE_NAME\n");
        // Log::print(Log::LOG_INFO, "----------------------------------------------------------------------------------------------------------------\n");
        printf("----------------------------------------------------------------------------------------------------------------\n");
    }
    for(unsigned int i = 0; i < m_offset_count; i++){
        Map_list::Map_item map;
        unsigned int tmp_off = m_start_offset + offset_revision;
        map.type = *((u2*)(m_dex + m_start_offset + offset_revision));
        offset_revision += sizeof(map.type);
        map.unused = *((u2*)(m_dex + m_start_offset + offset_revision));
        offset_revision += sizeof(map.unused);
        map.size = *((u4*)(m_dex + m_start_offset + offset_revision));
        offset_revision += sizeof(map.size);
        map.offset = *((u4*)(m_dex + m_start_offset + offset_revision));
        offset_revision += sizeof(map.offset);

        m_map_list.insert(std::make_pair(map.type, map));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t 0x%04X\t 0x%02X\t %05d\t 0x%08X\t %04d\t %s\n", m_start_offset + offset_revision, map.type, map.unused, map.size, map.offset, i + 1, get_type_name(map.type));
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t 0x%04X\t 0x%02X\t %05d\t 0x%08X\t %04d\t %s\n", tmp_off, map.type, map.unused, map.size, map.offset, i + 1, get_type_name(map.type));
    }
    // if(get_command_flag(eFLAG_LOG))Log::print(Log::LOG_INFO, "\n\n");   
    if(get_command_flag(eFLAG_LOG)) printf("\n\n");   
    return m_start_offset + offset_revision;
}

std::pair<unsigned int, unsigned int> Map_list::get(unsigned int type){
    Map_list::Map_item map_item = m_map_list[type];
    return std::make_pair(map_item.offset, map_item.size);
}

#endif /* _IN_MAP_LIST */