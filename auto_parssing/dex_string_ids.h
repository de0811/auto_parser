﻿#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>
#include "leb128.h"

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_STRING_IDS
#define _IN_STRING_IDS

//UTF-16 사용

class String_ids : public Base_dex_parssing, public Base_dex_reference {
    struct String_data_item{
        ULEB128                 utf16_size;
        unsigned char*          data;
    };
private:
    //ids의 offset 값만 저장
    std::vector<unsigned int> m_string_ids;
    //모든 string 값 저장
    std::vector<String_data_item> m_string_data_items;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);

public:
    String_ids(){init_reference(eTYPE_STRING_ID_ITEM, this); init_reference(eTYPE_STRING_DATA_ITEM, this);}
    //string_ids 의 모든 정보를 출력
    void print_full();
    //string_ids 의 idx 번째의 문자열을 반환
    virtual std::string as_string(unsigned int idx);
    //"/n" 문제를 해결하기 위해 사용 함수
    std::string ReplaceAll(std::string &str, const std::string& from, const std::string& to);
    //갯수에 맞게 size를 조정
    void convert(unsigned int idx, unsigned int limit_size);
};

std::string String_ids::ReplaceAll(std::string &str, const std::string& from, const std::string& to){
    size_t start_pos = 0; //string처음부터 검사
    while((start_pos = str.find(from, start_pos)) != std::string::npos)  //from을 찾을 수 없을 때까지
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // 중복검사를 피하고 from.length() > to.length()인 경우를 위해서
    }
    return str;
}

unsigned int String_ids::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    if(get_command_flag(eFLAG_LOG)){
        // Log::print(Log::LOG_INFO, "**String_ids & String_data_items\n");
        printf("**String_ids & String_data_items\n");
        // Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME                  \t DATA\n");
        printf("OFFSET\t\t COUNT\t NAME                  \t DATA\n");
        // Log::print(Log::LOG_INFO, "-------------------------------------------------------------------------\n");
        printf("-------------------------------------------------------------------------\n");
    }

    m_string_ids.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        unsigned int tmp_string_data_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t STRING_IDX            \t 0x%08X\t\t \n", m_start_offset + offset_revision, i, tmp_string_data_off);
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t STRING_IDX            \t 0x%08X\t\t \n", m_start_offset + offset_revision, i, tmp_string_data_off);
        m_string_ids.push_back(tmp_string_data_off);
        offset_revision += sizeof(u4);
        unsigned int tmp_utf16_size_off = tmp_string_data_off;  //cprint
        unsigned char* tmp_dex = tmp_string_data_off + m_dex;
        String_data_item string_data_item;
        string_data_item.utf16_size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t STRING_DATA.utf16_size\t %8d\n", tmp_utf16_size_off, i, string_data_item.utf16_size);
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t STRING_DATA.utf16_size\t %8d\n", tmp_utf16_size_off, i, string_data_item.utf16_size);
        string_data_item.data = tmp_dex;
        if(get_command_flag(eFLAG_LOG)){
            std::string tmp_string((const char*)tmp_dex);
            ReplaceAll(tmp_string, "\n", "\\n");
#ifdef WIN32
            printf("0x%08X\t %04d\t STRING_DATA.data      \t %s\n", tmp_dex - m_dex, i, tmp_string.c_str());
#else
            printf("0x%08lX\t %04d\t STRING_DATA.data      \t %s\n", tmp_dex - m_dex, i, tmp_string.c_str());
#endif
        }
        m_string_data_items.push_back(string_data_item);
    }
    return m_start_offset + offset_revision;
}


//string_ids 의 idx 번째의 문자열을 반환
std::string String_ids::as_string(unsigned int idx){
    if(m_string_data_items.size() <= idx){
        if(idx == 0xFFFFFFFF){
            return std::string();
        }
        Log::print(Log::LOG_ERROR, "Error !! String_ids.as_string(idx[%d])\n", idx);
        return std::string();
    }
    std::string tmp_str = std::string((const char*)m_string_data_items[idx].data);
    return ReplaceAll(tmp_str, "\n", "\\n");
}

//갯수에 맞게 size를 조정
void String_ids::convert(unsigned int idx, unsigned int limit_size){
    if(! get_command_flag(eFLAG_NORMALIZATION)){
        return;
    }

    if(idx == 0xFFFFFFFF || m_string_data_items.size() <= idx){
        return;
    }
    // unsigned char* pstr = m_string_data_items[idx].data;
    unsigned char buf[1000] = {0,};
    unsigned char* tmp_dex = m_string_ids[idx] + m_dex;
    m_string_data_items[idx].utf16_size = limit_size;
    tmp_dex = writeUnsignedLeb128(tmp_dex, m_string_data_items[idx].utf16_size);
    Log::print(Log::LOG_DEBUG, "__DD__String_Offset Origin 0x%08X  convert 0x%08X\n", m_string_data_items[idx].data - m_dex, tmp_dex - m_dex);

	strcpy((char*)tmp_dex, (char*)m_string_data_items[idx].data);
    //__strcpy_ss((char*)tmp_dex, limit_size, (char*)m_string_data_items[idx].data, limit_size);
    m_string_data_items[idx].data = tmp_dex;
    m_string_data_items[idx].data[limit_size + 1] = '\0';
    printf("0x%08lX\t %04d\t STRING_DATA.data      \t %s\n", tmp_dex - m_dex, idx, m_string_data_items[idx].data);
    // m_string_data_items[idx].data[limit_size + 1] = '\0';
    // pstr[limit_size] = '\0';
    // pstr += limit_size;
    // pstr = '\0';
}

#endif  /* _IN_STRING_IDS */