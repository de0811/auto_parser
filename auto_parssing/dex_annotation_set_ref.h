
#include "dex_common.h"
#include "dex_base_parssing.h"
#include "dex_encoded_value.h"
#include <vector>




#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ANNOTATION_SET_REF
#define _IN_ANNOTATION_SET_REF

class Annotation_set_ref : public Base_dex_parssing, public Base_dex_reference{

private:
#pragma pack(push, 1)
    struct Annotation_set_ref_item{
                u4                                      size;
                std::vector<u4>                           annotation_off_item;
    };
#pragma pack(pop)
    std::vector<Annotation_set_ref_item>        m_annotation_set_refs;
protected:
    unsigned int __parssing(unsigned int _command_flag);
public:
};

unsigned int Annotation_set_ref::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    Base_dex_reference* pannotation = get_reference(eTYPE_ANNOTATION_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        Log::print(Log::LOG_INFO, "**Annotation_set_ref_item ----\n");
        Log::print(Log::LOG_INFO, "------------------------------------------------------------------------------------------------------\n");
        if(pannotation == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Annotation_set_ref.__parssing() reference NULL !\n");
            return m_start_offset;

        }
    }

    m_annotation_set_refs.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        Annotation_set_ref_item annotation_set_ref_item;
        annotation_set_ref_item.size = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotation_set_ref.size\t 0x%02X\n", m_start_offset + offset_revision, i, annotation_set_ref_item.size);
        offset_revision += sizeof(u4);
        
        annotation_set_ref_item.annotation_off_item.reserve(annotation_set_ref_item.size);
        for(unsigned int k = 0; k < annotation_set_ref_item.size; k++){
            u4 annotation_off = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotation_set_ref[%d].annotation_off\t 0x%08X\n", m_start_offset + offset_revision, i, k, annotation_off);
            offset_revision += sizeof(u4);
            annotation_set_ref_item.annotation_off_item.push_back(annotation_off);
        }
        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;
        m_annotation_set_refs.push_back(annotation_set_ref_item);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");


    return m_start_offset + offset_revision;
}


#endif /* _IN_ANNOTATION_SET_REF */