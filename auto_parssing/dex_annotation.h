
#include "dex_common.h"
#include "dex_base_parssing.h"
#include "dex_encoded_value.h"
#include <vector>




#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ANNOTATION
#define _IN_ANNOTATION

class Annotation : public Base_dex_parssing, public Base_dex_reference{
public:
    enum{
        VISIBILITY_BUILD        =0x00,
        VISIBILITY_RUNTIME      =0x01,
        VISIBILITY_SYSTEM       =0x02,
    };
private:
#pragma pack(push, 1)
    struct Annotation_item{
                u1                                      visibility;
                Encoded_value                           encoded_annotation;
    };
#pragma pack(pop)
    std::vector<unsigned int>                       m_annotation_offs;
    std::vector<Annotation_item>                    m_annotations;
protected:
    unsigned int __parssing(unsigned int _command_flag);
public:
    Annotation(){init_reference(eTYPE_ANNOTATION_ITEM, this);}
    //해당하는 index 값에 맞는 문자열을 반환
    virtual std::string as_string(unsigned int idx);
    //offset 값에 해당하는 문자열을 반환
    virtual std::string as_off_string(unsigned int off);
};

unsigned int Annotation::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    if(get_command_flag(eFLAG_LOG)){
        Log::print(Log::LOG_INFO, "**Annotation_item ----\n");
        Log::print(Log::LOG_INFO, "------------------------------------------------------------------------------------------------------\n");
    }

    m_annotations.reserve(m_offset_count);
    m_annotation_offs.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        m_annotation_offs.push_back(m_start_offset + offset_revision);
        Annotation_item annotation_item;
        annotation_item.visibility = *((u1*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotation.visibility\t 0x%02X\n", m_start_offset + offset_revision, i, annotation_item.visibility);
        offset_revision += sizeof(u1);
        
        annotation_item.encoded_annotation.init(m_dex, m_start_offset + offset_revision, 1);
        offset_revision = annotation_item.encoded_annotation.parssing_annotation(2, get_command_flag(eFLAG_LOG)) - m_start_offset;
        m_annotations.push_back(annotation_item);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");


    return m_start_offset + offset_revision;
}

//해당하는 index 값에 맞는 문자열을 반환
std::string Annotation::as_string(unsigned int idx){
    if(m_annotations.size() <= idx){
        Log::print(Log::LOG_ERROR, "ERROR !! Annotation(%d) idx over !\n", idx);
        return std::string("");
    }
    return m_annotations[idx].encoded_annotation.as_annotation_string();
}
//offset 값에 해당하는 문자열을 반환
std::string Annotation::as_off_string(unsigned int off){
    bool check = false;
    if(off == 0) return std::string("");
    unsigned int i = 0;
    for(i = 0; i < m_annotation_offs.size(); i++){
        if( m_annotation_offs[i] == off ){
            check = true;
            break;
        }
    }
    if(check){
        return as_string(i);
    }
    else{
        Log::print(Log::LOG_ERROR, "ERROR !! Annotation(0x%08X) None Offset !\n", off);
        return std::string("");
    }
}

#endif /* _IN_ANNOTATION */