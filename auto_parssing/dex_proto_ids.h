﻿#include "dex_common.h"
#include <vector>
#include <string>
#include "dex_base_parssing.h"

//convert 때문에 궂이.... 하...... 이거때매 하.... 내가... 하....... 거지같은.. .하ㅏㅏ......
#include <list>
#include "dex_type_list.h"
#include "dex_string_ids.h"

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_PROTO_IDS
#define _IN_PROTO_IDS

class Proto_ids : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Proto_id_item{
                unsigned int            shorty_idx;                     //4
                unsigned int            return_type_idx;                //8
                unsigned int            parameters_off;                 //12
    };
#pragma pack(pop)
    //Proto_ids의 모든 데이터 저장
    std::vector<Proto_id_item>      m_proto_ids;
    //Proto_ids의 string으로 반환할때 조합이 많이 필요해서 미리 만들어서 저장
    std::vector<std::string>        m_proto_strings;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
    unsigned int __convert();
public:
    Proto_ids(){init_reference(eTYPE_PROTO_ID_ITEM, this);}
    virtual std::string as_string(unsigned int idx);

    void convert();
};

unsigned int Proto_ids::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    Base_dex_reference* ptype_lists = get_reference(eTYPE_TYPE_LIST);
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    if(ptype_ids == NULL || ptype_lists == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Proto_ids.__parssing() reference NULL Error !\n");
        return m_start_offset;
    }

    if(get_command_flag(eFLAG_LOG)){
        if(pstr_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Proto_ids.print_full reference NULL\n");
            return m_start_offset;
        }
        //  Log::print(Log::LOG_INFO, "**Proto ids Full\n");
         printf("**Proto ids Full\n");
        //  Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t IDX/OFF\t\t STRING\n");
         printf("OFFSET\t\t COUNT\t NAME\t\t\t\t IDX/OFF\t\t STRING\n");
        //  Log::print(Log::LOG_INFO, "-----------------------------------------------------------------------------------------\n");
         printf("-----------------------------------------------------------------------------------------\n");
    }

    m_proto_ids.reserve(m_offset_count);
    m_proto_strings.reserve(m_offset_count);

    for(unsigned int i = 0; i < m_offset_count; i++){
        Proto_ids::Proto_id_item proto;
        proto.shorty_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t shorty_idx     \t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.shorty_idx, pstr_ids->as_string(proto.shorty_idx).c_str());
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t shorty_idx     \t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.shorty_idx, pstr_ids->as_string(proto.shorty_idx).c_str());
        offset_revision += sizeof(u4);
        proto.return_type_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t return_type_idx\t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.return_type_idx, ptype_ids->as_string(proto.return_type_idx).c_str());
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t return_type_idx\t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.return_type_idx, ptype_ids->as_string(proto.return_type_idx).c_str());
        offset_revision += sizeof(u4);
        proto.parameters_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t parameters_off\t\t 0x%08X\t\t %s\n", m_start_offset + offset_revision, i, proto.parameters_off, ptype_lists->as_off_string(proto.parameters_off).c_str());
        if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t parameters_off\t\t 0x%08X\t\t %s\n", m_start_offset + offset_revision, i, proto.parameters_off, ptype_lists->as_off_string(proto.parameters_off).c_str());
        offset_revision += sizeof(u4);

        m_proto_ids.push_back(proto);

        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;

        //Proto_String
        std::string proto_string;
        proto_string.append("(");
        proto_string.append( ptype_lists->as_off_string(proto.parameters_off) );
        proto_string.append(")");
        proto_string.append( ptype_ids->as_string(proto.return_type_idx) );
        m_proto_strings.push_back(proto_string);
        //Proto_String
    }
    // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n\n");   
    if(get_command_flag(eFLAG_LOG)) printf("\n\n");   
    return m_start_offset + offset_revision;
}

unsigned int Proto_ids::__convert(){
    unsigned int offset_revision = 0;

    Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
    Base_dex_reference* ptype_lists = get_reference(eTYPE_TYPE_LIST);
    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    if(ptype_ids == NULL || ptype_lists == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Proto_ids.__convert() reference NULL Error !\n");
        return m_start_offset;
    }

    if(get_command_flag(eFLAG_LOG)){
        if(pstr_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Proto_ids.__convert reference NULL\n");
            return m_start_offset;
        }
         Log::print(Log::LOG_INFO, "**Convert Proto ids Full\n");
         Log::print(Log::LOG_INFO, "OFFSET\t\t COUNT\t NAME\t\t\t\t IDX/OFF\t\t STRING\n");
         Log::print(Log::LOG_INFO, "-----------------------------------------------------------------------------------------\n");
    }

    // m_proto_ids.reserve(m_offset_count);
    // m_proto_strings.reserve(m_offset_count);
    m_proto_strings.clear();

    for(unsigned int i = 0; i < m_proto_ids.size(); i++){
        Proto_ids::Proto_id_item &proto = m_proto_ids[i];
        *((u4*)(m_dex + m_start_offset + offset_revision)) = proto.shorty_idx;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t shorty_idx     \t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.shorty_idx, pstr_ids->as_string(proto.shorty_idx).c_str());
        // if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t shorty_idx     \t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.shorty_idx, pstr_ids->as_string(proto.shorty_idx).c_str());
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = proto.return_type_idx;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t return_type_idx\t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.return_type_idx, ptype_ids->as_string(proto.return_type_idx).c_str());
        // if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t return_type_idx\t %010d\t\t %s\n", m_start_offset + offset_revision, i, proto.return_type_idx, ptype_ids->as_string(proto.return_type_idx).c_str());
        offset_revision += sizeof(u4);
        *((u4*)(m_dex + m_start_offset + offset_revision)) = proto.parameters_off;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %04d\t parameters_off\t\t 0x%08X\t\t %s\n", m_start_offset + offset_revision, i, proto.parameters_off, ptype_lists->as_off_string(proto.parameters_off).c_str());
        // if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t %04d\t parameters_off\t\t 0x%08X\t\t %s\n", m_start_offset + offset_revision, i, proto.parameters_off, ptype_lists->as_off_string(proto.parameters_off).c_str());
        offset_revision += sizeof(u4);

        // m_proto_ids.push_back(proto);

        offset_revision = __align_offset(m_start_offset + offset_revision, 4) - m_start_offset;

        //Proto_String
        std::string proto_string;
        proto_string.append("(");
        proto_string.append( ptype_lists->as_off_string(proto.parameters_off) );
        proto_string.append(")");
        proto_string.append( ptype_ids->as_string(proto.return_type_idx) );
        m_proto_strings.push_back(proto_string);
        //Proto_String
    }
    // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n\n");   
    if(get_command_flag(eFLAG_LOG)) printf("\n\n");   
    return m_start_offset + offset_revision;
}

std::string Proto_ids::as_string(unsigned int idx){
    if( m_proto_strings.size() <= idx ){
        Log::print(Log::LOG_ERROR, "ERROR !! Proto_ids.as_string(idx[%d])\n", idx);
        return std::string("");
    }
    return m_proto_strings[idx];
}

void Proto_ids::convert(){
    if(! get_command_flag(eFLAG_NORMALIZATION) ){
        return;
    }

    Type_lists* ptype_lists = (Type_lists*)get_reference(eTYPE_TYPE_LIST);
    String_ids* pstr_ids = (String_ids*)get_reference(eTYPE_STRING_ID_ITEM);
    if(ptype_lists == NULL || pstr_ids == NULL){
        Log::print(Log::LOG_ERROR, "ERROR !! Proto_ids.convert() reference NULL Error !\n");
        return;
    }
    unsigned int limit_size = 50;
    ptype_lists->convert(limit_size);
    std::list<unsigned int> convert_offsets = ptype_lists->get_convert_offsets();

    if(convert_offsets.size() == 0) return;

    for(unsigned int i = 0; i < m_proto_ids.size(); i++){
        for(std::list<unsigned int>::iterator iter = convert_offsets.begin(); iter != convert_offsets.end(); iter++){
            if( m_proto_ids[i].parameters_off == (*iter) ){
                //string_ids 의 값을 조절해야함
                Log::print(Log::LOG_DEBUG, "__DD__PROTO_CONVERT String_convert idx[%d] limit_size[%d]\n", m_proto_ids[i].shorty_idx, limit_size);
                pstr_ids->convert(m_proto_ids[i].shorty_idx, limit_size);
            }
        }
        Log::print(Log::LOG_DEBUG, "__DD__PROTO_CONVERT Before 0x%08X\n", m_proto_ids[i].parameters_off);
        m_proto_ids[i].parameters_off = ptype_lists->get_symmetry_offset( m_proto_ids[i].parameters_off );
        Log::print(Log::LOG_DEBUG, "__DD__PROTO_CONVERT after 0x%08X\n", m_proto_ids[i].parameters_off);
    }
    __convert();
}





#endif  /* _IN_PROTO_IDS */