




#include "dex_common.h"
#include "dex_base_parssing.h"
#include "leb128.h"

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_DEBUG_OPCODE
#define _IN_DEBUG_OPCODE

class Base_dex_debug_opcode : public Base_dex_parssing{
protected:
    int m_opcode = 0;
public:
    enum{
        DBG_END_SEQUENCE                =0x00,
        DBG_ADVANCE_PC                  =0x01,
        DBG_ADVANCE_LINE                =0x02,
        DBG_START_LOCAL                 =0x03,
        DBG_START_LOCAL_EXTENDED        =0x04,
        DBG_END_LOCAL                   =0x05,
        DBG_RESTART_LOCAL               =0x06,
        DBG_SET_PROLOGUE_END            =0x07,
        DBG_SET_EPILOGUE_BEGIN          =0x08,
        DBG_SET_FILE                    =0x09,
        DBG_SPECIAL_OP_START            =0x0a,
        DBG_SPECIAL_OP_END              =0xff,
    };

    void init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count, int opcode){
        m_dex = dex;
        m_start_offset = start_offset;
        m_offset_count = offset_count;
        m_opcode = opcode;
        __init(dex, start_offset, offset_count);
    }
};


class Debug_opcode_END_SEQUENCE : public Base_dex_debug_opcode{
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\t\t\t debug_opcode.END_SEQUENCE\n");
        return m_start_offset + offset_revision;;
    }
};

class Debug_opcode_ADVANCE_PC : public Base_dex_debug_opcode{
private:
    ULEB128 addr_diff = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        unsigned char* tmp_dex = 0;
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            addr_diff = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t Debug_opcode_ADVANCE_PC.addr_diff\t %d\n", m_start_offset + offset_revision, addr_diff);
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_ADVANCE_LINE : public Base_dex_debug_opcode{
private:
    SLEB128 line_diff = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        unsigned char* tmp_dex = 0;
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            line_diff = readSignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_ADVANCE_LINE.line_diff\t %d\n", m_start_offset + offset_revision, line_diff);
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_START_LOCAL : public Base_dex_debug_opcode{
private:
    ULEB128 register_num = 0;
    ULEB128 name_idx = 0;
    ULEB128 type_idx = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        unsigned char* tmp_dex = 0;

        Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
        Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
        if(get_command_flag(eFLAG_LOG)){
            if(pstr_ids == NULL || ptype_ids == NULL){
                Log::print(Log::LOG_ERROR, "ERROR !! Debug_opcode_START_LOCAL.__parssing() reference is NULL\n");
                return m_start_offset;
            }
        }
        
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            register_num = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL.register_num\t %d\n", m_start_offset + offset_revision, register_num);
            offset_revision = tmp_dex - m_dex - m_start_offset;

            tmp_dex = m_dex + m_start_offset + offset_revision;
            name_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL.name_idx\t %d\t %s\n", m_start_offset + offset_revision, name_idx, pstr_ids->as_string(name_idx).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;

            tmp_dex = m_dex + m_start_offset + offset_revision;
            type_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL.type_idx\t %d\t %s\n", m_start_offset + offset_revision, type_idx, ptype_ids->as_string(type_idx).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_START_LOCAL_EXTENDED : public Base_dex_debug_opcode{
private:
    ULEB128 register_num = 0;
    ULEB128 name_idx = 0;
    ULEB128 type_idx = 0;
    ULEB128 sig_idx = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        unsigned char* tmp_dex = 0;

        Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
        Base_dex_reference* ptype_ids = get_reference(eTYPE_TYPE_ID_ITEM);
        if(get_command_flag(eFLAG_LOG)){
            if(pstr_ids == NULL || ptype_ids == NULL){
                Log::print(Log::LOG_ERROR, "ERROR !! Debug_opcode_START_LOCAL.__parssing() reference is NULL\n");
                return m_start_offset;
            }
        }
        
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            register_num = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL_EXTENDED.register_num\t %d\n", m_start_offset + offset_revision, register_num);
            offset_revision = tmp_dex - m_dex - m_start_offset;

            tmp_dex = m_dex + m_start_offset + offset_revision;
            name_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL_EXTENDED.name_idx\t %d\t %s\n", m_start_offset + offset_revision, name_idx, pstr_ids->as_string(name_idx).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;

            tmp_dex = m_dex + m_start_offset + offset_revision;
            type_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL_EXTENDED.type_idx\t %d\t %s\n", m_start_offset + offset_revision, type_idx, ptype_ids->as_string(type_idx).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            sig_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_START_LOCAL_EXTENDED.sig_idx\t %d\n", m_start_offset + offset_revision, sig_idx);
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_END_LOCAL : public Base_dex_debug_opcode{
private:
    ULEB128 register_num = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        unsigned char* tmp_dex = 0;
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            register_num = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_END_LOCAL.register_num\t %d\n", m_start_offset + offset_revision, register_num);
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_RESTART_LOCAL : public Base_dex_debug_opcode{
private:
    ULEB128 register_num = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        unsigned char* tmp_dex = 0;
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            register_num = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) printf("0x%08X\t\t Debug_opcode_RESTART_LOCAL.register_num\t %d\n", m_start_offset + offset_revision, register_num);
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_SET_PROLOGUE_END : public Base_dex_debug_opcode{
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\t\t\t debug_opcode_SET_PROLOGUE_END\n");
        return m_start_offset + offset_revision;;
    }
};

class Debug_opcode_SET_EPILOGUE_BEGIN : public Base_dex_debug_opcode{
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\t\t\t debug_opcode_SET_EPILOGUE_BEGIN\n");
        return m_start_offset + offset_revision;;
    }
};

class Debug_opcode_SET_FILE : public Base_dex_debug_opcode{
private:
    ULEB128 name_idx = 0;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;

        Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
        if(get_command_flag(eFLAG_LOG)){
            if(pstr_ids == NULL){
                Log::print(Log::LOG_ERROR, "ERROR !! Debug_opcode_SET_FILE.__parssing() refenrence is NULL\n");
                return m_start_offset;
            }
        }

        unsigned char* tmp_dex = 0;
        for(unsigned int i = 0; i < m_offset_count; i++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            name_idx = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Debug_opcode_SET_FILE.name_idx\t %d\t %s\n", m_start_offset + offset_revision, name_idx, pstr_ids->as_string(name_idx).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
        }
        return m_start_offset + offset_revision;
    }
};

class Debug_opcode_SPECIAL_OP : public Base_dex_debug_opcode{
protected:
    virtual unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        int adjusted = m_opcode - DBG_SPECIAL_OP_START;
        int address_diff = adjusted / 15;
        int line_diff = (adjusted % 15) - 4;

        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\t\t\t Debug_opcode_SPECIAL_OP.address_diff %d\n", address_diff);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\t\t\t Debug_opcode_SPECIAL_OP.line_diff %d\n", line_diff);
        return m_start_offset + offset_revision;
    }
};




#endif /* _IN_DEBUG_OPCODE */