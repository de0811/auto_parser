#include "dex_common.h"
#include "dex_base_parssing.h"

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_BASE_INSTRUCTION
#define _IN_BASE_INSTRUCTION

class Base_instruction : public Base_dex_parssing{
protected:
unsigned int        m_type_instruction;

// unsigned short      inst_a

public:
enum{
    ARG_NONE,
    ARG_00x ,   //Baksmali에선 없음
    ARG_10x ,
    ARG_12x ,
    ARG_11n ,
    ARG_11x ,
    ARG_10t ,
    ARG_20t ,
    ARG_20bc,
    ARG_22x ,
    ARG_21t ,
    ARG_21s ,
    ARG_21h , //?????? Baksmali에선 없음....?
    ARG_21ih, //Baksmali에선 있음
    ARG_21lh, //Baksmali에선 있음
    ARG_21c ,
    ARG_23x ,
    ARG_22b ,
    ARG_22t ,
    ARG_22s ,
    ARG_22c ,
    ARG_22cs,
    ARG_30t ,
    ARG_32x ,
    ARG_31i ,
    ARG_31t ,
    ARG_31c ,
    ARG_35c ,
    ARG_35ms,
    ARG_35mi,
    ARG_3rc ,
    ARG_3rms,
    ARG_3rmi,
    ARG_45cc,
    ARG_4rcc,
    ARG_51l ,
    ARG_PACKED_SWITCH_PAYLOAD,
    ARG_SPARSE_SWITCH_PAYLOAD,
    ARG_FILL_ARRAY_DATA_PAYLOAD,
};
};

class Util_nibble{
public:
/*
    int extractHighSignedNibble(int value) {
        return (value << 24) >> 28;
    }
    int extractLowSignedNibble(int value) {
        return (value << 28) >> 28;
    }
    int extractHighUnsignedNibble(int value) {
        return (value & 0xF0) >>> 4;
    }
    int extractLowUnsignedNibble(int value) {
        return value & 0x0F;
    }
*/
    inline static unsigned char get_byte_hight(unsigned char value){
        return (value & 0xF0) >> 4;
    }
    inline static unsigned char get_byte_low(unsigned char value) {
        return value & 0x0F;
    }
};

//-------------------------------------------------------------------------------------------------------

class Instruction_00x : public Base_instruction{
    //N/A
    unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        //Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_00x\n");
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_00x\n", m_start_offset + offset_revision);

        offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
        return m_start_offset + offset_revision;
    }
};

//-------------------------------------------------------------------------------------------------------


class Instruction_10t : public Base_instruction{
    //op +AA
    //sign-extend 8-bit value
private:
signed char m_code_offset;
protected:
    unsigned int __parssing(unsigned int _command_flag);
};

unsigned int Instruction_10t::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_code_offset = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    //Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_10t.m_code_offset\t 0x%02X\n", m_code_offset);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_10t.m_code_offset\t 0x%02X\n", m_start_offset + offset_revision, m_code_offset);
    offset_revision += sizeof(m_code_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}


//-------------------------------------------------------------------------------------------------------

class Instruction_10x : public Base_instruction{
    //op
    unsigned char m_padding;
    unsigned int __parssing(unsigned int _command_flag){
        unsigned int offset_revision = 0;
        m_padding = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
        // Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_10x.m_padding\t 0x%02X\n", m_padding);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_10x.m_padding\t 0x%02X\n", m_start_offset + offset_revision, m_padding);
        offset_revision += sizeof(m_padding);

        offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
        return m_start_offset + offset_revision;
    }
};

//-------------------------------------------------------------------------------------------------------

class Instruction_11n : public Base_instruction, public Util_nibble{
    //op vA, #+B
private:
    unsigned char m_reg_A;
    signed char m_literal;
protected:
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_11n::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char tmp_data = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );

    m_reg_A = get_byte_low(tmp_data);
    m_literal = get_byte_hight(tmp_data);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_11n.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_11n.m_literal\t 0x%02X\n", m_start_offset + offset_revision, m_literal);
    
    offset_revision += sizeof(tmp_data);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_11x : public Base_instruction{
    //op vAA
private:
    unsigned char m_reg_A;
protected:
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_11x::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_11x.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(unsigned char);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_12x : public Base_instruction, public Util_nibble{
    //op vA, vB
private:
    unsigned char m_reg_A;
    unsigned char m_reg_B;
protected:
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_12x::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char tmp_data = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );

    m_reg_A = get_byte_low(tmp_data);
    m_reg_B = get_byte_hight(tmp_data);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_12x.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_12x.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);

    offset_revision += sizeof(tmp_data);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_20bc : public Base_instruction{
    //[opt] op AA, thing@BBBB
    unsigned char m_ref_type = 0;
    unsigned short m_reference1 = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_20bc::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_ref_type = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_20bc.m_ref_type\t 0x%02X\n", m_start_offset + offset_revision, m_ref_type);
    offset_revision += sizeof(m_ref_type);
    m_reference1 = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_20bc.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_20t : public Base_instruction{
    //op +AAAA
    unsigned char m_padding = 0;
    signed short m_code_offset = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_20t::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_padding = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_20t.m_padding\t 0x%02X\n", m_start_offset + offset_revision, m_padding);
    offset_revision += sizeof(m_padding);
    m_code_offset = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_20t.m_code_offset\t 0x%04X\n", m_start_offset + offset_revision, m_code_offset);
    offset_revision += sizeof(m_code_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_21c : public Base_instruction{
    //op vAA, thing@BBBB
    unsigned char m_reg_A = 0;
    unsigned short m_reference1 = 0;

    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_21c::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21c.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_reference1 = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21c.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;   
}

//-------------------------------------------------------------------------------------------------------
class Instruction_21h : public Base_instruction{
    // op vAA, #+BBBB0000[00000000]
    unsigned char m_reg_A = 0;
    unsigned short m_literal = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_21h::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21h.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_literal = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21h.m_literal\t 0x%04X\n", m_start_offset + offset_revision, m_literal);
    offset_revision += sizeof(m_literal);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

class Instruction_21ih : public Base_instruction{
    //baksmali에서는 
    // op vAA, #+BBBB0000
    // op vAA, #+BBBB000000000000
    //이 두가지 때문에 21ih와 21lh를 나누는 것 같음
};

class Instruction_21lh : public Base_instruction{

};

//-------------------------------------------------------------------------------------------------------

class Instruction_21s : public Base_instruction{
    //op vAA, #+BBBB
    unsigned char m_reg_A = 0;
    signed short m_literal = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_21s::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21s.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_literal = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21s.m_literal\t 0x%02X\n", m_start_offset + offset_revision, m_literal);
    offset_revision += sizeof(m_literal);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_21t : public Base_instruction{
    //op vAA, +BBBB
    unsigned char m_reg_A = 0;
    signed short m_code_offset = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_21t::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21t.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_code_offset = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_21t.m_code_offset\t 0x%04X\n", m_start_offset + offset_revision, m_code_offset);
    offset_revision += sizeof(m_code_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}
//-------------------------------------------------------------------------------------------------------

class Instruction_22b : public Base_instruction{
    //op vAA, vBB, #+CC
    unsigned char m_reg_A = 0;
    unsigned char m_reg_B = 0;
    signed char m_literal = 0;

    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_22b::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22b.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_reg_B = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22b.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(m_reg_B);
    m_literal = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22b.m_literal\t 0x%02X\n", m_start_offset + offset_revision, m_literal);
    offset_revision += sizeof(m_literal);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_22c : public Base_instruction, public Util_nibble{
    //op vA, vB, thing@CCCC
    unsigned char m_reg_A = 0;
    unsigned char m_reg_B = 0;
    unsigned short m_reference1 = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_22c::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char tmp_data = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );

    m_reg_A = get_byte_low(tmp_data);
    m_reg_B = get_byte_hight(tmp_data);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22c.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22c.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(tmp_data);

    m_reference1 = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22c.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_22cs : public Base_instruction, public Util_nibble{
    //[opt] op vA, vB, field offset CCCC
    unsigned char m_reg_A = 0;
    unsigned char m_reg_B = 0;
    unsigned short m_field_offset = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_22cs::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char tmp_data = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );

    m_reg_A = get_byte_low(tmp_data);
    m_reg_B = get_byte_hight(tmp_data);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22cs.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22cs.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(tmp_data);

    m_field_offset = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22cs.m_field_offset\t 0x%04X\n", m_start_offset + offset_revision, m_field_offset);
    offset_revision += sizeof(m_field_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}
//-------------------------------------------------------------------------------------------------------

class Instruction_22s : public Base_instruction, public Util_nibble{
    //op vA, vB, #+CCCC
    unsigned char m_reg_A = 0;
    unsigned char m_reg_B = 0;
    signed short m_literal = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_22s::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char tmp_data = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );

    m_reg_A = get_byte_low(tmp_data);
    m_reg_B = get_byte_hight(tmp_data);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22s.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22s.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(tmp_data);

    m_literal = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22s.m_literal\t 0x%04X\n", m_start_offset + offset_revision, m_literal);
    offset_revision += sizeof(m_literal);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_22t : public Base_instruction, public Util_nibble{
    //op vA, vB, +CCCC
    unsigned char m_reg_A = 0;
    unsigned char m_reg_B = 0;
    signed short m_code_offset = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_22t::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char tmp_data = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );

    m_reg_A = get_byte_low(tmp_data);
    m_reg_B = get_byte_hight(tmp_data);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22t.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22t.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(tmp_data);

    m_code_offset = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22t.m_code_offset\t 0x%02X\n", m_start_offset + offset_revision, m_code_offset);
    offset_revision += sizeof(m_code_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}
//-------------------------------------------------------------------------------------------------------

class Instruction_22x : public Base_instruction{
    // op vAA, vBBBB
    unsigned char m_reg_A = 0;
    unsigned short m_reg_B = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_22x::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22x.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_reg_B = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_22x.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(m_reg_B);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_23x : public Base_instruction{
    // op vAA, vBB, vCC
    unsigned char m_reg_A = 0;
    unsigned char m_reg_B = 0;
    unsigned char m_reg_C = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_23x::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_23x.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_reg_B = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_23x.m_reg_B\t 0x%02X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(m_reg_B);
    m_reg_C = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_23x.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
    offset_revision += sizeof(m_reg_C);
    
    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_30t : public Base_instruction{
    // op +AAAAAAAA
    unsigned char m_padding = 0;
    signed int m_code_offset = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_30t::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_padding = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_30t.m_padding\t 0x%02X\n", m_start_offset + offset_revision, m_padding);
    offset_revision += sizeof(m_padding);

    m_code_offset = *( (signed int*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_30t.m_code_offset\t 0x%08X\n", m_start_offset + offset_revision, m_code_offset);
    offset_revision += sizeof(m_code_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_31c : public Base_instruction{
    // op vAA, string@BBBBBBBB
    unsigned char m_reg_A = 0;
    unsigned int m_reference1 = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_31c::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_31c.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_reference1 = *( (unsigned int*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_31c.m_reference1\t 0x%08X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_31i : public Base_instruction{
    // op vAA, #+BBBBBBBB
    unsigned char m_reg_A = 0;
    signed int m_literal = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_31i::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_31i.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_literal = *( (signed int*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_31i.m_literal\t 0x%08X\n", m_start_offset + offset_revision, m_literal);
    offset_revision += sizeof(m_literal);
    
    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_31t : public Base_instruction{
    // op vAA, +BBBBBBBB
    unsigned char m_reg_A = 0;
    unsigned int m_code_offset = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_31t::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_reg_A = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_31t.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_code_offset = *( (unsigned int*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_31t.m_code_offset\t 0x%08X\n", m_start_offset + offset_revision, m_code_offset);
    offset_revision += sizeof(m_code_offset);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_32x : public Base_instruction{
    // op vAAAA, vBBBB
    unsigned char m_padding = 0;
    unsigned short m_reg_A = 0;
    unsigned short m_reg_B = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_32x::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    m_padding = *( (unsigned char*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_32x.m_padding\t 0x%02X\n", m_start_offset + offset_revision, m_padding);
    offset_revision += sizeof(m_padding);
    m_reg_A = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_32x.m_reg_A\t 0x%04X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_reg_B = *( (unsigned short*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_32x.m_reg_B\t 0x%04X\n", m_start_offset + offset_revision, m_reg_B);
    offset_revision += sizeof(m_reg_B);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_35c : public Base_instruction, public Util_nibble{
    // op {vC, vD, vE, vF, vG}, thing@BBBB
    /*
        [A=5] op {vC, vD, vE, vF, vG}, meth@BBBB
        [A=5] op {vC, vD, vE, vF, vG}, site@BBBB
        [A=5] op {vC, vD, vE, vF, vG}, type@BBBB
        [A=4] op {vC, vD, vE, vF}, kind@BBBB
        [A=3] op {vC, vD, vE}, kind@BBBB
        [A=2] op {vC, vD}, kind@BBBB
        [A=1] op {vC}, kind@BBBB
        [A=0] op {}, kind@BBBB
    */
    u1 m_count = 0;
    u1 m_reg_G = 0;
    u2 m_reference1 = 0;
    u1 m_reg_C = 0;
    u1 m_reg_D = 0;
    u1 m_reg_E = 0;
    u1 m_reg_F = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_35c::__parssing(unsigned int _command_flag){
    //bit position
    //0000 0000     0000 0000     0000 0000     0000 0000
    //count G        reference     C    D        E    F
    //  0   1                      2    3        4    5
    unsigned int offset_revision = 0;

    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) );
    m_count = get_byte_hight(m_count);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);

    if(m_count > 5){
            Log::print(Log::LOG_ERROR, "ERROR !! Instruction_35c Error!! m_count[%d]\n", m_count);
            m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) );
            m_count = get_byte_low(m_count);
            if(get_command_flag(eFLAG_NORMALIZATION)){
                *( (u1*)(m_dex + m_start_offset + offset_revision) ) = m_count;//5 이상의 값을 0으로 변경해줌
                m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) );
            }
            m_count = get_byte_hight(m_count);
            Log::print(Log::LOG_ERROR, "ERROR !! Instruction_35c Error!! -- Modify m_count[%d]\n", m_count);
    }
    if(get_command_flag(eFLAG_OBFUSCATION)){    //이후 추가해야겠지?
        if(m_count == 0){

        }
    }


    if(m_count == 5){
        m_reg_G = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_G = get_byte_low(m_reg_G);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_G\t 0x%02X\n", m_start_offset + offset_revision, m_reg_G);
    }
    offset_revision += sizeof(u1);

    m_reference1 = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if( m_count == 0 && m_reference1 == 0){
        Log::print(Log::LOG_ERROR, "ERROR !! Instruction_35c m_reference1 value 0\n");
        /*
        if(get_command_flag(eFLAG_NORMALIZATION)){
            *( (u2*)(m_dex + m_start_offset + offset_revision) ) = 0;
            *( (u2*)(m_dex + m_start_offset + (offset_revision - sizeof(u2))) ) = 0;
            Log::print(Log::LOG_ERROR, "ERROR !! Instruction_35c Normalization\n");
            return m_start_offset + (offset_revision - sizeof(u2));
        }
        */
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    if(m_count == 1){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        offset_revision += sizeof(u1);
    }
    if(m_count == 2){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
    }
    if(m_count == 3){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        offset_revision += sizeof(u1);
    }
    if(m_count >= 4){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        m_reg_F = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_F = get_byte_low(m_reg_F);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35c.m_reg_F\t 0x%02X\n", m_start_offset + offset_revision, m_reg_F);
        offset_revision += sizeof(u1);
    }

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_35ms : public Base_instruction, public Util_nibble{
    // [opt] invoke-virtual+super
    /*
        [A=5] op {vC, vD, vE, vF, vG}, vtaboff@BBBB
        [A=4] op {vC, vD, vE, vF}, vtaboff@BBBB
        [A=3] op {vC, vD, vE}, vtaboff@BBBB
        [A=2] op {vC, vD}, vtaboff@BBBB
        [A=1] op {vC}, vtaboff@BBBB
    */
    u1 m_count = 0;
    u1 m_reg_G = 0;
    u2 m_vtable_idx = 0;
    u1 m_reg_C = 0;
    u1 m_reg_D = 0;
    u1 m_reg_E = 0;
    u1 m_reg_F = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_35ms::__parssing(unsigned int _command_flag){
    //bit position
    //0000 0000     0000 0000     0000 0000     0000 0000
    //count G       vtable_idx     C    D        E    F
    unsigned int offset_revision = 0;

    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) );
    m_count = get_byte_hight(m_count);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);

    if(m_count == 5){
        m_reg_G = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_G = get_byte_low(m_reg_G);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_G\t 0x%02X\n", m_start_offset + offset_revision, m_reg_G);
    }
    offset_revision += sizeof(u1);

    m_vtable_idx = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_vtable_idx\t 0x%04X\n", m_start_offset + offset_revision, m_vtable_idx);
    offset_revision += sizeof(m_vtable_idx);

    if(m_count == 1){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        offset_revision += sizeof(u1);
    }
    if(m_count == 2){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
    }
    if(m_count == 3){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        offset_revision += sizeof(u1);
    }
    if(m_count >= 4){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        m_reg_F = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_F = get_byte_low(m_reg_F);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35ms.m_reg_F\t 0x%02X\n", m_start_offset + offset_revision, m_reg_F);
        offset_revision += sizeof(u1);
    }
    if(m_count > 5 || m_count == 0){
        // Log::print(Log::LOG_INFO, "Instruction_35ms Error!! m_count[%d]\n", m_count);
        Log::print(Log::LOG_ERROR, "Instruction_35ms Error!! m_count[%d]\n", m_count);
    }

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}
//-------------------------------------------------------------------------------------------------------

class Instruction_35mi : public Base_instruction, public Util_nibble{
    // [opt] inline invoke
    /*
        [A=5] op {vC, vD, vE, vF, vG}, inline@BBBB
        [A=4] op {vC, vD, vE, vF}, inline@BBBB
        [A=3] op {vC, vD, vE}, inline@BBBB
        [A=2] op {vC, vD}, inline@BBBB
        [A=1] op {vC}, inline@BBBB
    */
    u1 m_count = 0;
    u1 m_reg_G = 0;
    u2 m_inline_idx = 0;
    u1 m_reg_C = 0;
    u1 m_reg_D = 0;
    u1 m_reg_E = 0;
    u1 m_reg_F = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_35mi::__parssing(unsigned int _command_flag){
    //bit position
    //0000 0000     0000 0000     0000 0000     0000 0000
    //count C       inline_idx     D    E        F    G
    unsigned int offset_revision = 0;

    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) );
    m_count = get_byte_hight(m_count);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);

    if(m_count == 5){
        m_reg_G = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_G = get_byte_low(m_reg_G);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_G\t 0x%02X\n", m_start_offset + offset_revision, m_reg_G);
    }
    offset_revision += sizeof(u1);

    m_inline_idx = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_inline_idx\t 0x%04X\n", m_start_offset + offset_revision, m_inline_idx);
    offset_revision += sizeof(m_inline_idx);

    if(m_count == 1){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        offset_revision += sizeof(u1);
    }
    if(m_count == 2){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
    }
    if(m_count == 3){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        offset_revision += sizeof(u1);
    }
    if(m_count >= 4){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        m_reg_F = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_F = get_byte_low(m_reg_F);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_35mi.m_reg_F\t 0x%02X\n", m_start_offset + offset_revision, m_reg_F);
        offset_revision += sizeof(u1);
    }
    if(m_count > 5 || m_count == 0){
        // Log::print(Log::LOG_INFO, "Instruction_35mi Error!! m_count[%d]\n", m_count);
        Log::print(Log::LOG_ERROR, "Instruction_35mi Error!! m_count[%d]\n", m_count);
    }

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}
//-------------------------------------------------------------------------------------------------------

class Instruction_3rc : public Base_instruction{
    // op {vCCCC .. v(CCCC+AA-1)}, meth@BBBB
    /*
        op {vCCCC .. vNNNN}, meth@BBBB
        op {vCCCC .. vNNNN}, site@BBBB
        op {vCCCC .. vNNNN}, type@BBBB
        where NNNN = CCCC+AA-1, that is A determines the count 0..255, and C determines the first register
    */
    u1 m_count = 0;
    u2 m_reference1 = 0;
    u2 m_start_reg = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_3rc::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    
    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rc.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);
    offset_revision += sizeof(m_count);
    m_reference1 = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rc.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    m_start_reg = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rc.m_start_reg\t 0x%04X\n", m_start_offset + offset_revision, m_start_reg);
    offset_revision += sizeof(m_start_reg);


    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_3rmi : public Base_instruction{
    // [opt] execute-inline/range
    u1 m_count = 0;
    u2 m_inline_idx = 0;
    u2 m_start_reg = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_3rmi::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    
    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rmi.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);
    offset_revision += sizeof(m_count);
    m_inline_idx = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rmi.m_inline_idx\t 0x%04X\n", m_start_offset + offset_revision, m_inline_idx);
    offset_revision += sizeof(m_inline_idx);

    m_start_reg = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rmi.m_start_reg\t 0x%04X\n", m_start_offset + offset_revision, m_start_reg);
    offset_revision += sizeof(m_start_reg);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_3rms : public Base_instruction{
    // [opt] invoke-virtual+super/range
    u1 m_count = 0;
    u2 m_vtable_idx = 0;
    u2 m_start_reg = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_3rms::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    
    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rms.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);
    offset_revision += sizeof(m_count);
    m_vtable_idx = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rms.m_vtable_idx\t 0x%02X\n", m_start_offset + offset_revision, m_vtable_idx);
    offset_revision += sizeof(m_vtable_idx);

    m_start_reg = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_3rms.m_start_reg\t 0x%04X\n", m_start_offset + offset_revision, m_start_reg);
    offset_revision += sizeof(m_start_reg);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_45cc : public Base_instruction, public Util_nibble{
    /*
    [A=5] op {vC, vD, vE, vF, vG}, meth@BBBB, proto@HHHH
    [A=4] op {vC, vD, vE, vF}, meth@BBBB, proto@HHHH
    [A=3] op {vC, vD, vE}, meth@BBBB, proto@HHHH
    [A=2] op {vC, vD}, meth@BBBB, proto@HHHH
    [A=1] op {vC}, meth@BBBB, proto@HHHH	
    */
    u1 m_count = 0;
    u1 m_reg_C = 0;
    u2 m_reference1 = 0;
    u1 m_reg_D = 0;
    u1 m_reg_E = 0;
    u1 m_reg_F = 0;
    u1 m_reg_G = 0;
    u2 m_reference2 = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_45cc::__parssing(unsigned int _command_flag){
    //bit position
    //0000 0000     0000 0000     0000 0000     0000 0000       0000 0000
    //count G        reference     C    D        E    F          reference2
    //  0   5                      1    2        3    4
    unsigned int offset_revision = 0;

    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) );
    m_count = get_byte_hight(m_count);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);

    if(m_count == 5){
        m_reg_G = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_G = get_byte_low(m_reg_G);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_G\t 0x%02X\n", m_start_offset + offset_revision, m_reg_G);
    }
    offset_revision += sizeof(u1);

    m_reference1 = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);

    if(m_count == 1){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        offset_revision += sizeof(u1);
    }
    if(m_count == 2){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
    }
    if(m_count == 3){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        offset_revision += sizeof(u1);
    }
    if(m_count >= 4){
        m_reg_C = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_C = get_byte_hight(m_reg_C);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_C\t 0x%02X\n", m_start_offset + offset_revision, m_reg_C);
        m_reg_D = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_D = get_byte_low(m_reg_D);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_D\t 0x%02X\n", m_start_offset + offset_revision, m_reg_D);
        offset_revision += sizeof(u1);
        m_reg_E = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_E = get_byte_hight(m_reg_E);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_E\t 0x%02X\n", m_start_offset + offset_revision, m_reg_E);
        m_reg_F = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_reg_F = get_byte_low(m_reg_F);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reg_F\t 0x%02X\n", m_start_offset + offset_revision, m_reg_F);
        offset_revision += sizeof(u1);
    }
    if(m_count > 5 || m_count == 0){
        // Log::print(Log::LOG_INFO, "Instruction_45cc Error!! m_count[%d]\n", m_count);
        Log::print(Log::LOG_ERROR, "Instruction_45cc Error!! m_count[%d]\n", m_count);
    }

    offset_revision = 6;
    m_reference2 = *( (u2*)(m_dex + m_start_offset + offset_revision) );
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_45cc.m_reference2\t 0x%04X\n", m_start_offset + offset_revision, m_reference2);
    offset_revision += sizeof(m_reference2);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_4rcc : public Base_instruction{
    // AA op BBBB CCCC HHHH
    /*
        op> {vCCCC .. vNNNN}, meth@BBBB, proto@HHHH
        wheere NNNN = CCCC+AA-1, that is A determines the count 0..255, and C determines the first register
    */
    u1 m_count = 0;
    u2 m_reference1 = 0;
    std::vector<u1> m_regs;
    u2 m_reference2 = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_4rcc::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_count = *( (u1*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_4rcc.m_count\t 0x%02X\n", m_start_offset + offset_revision, m_count);
    offset_revision += sizeof(m_count);
    m_reference1 = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_4rcc.m_reference1\t 0x%04X\n", m_start_offset + offset_revision, m_reference1);
    offset_revision += sizeof(m_reference1);
    if( m_count > 4 ){
        // Log::print(Log::LOG_INFO, "Instruction_4rcc m_count Error[%d]\n", m_count);
        Log::print(Log::LOG_ERROR, "Instruction_4rcc m_count Error[%d]\n", m_count);
    }
    m_regs.reserve(m_count);
    u1 tmp_reg = 0;
    for(int i = 0; i < m_count; i++){
        tmp_reg = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_regs.push_back(  tmp_reg  );
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_4rcc.m_regs\t 0x%04X\n", m_start_offset + offset_revision, tmp_reg);
        offset_revision += sizeof(tmp_reg);
    }
    m_reference2 = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_4rcc.m_reference2\t 0x%04X\n", m_start_offset + offset_revision, m_reference2);
    offset_revision += sizeof(m_reference2);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_51l : public Base_instruction{
    // op vAA, #+BBBBBBBBBBBBBBBB
    u1 m_reg_A = 0;
    u8 m_literal = 0;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_51l::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    
    m_reg_A = *( (u1*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_51l.m_reg_A\t 0x%02X\n", m_start_offset + offset_revision, m_reg_A);
    offset_revision += sizeof(m_reg_A);
    m_literal = *( (u8*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_51l.m_literal\t 0x%16X\n", m_start_offset + offset_revision, m_literal);
    offset_revision += sizeof(m_literal);

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_packed_switch_payload : public Base_instruction{
    u2 m_size = 0;
    s4 m_first_key = 0;
    std::vector<s4> m_targets;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_packed_switch_payload::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_size = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_packed_switch_payload.m_size\t 0x%04X\n", m_start_offset + offset_revision, m_size);
    offset_revision += sizeof(m_size);
    m_first_key = *( (u4*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_packed_switch_payload.m_first_key\t 0x%08X\n", m_start_offset + offset_revision, m_first_key);
    offset_revision += sizeof(m_first_key);
    m_targets.reserve(m_size);
    s4 tmp_target = 0;
    for(int i = 0; i < m_size; i++){
        tmp_target = *( (u4*)(m_dex + m_start_offset + offset_revision) );
        m_targets.push_back(  tmp_target  );
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_packed_switch_payload.m_targets\t 0x%08X\n", m_start_offset + offset_revision, tmp_target);
        offset_revision += sizeof(tmp_target);
    }

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_sparese_switch_payload : public Base_instruction{
    u2 m_size = 0;
    std::vector<s4> m_keys;
    std::vector<s4> m_targets;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_sparese_switch_payload::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_size = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_sparese_switch_payload.m_size\t 0x%04X\n", m_start_offset + offset_revision, m_size);
    offset_revision += sizeof(m_size);

    m_keys.reserve(m_size);
    s4 tmp_key = 0;
    for(int i = 0; i < m_size; i++){
        tmp_key = *( (s4*)(m_dex + m_start_offset + offset_revision) );
        m_keys.push_back(  tmp_key  );
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_sparese_switch_payload.m_keys\t 0x%08X\n", m_start_offset + offset_revision, tmp_key);
        offset_revision += sizeof(tmp_key);
    }
    s4 tmp_target = 0;
    m_targets.reserve(m_size);
    for(int i = 0; i < m_size; i++){
        tmp_target = *( (s4*)(m_dex + m_start_offset + offset_revision) );
        m_targets.push_back(  tmp_target  );
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_sparese_switch_payload.m_targets\t 0x%08X\n", m_start_offset + offset_revision, tmp_target);
        offset_revision += sizeof(tmp_target);
    }

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------

class Instruction_fill_array_data_payload : public Base_instruction{
    u2 m_element_width = 0;
    u4 m_size = 0;
    std::vector<u1> m_datas;
    unsigned int __parssing(unsigned int _command_flag);
};
unsigned int Instruction_fill_array_data_payload::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    m_element_width = *( (u2*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_fill_array_data_payload.m_element_width\t 0x%04X\n", m_start_offset + offset_revision, m_element_width);
    offset_revision += sizeof(u2);
    m_size = *( (u4*)(m_dex + m_start_offset + offset_revision) ); 
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_fill_array_data_payload.m_size\t 0x%08X\n", m_start_offset + offset_revision, m_size);
    offset_revision += sizeof(u4);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_DEBUG, "m_offset_count : %d[0x%08X],  offset_revision : %d[0x%08X]\n", m_offset_count, m_offset_count , offset_revision, offset_revision);

    if( m_element_width == 0 ){
        Log::print(Log::LOG_ERROR, "ERROR !! Instruction_fill_array_data_payload.m_element_width[%d] / m_size[%d]\n", m_element_width, m_size);
        offset_revision -= sizeof(u4);
        offset_revision -= sizeof(u2);
        m_element_width = 1;
        if(get_command_flag(eFLAG_NORMALIZATION)){
            *( (u2*)(m_dex + m_start_offset + offset_revision) ) = m_element_width;
            m_element_width = *( (u2*)(m_dex + m_start_offset + offset_revision) );
        }
        offset_revision += sizeof(u2);

        Log::print(Log::LOG_ERROR, "m_offset_count : %d[0x%08X],  offset_revision : %d[0x%08X]\n", m_offset_count, m_offset_count , offset_revision, offset_revision);
        m_size = (m_offset_count - offset_revision) - sizeof(u4); //sizeof(u4)인 이유는 m_size길이를 계산을 제외하기 위해서임
        if(get_command_flag(eFLAG_NORMALIZATION)){
            Log::print(Log::LOG_ERROR, "ERROR !! **-Modify Instruction_fill_array_data_payload.m_element_width[%d] / m_size[%d]\n", m_element_width, m_size);
            *( (u4*)(m_dex + m_start_offset + offset_revision) ) = m_size;
            m_size = *( (u4*)(m_dex + m_start_offset + offset_revision) );
        }

        offset_revision += sizeof(u4);
        Log::print(Log::LOG_ERROR, "ERROR !! -Modify Instruction_fill_array_data_payload.m_element_width[%d] / m_size[%d]\n", m_element_width, m_size);
        Log::print(Log::LOG_ERROR, "ERROR !! schedule end [0x%08X]\n", m_start_offset + m_offset_count);
    }
    else if( get_command_flag(eFLAG_OBFUSCATION) && m_element_width == 1 && m_size == 0){
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "Instruction_fill_array_data_payload.m_element_width[%d] / m_size[%d]\n", m_element_width, m_size);
        offset_revision -= sizeof(u4);
        offset_revision -= sizeof(u2);
        m_element_width = 0;
        *( (u2*)(m_dex + m_start_offset + offset_revision) ) = m_element_width;
        m_element_width = *( (u2*)(m_dex + m_start_offset + offset_revision) );
        offset_revision += sizeof(u2);

        // m_size = (m_offset_count - offset_revision);
        m_size = 1111;
        *( (u4*)(m_dex + m_start_offset + offset_revision) ) = m_size;
        m_size = *( (u4*)(m_dex + m_start_offset + offset_revision) );
        offset_revision += sizeof(u4);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "-Modify Instruction_fill_array_data_payload.m_element_width[%d] / m_size[%d]\n", m_element_width, m_size);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_ERROR, "schedule end [0x%08X]\n", m_start_offset + m_offset_count);
    }

    m_datas.reserve(m_element_width * m_size);
    u1 tmp_data = 0;
    for(unsigned int i = 0; i < m_element_width * m_size; i++){
        tmp_data = *( (u1*)(m_dex + m_start_offset + offset_revision) );
        m_datas.push_back(  tmp_data  );
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t\t Instruction_fill_array_data_payload.m_datas\t 0x%02X\n", m_start_offset + offset_revision, tmp_data);
        offset_revision += sizeof(tmp_data);
    }

   if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_DEBUG, "END Instruction OFFSET -- [0x%08X]\n", __align_offset(m_start_offset + offset_revision, 2));

    offset_revision = __align_offset(m_start_offset + offset_revision, 2) - m_start_offset;
    return m_start_offset + offset_revision;
}

//-------------------------------------------------------------------------------------------------------







#endif  /* _IN_BASE_INSTRUCTION */