
#include <map>
#include <string>
#include "dex_base_parssing.h"
#include "dex_base_instruction.h"

// OPCODE == Instruction
class Opcode : public Base_dex_parssing{
public:
    struct Opinfo {
        public :
        unsigned short          type;
        std::string                   name;
        unsigned int            arg_type;
        Opinfo(){}
        Opinfo(unsigned short type, std::string name, unsigned int arg_type){
            this->type = type;
            this->name = name;
            this->arg_type = arg_type;
        }
    };
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
private:
    static std::map<unsigned short, Opinfo> *m_opcodes;
    Opinfo                                  m_opinfo;
    Base_instruction                        *m_instruction;
public:
enum {
    // BEGIN(libdex-opcode-enum); GENERATED AUTOMATICALLY BY opcode-gen
    OP_NOP                          = 0x00,
    OP_MOVE                         = 0x01,
    OP_MOVE_FROM16                  = 0x02,
    OP_MOVE_16                      = 0x03,
    OP_MOVE_WIDE                    = 0x04,
    OP_MOVE_WIDE_FROM16             = 0x05,
    OP_MOVE_WIDE_16                 = 0x06,
    OP_MOVE_OBJECT                  = 0x07,
    OP_MOVE_OBJECT_FROM16           = 0x08,
    OP_MOVE_OBJECT_16               = 0x09,
    OP_MOVE_RESULT                  = 0x0a,
    OP_MOVE_RESULT_WIDE             = 0x0b,
    OP_MOVE_RESULT_OBJECT           = 0x0c,
    OP_MOVE_EXCEPTION               = 0x0d,
    OP_RETURN_VOID                  = 0x0e,
    OP_RETURN                       = 0x0f,
    OP_RETURN_WIDE                  = 0x10,
    OP_RETURN_OBJECT                = 0x11,
    OP_CONST_4                      = 0x12,
    OP_CONST_16                     = 0x13,
    OP_CONST                        = 0x14,
    OP_CONST_HIGH16                 = 0x15,
    OP_CONST_WIDE_16                = 0x16,
    OP_CONST_WIDE_32                = 0x17,
    OP_CONST_WIDE                   = 0x18,
    OP_CONST_WIDE_HIGH16            = 0x19,
    OP_CONST_STRING                 = 0x1a,
    OP_CONST_STRING_JUMBO           = 0x1b,
    OP_CONST_CLASS                  = 0x1c,
    OP_MONITOR_ENTER                = 0x1d,
    OP_MONITOR_EXIT                 = 0x1e,
    OP_CHECK_CAST                   = 0x1f,
    OP_INSTANCE_OF                  = 0x20,
    OP_ARRAY_LENGTH                 = 0x21,
    OP_NEW_INSTANCE                 = 0x22,
    OP_NEW_ARRAY                    = 0x23,
    OP_FILLED_NEW_ARRAY             = 0x24,
    OP_FILLED_NEW_ARRAY_RANGE       = 0x25,
    OP_FILL_ARRAY_DATA              = 0x26,
    OP_THROW                        = 0x27,
    OP_GOTO                         = 0x28,
    OP_GOTO_16                      = 0x29,
    OP_GOTO_32                      = 0x2a,
    OP_PACKED_SWITCH                = 0x2b,
    OP_SPARSE_SWITCH                = 0x2c,
    OP_CMPL_FLOAT                   = 0x2d,
    OP_CMPG_FLOAT                   = 0x2e,
    OP_CMPL_DOUBLE                  = 0x2f,
    OP_CMPG_DOUBLE                  = 0x30,
    OP_CMP_LONG                     = 0x31,
    OP_IF_EQ                        = 0x32,
    OP_IF_NE                        = 0x33,
    OP_IF_LT                        = 0x34,
    OP_IF_GE                        = 0x35,
    OP_IF_GT                        = 0x36,
    OP_IF_LE                        = 0x37,
    OP_IF_EQZ                       = 0x38,
    OP_IF_NEZ                       = 0x39,
    OP_IF_LTZ                       = 0x3a,
    OP_IF_GEZ                       = 0x3b,
    OP_IF_GTZ                       = 0x3c,
    OP_IF_LEZ                       = 0x3d,
    OP_UNUSED_3E                    = 0x3e,
    OP_UNUSED_3F                    = 0x3f,
    OP_UNUSED_40                    = 0x40,
    OP_UNUSED_41                    = 0x41,
    OP_UNUSED_42                    = 0x42,
    OP_UNUSED_43                    = 0x43,
    OP_AGET                         = 0x44,
    OP_AGET_WIDE                    = 0x45,
    OP_AGET_OBJECT                  = 0x46,
    OP_AGET_BOOLEAN                 = 0x47,
    OP_AGET_BYTE                    = 0x48,
    OP_AGET_CHAR                    = 0x49,
    OP_AGET_SHORT                   = 0x4a,
    OP_APUT                         = 0x4b,
    OP_APUT_WIDE                    = 0x4c,
    OP_APUT_OBJECT                  = 0x4d,
    OP_APUT_BOOLEAN                 = 0x4e,
    OP_APUT_BYTE                    = 0x4f,
    OP_APUT_CHAR                    = 0x50,
    OP_APUT_SHORT                   = 0x51,
    OP_IGET                         = 0x52,
    OP_IGET_WIDE                    = 0x53,
    OP_IGET_OBJECT                  = 0x54,
    OP_IGET_BOOLEAN                 = 0x55,
    OP_IGET_BYTE                    = 0x56,
    OP_IGET_CHAR                    = 0x57,
    OP_IGET_SHORT                   = 0x58,
    OP_IPUT                         = 0x59,
    OP_IPUT_WIDE                    = 0x5a,
    OP_IPUT_OBJECT                  = 0x5b,
    OP_IPUT_BOOLEAN                 = 0x5c,
    OP_IPUT_BYTE                    = 0x5d,
    OP_IPUT_CHAR                    = 0x5e,
    OP_IPUT_SHORT                   = 0x5f,
    OP_SGET                         = 0x60,
    OP_SGET_WIDE                    = 0x61,
    OP_SGET_OBJECT                  = 0x62,
    OP_SGET_BOOLEAN                 = 0x63,
    OP_SGET_BYTE                    = 0x64,
    OP_SGET_CHAR                    = 0x65,
    OP_SGET_SHORT                   = 0x66,
    OP_SPUT                         = 0x67,
    OP_SPUT_WIDE                    = 0x68,
    OP_SPUT_OBJECT                  = 0x69,
    OP_SPUT_BOOLEAN                 = 0x6a,
    OP_SPUT_BYTE                    = 0x6b,
    OP_SPUT_CHAR                    = 0x6c,
    OP_SPUT_SHORT                   = 0x6d,
    OP_INVOKE_VIRTUAL               = 0x6e,
    OP_INVOKE_SUPER                 = 0x6f,
    OP_INVOKE_DIRECT                = 0x70,
    OP_INVOKE_STATIC                = 0x71,
    OP_INVOKE_INTERFACE             = 0x72,
    OP_UNUSED_73                    = 0x73,
    OP_INVOKE_VIRTUAL_RANGE         = 0x74,
    OP_INVOKE_SUPER_RANGE           = 0x75,
    OP_INVOKE_DIRECT_RANGE          = 0x76,
    OP_INVOKE_STATIC_RANGE          = 0x77,
    OP_INVOKE_INTERFACE_RANGE       = 0x78,
    OP_UNUSED_79                    = 0x79,
    OP_UNUSED_7A                    = 0x7a,
    OP_NEG_INT                      = 0x7b,
    OP_NOT_INT                      = 0x7c,
    OP_NEG_LONG                     = 0x7d,
    OP_NOT_LONG                     = 0x7e,
    OP_NEG_FLOAT                    = 0x7f,
    OP_NEG_DOUBLE                   = 0x80,
    OP_INT_TO_LONG                  = 0x81,
    OP_INT_TO_FLOAT                 = 0x82,
    OP_INT_TO_DOUBLE                = 0x83,
    OP_LONG_TO_INT                  = 0x84,
    OP_LONG_TO_FLOAT                = 0x85,
    OP_LONG_TO_DOUBLE               = 0x86,
    OP_FLOAT_TO_INT                 = 0x87,
    OP_FLOAT_TO_LONG                = 0x88,
    OP_FLOAT_TO_DOUBLE              = 0x89,
    OP_DOUBLE_TO_INT                = 0x8a,
    OP_DOUBLE_TO_LONG               = 0x8b,
    OP_DOUBLE_TO_FLOAT              = 0x8c,
    OP_INT_TO_BYTE                  = 0x8d,
    OP_INT_TO_CHAR                  = 0x8e,
    OP_INT_TO_SHORT                 = 0x8f,
    OP_ADD_INT                      = 0x90,
    OP_SUB_INT                      = 0x91,
    OP_MUL_INT                      = 0x92,
    OP_DIV_INT                      = 0x93,
    OP_REM_INT                      = 0x94,
    OP_AND_INT                      = 0x95,
    OP_OR_INT                       = 0x96,
    OP_XOR_INT                      = 0x97,
    OP_SHL_INT                      = 0x98,
    OP_SHR_INT                      = 0x99,
    OP_USHR_INT                     = 0x9a,
    OP_ADD_LONG                     = 0x9b,
    OP_SUB_LONG                     = 0x9c,
    OP_MUL_LONG                     = 0x9d,
    OP_DIV_LONG                     = 0x9e,
    OP_REM_LONG                     = 0x9f,
    OP_AND_LONG                     = 0xa0,
    OP_OR_LONG                      = 0xa1,
    OP_XOR_LONG                     = 0xa2,
    OP_SHL_LONG                     = 0xa3,
    OP_SHR_LONG                     = 0xa4,
    OP_USHR_LONG                    = 0xa5,
    OP_ADD_FLOAT                    = 0xa6,
    OP_SUB_FLOAT                    = 0xa7,
    OP_MUL_FLOAT                    = 0xa8,
    OP_DIV_FLOAT                    = 0xa9,
    OP_REM_FLOAT                    = 0xaa,
    OP_ADD_DOUBLE                   = 0xab,
    OP_SUB_DOUBLE                   = 0xac,
    OP_MUL_DOUBLE                   = 0xad,
    OP_DIV_DOUBLE                   = 0xae,
    OP_REM_DOUBLE                   = 0xaf,
    OP_ADD_INT_2ADDR                = 0xb0,
    OP_SUB_INT_2ADDR                = 0xb1,
    OP_MUL_INT_2ADDR                = 0xb2,
    OP_DIV_INT_2ADDR                = 0xb3,
    OP_REM_INT_2ADDR                = 0xb4,
    OP_AND_INT_2ADDR                = 0xb5,
    OP_OR_INT_2ADDR                 = 0xb6,
    OP_XOR_INT_2ADDR                = 0xb7,
    OP_SHL_INT_2ADDR                = 0xb8,
    OP_SHR_INT_2ADDR                = 0xb9,
    OP_USHR_INT_2ADDR               = 0xba,
    OP_ADD_LONG_2ADDR               = 0xbb,
    OP_SUB_LONG_2ADDR               = 0xbc,
    OP_MUL_LONG_2ADDR               = 0xbd,
    OP_DIV_LONG_2ADDR               = 0xbe,
    OP_REM_LONG_2ADDR               = 0xbf,
    OP_AND_LONG_2ADDR               = 0xc0,
    OP_OR_LONG_2ADDR                = 0xc1,
    OP_XOR_LONG_2ADDR               = 0xc2,
    OP_SHL_LONG_2ADDR               = 0xc3,
    OP_SHR_LONG_2ADDR               = 0xc4,
    OP_USHR_LONG_2ADDR              = 0xc5,
    OP_ADD_FLOAT_2ADDR              = 0xc6,
    OP_SUB_FLOAT_2ADDR              = 0xc7,
    OP_MUL_FLOAT_2ADDR              = 0xc8,
    OP_DIV_FLOAT_2ADDR              = 0xc9,
    OP_REM_FLOAT_2ADDR              = 0xca,
    OP_ADD_DOUBLE_2ADDR             = 0xcb,
    OP_SUB_DOUBLE_2ADDR             = 0xcc,
    OP_MUL_DOUBLE_2ADDR             = 0xcd,
    OP_DIV_DOUBLE_2ADDR             = 0xce,
    OP_REM_DOUBLE_2ADDR             = 0xcf,
    OP_ADD_INT_LIT16                = 0xd0,
    OP_RSUB_INT                     = 0xd1,
    OP_MUL_INT_LIT16                = 0xd2,
    OP_DIV_INT_LIT16                = 0xd3,
    OP_REM_INT_LIT16                = 0xd4,
    OP_AND_INT_LIT16                = 0xd5,
    OP_OR_INT_LIT16                 = 0xd6,
    OP_XOR_INT_LIT16                = 0xd7,
    OP_ADD_INT_LIT8                 = 0xd8,
    OP_RSUB_INT_LIT8                = 0xd9,
    OP_MUL_INT_LIT8                 = 0xda,
    OP_DIV_INT_LIT8                 = 0xdb,
    OP_REM_INT_LIT8                 = 0xdc,
    OP_AND_INT_LIT8                 = 0xdd,
    OP_OR_INT_LIT8                  = 0xde,
    OP_XOR_INT_LIT8                 = 0xdf,
    OP_SHL_INT_LIT8                 = 0xe0,
    OP_SHR_INT_LIT8                 = 0xe1,
    OP_USHR_INT_LIT8                = 0xe2,
    OP_IGET_VOLATILE                = 0xe3,
    OP_IPUT_VOLATILE                = 0xe4,
    OP_SGET_VOLATILE                = 0xe5,
    OP_SPUT_VOLATILE                = 0xe6,
    OP_IGET_OBJECT_VOLATILE         = 0xe7,
    OP_IGET_WIDE_VOLATILE           = 0xe8,
    OP_IPUT_WIDE_VOLATILE           = 0xe9,
    OP_SGET_WIDE_VOLATILE           = 0xea,
    OP_SPUT_WIDE_VOLATILE           = 0xeb,
    OP_BREAKPOINT                   = 0xec,
    OP_THROW_VERIFICATION_ERROR     = 0xed,
    OP_EXECUTE_INLINE               = 0xee,
    OP_EXECUTE_INLINE_RANGE         = 0xef,
    OP_INVOKE_OBJECT_INIT_RANGE     = 0xf0,
    OP_RETURN_VOID_BARRIER          = 0xf1,
    OP_IGET_QUICK                   = 0xf2,
    OP_UNUSED_F3                    = 0xf3,
    OP_UNUSED_F4                    = 0xf4,
    OP_UNUSED_F5                    = 0xf5,
    OP_UNUSED_F6                    = 0xf6,
    OP_UNUSED_F7                    = 0xf7,
    OP_UNUSED_F8                    = 0xf8,
    OP_UNUSED_F9                    = 0xf9,
    OP_INVOKE_POLYMORPHIC           = 0xfa,
    OP_INVOKE_POLYMORPHIC_RANGE     = 0xfb,
    OP_INVOKE_CUSTOM                = 0xfc,
    OP_INVOKE_CUSTOM_RANGE          = 0xfd,
    OP_CONST_METHOD_HANDLE          = 0xfe,
    OP_CONST_METHOD_TYPE            = 0xff,
    OP_START_PAYLOAD                = 0x100,
    OP_PACKED_SWITCH_PAYLOAD        = 0x100,
    OP_SPARSE_SWITCH_PAYLOAD        = 0x200,
    OP_FILL_ARRAY_DATA_PAYLOAD      = 0x300,
    // END(libdex-opcode-enum)
};

    Opcode() :m_instruction(0) { __init(); }
    void __init();
    ~Opcode(){ delete m_instruction; }

    /*
     * Return the name of an opcode.
     */
    static const Opinfo get_opinfo(unsigned short op)
    {
        /*
        if(op < 0 || op >= OP_END){
            if(!(op == OP_PACKED_SWITCH_PAYLOAD || op == OP_SPARSE_SWITCH_PAYLOAD || op == OP_FILL_ARRAY_DATA_PAYLOAD)){
                return Opinfo();
            }
        }
        */
        //assert(op >= 0 && op < kNumPackedOpcodes);
        return (*m_opcodes)[op];
    }

    inline static unsigned int parssing_type_opcode(unsigned short value) {
        /*
        if ((unsigned char)(value >> 8 )== 0) return (unsigned int)value;
	    else return (unsigned int)((unsigned char)(value >> 8));
        */
        if ((unsigned char)(value)== 0) return (unsigned int)value;
	    else return (unsigned int)((unsigned char)(value));
    }

    Base_instruction* new_instruction(unsigned int insn_type);
};
std::map<unsigned short, Opcode::Opinfo> *Opcode::m_opcodes              = new std::map<unsigned short, Opinfo>();

void Opcode::__init(){
    if(m_opcodes->empty() == 0) return;
    m_opcodes->insert(std::make_pair( OP_NOP                          , Opinfo(OP_NOP                          , "nop"                      ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE                         , Opinfo(OP_MOVE                         , "move"                     ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_FROM16                  , Opinfo(OP_MOVE_FROM16                  , "move/from16"              ,  Base_instruction::ARG_22x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_16                      , Opinfo(OP_MOVE_16                      , "move/16"                  ,  Base_instruction::ARG_32x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_WIDE                    , Opinfo(OP_MOVE_WIDE                    , "move-wide"                ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_WIDE_FROM16             , Opinfo(OP_MOVE_WIDE_FROM16             , "move-wide/from16"         ,  Base_instruction::ARG_22x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_WIDE_16                 , Opinfo(OP_MOVE_WIDE_16                 , "move-wide/16"             ,  Base_instruction::ARG_32x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_OBJECT                  , Opinfo(OP_MOVE_OBJECT                  , "move-object"              ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_OBJECT_FROM16           , Opinfo(OP_MOVE_OBJECT_FROM16           , "move-object/from16"       ,  Base_instruction::ARG_22x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_OBJECT_16               , Opinfo(OP_MOVE_OBJECT_16               , "move-object/16"           ,  Base_instruction::ARG_32x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_RESULT                  , Opinfo(OP_MOVE_RESULT                  , "move-result"              ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_RESULT_WIDE             , Opinfo(OP_MOVE_RESULT_WIDE             , "move-result-wide"         ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_RESULT_OBJECT           , Opinfo(OP_MOVE_RESULT_OBJECT           , "move-result-object"       ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_MOVE_EXCEPTION               , Opinfo(OP_MOVE_EXCEPTION               , "move-exception"           ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_RETURN_VOID                  , Opinfo(OP_RETURN_VOID                  , "return-void"              ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_RETURN                       , Opinfo(OP_RETURN                       , "return"                   ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_RETURN_WIDE                  , Opinfo(OP_RETURN_WIDE                  , "return-wide"              ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_RETURN_OBJECT                , Opinfo(OP_RETURN_OBJECT                , "return-object"            ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_4                      , Opinfo(OP_CONST_4                      , "const/4"                  ,  Base_instruction::ARG_11n) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_16                     , Opinfo(OP_CONST_16                     , "const/16"                 ,  Base_instruction::ARG_21s) ));          
    m_opcodes->insert(std::make_pair( OP_CONST                        , Opinfo(OP_CONST                        , "const"                    ,  Base_instruction::ARG_31i) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_HIGH16                 , Opinfo(OP_CONST_HIGH16                 , "const/high16"             ,  Base_instruction::ARG_21h) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_WIDE_16                , Opinfo(OP_CONST_WIDE_16                , "const-wide/16"            ,  Base_instruction::ARG_21s) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_WIDE_32                , Opinfo(OP_CONST_WIDE_32                , "const-wide/32"            ,  Base_instruction::ARG_31i) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_WIDE                   , Opinfo(OP_CONST_WIDE                   , "const-wide"               ,  Base_instruction::ARG_51l) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_WIDE_HIGH16            , Opinfo(OP_CONST_WIDE_HIGH16            , "const-wide/high16"        ,  Base_instruction::ARG_21h) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_STRING                 , Opinfo(OP_CONST_STRING                 , "const-string"             ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_STRING_JUMBO           , Opinfo(OP_CONST_STRING_JUMBO           , "const-string/jumbo"       ,  Base_instruction::ARG_31c) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_CLASS                  , Opinfo(OP_CONST_CLASS                  , "const-class"              ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_MONITOR_ENTER                , Opinfo(OP_MONITOR_ENTER                , "monitor-enter"            ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_MONITOR_EXIT                 , Opinfo(OP_MONITOR_EXIT                 , "monitor-exit"             ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_CHECK_CAST                   , Opinfo(OP_CHECK_CAST                   , "check-cast"               ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_INSTANCE_OF                  , Opinfo(OP_INSTANCE_OF                  , "instance-of"              ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_ARRAY_LENGTH                 , Opinfo(OP_ARRAY_LENGTH                 , "array-length"             ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_NEW_INSTANCE                 , Opinfo(OP_NEW_INSTANCE                 , "new-instance"             ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_NEW_ARRAY                    , Opinfo(OP_NEW_ARRAY                    , "new-array"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_FILLED_NEW_ARRAY             , Opinfo(OP_FILLED_NEW_ARRAY             , "filled-new-array"         ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_FILLED_NEW_ARRAY_RANGE       , Opinfo(OP_FILLED_NEW_ARRAY_RANGE       , "filled-new-array/range"   ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_FILL_ARRAY_DATA              , Opinfo(OP_FILL_ARRAY_DATA              , "fill-array-data"          ,  Base_instruction::ARG_31t) ));          
    m_opcodes->insert(std::make_pair( OP_THROW                        , Opinfo(OP_THROW                        , "throw"                    ,  Base_instruction::ARG_11x) ));          
    m_opcodes->insert(std::make_pair( OP_GOTO                         , Opinfo(OP_GOTO                         , "goto"                     ,  Base_instruction::ARG_10t) ));          
    m_opcodes->insert(std::make_pair( OP_GOTO_16                      , Opinfo(OP_GOTO_16                      , "goto/16"                  ,  Base_instruction::ARG_20t) ));          
    m_opcodes->insert(std::make_pair( OP_GOTO_32                      , Opinfo(OP_GOTO_32                      , "goto/32"                  ,  Base_instruction::ARG_30t) ));          
    m_opcodes->insert(std::make_pair( OP_PACKED_SWITCH                , Opinfo(OP_PACKED_SWITCH                , "packed-switch"            ,  Base_instruction::ARG_31t) ));          
    m_opcodes->insert(std::make_pair( OP_SPARSE_SWITCH                , Opinfo(OP_SPARSE_SWITCH                , "sparse-switch"            ,  Base_instruction::ARG_31t) ));          
    m_opcodes->insert(std::make_pair( OP_CMPL_FLOAT                   , Opinfo(OP_CMPL_FLOAT                   , "cmpl-float"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_CMPG_FLOAT                   , Opinfo(OP_CMPG_FLOAT                   , "cmpg-float"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_CMPL_DOUBLE                  , Opinfo(OP_CMPL_DOUBLE                  , "cmpl-double"              ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_CMPG_DOUBLE                  , Opinfo(OP_CMPG_DOUBLE                  , "cmpg-double"              ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_CMP_LONG                     , Opinfo(OP_CMP_LONG                     , "cmp-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_IF_EQ                        , Opinfo(OP_IF_EQ                        , "if-eq"                    ,  Base_instruction::ARG_22t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_NE                        , Opinfo(OP_IF_NE                        , "if-ne"                    ,  Base_instruction::ARG_22t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_LT                        , Opinfo(OP_IF_LT                        , "if-lt"                    ,  Base_instruction::ARG_22t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_GE                        , Opinfo(OP_IF_GE                        , "if-ge"                    ,  Base_instruction::ARG_22t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_GT                        , Opinfo(OP_IF_GT                        , "if-gt"                    ,  Base_instruction::ARG_22t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_LE                        , Opinfo(OP_IF_LE                        , "if-le"                    ,  Base_instruction::ARG_22t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_EQZ                       , Opinfo(OP_IF_EQZ                       , "if-eqz"                   ,  Base_instruction::ARG_21t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_NEZ                       , Opinfo(OP_IF_NEZ                       , "if-nez"                   ,  Base_instruction::ARG_21t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_LTZ                       , Opinfo(OP_IF_LTZ                       , "if-ltz"                   ,  Base_instruction::ARG_21t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_GEZ                       , Opinfo(OP_IF_GEZ                       , "if-gez"                   ,  Base_instruction::ARG_21t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_GTZ                       , Opinfo(OP_IF_GTZ                       , "if-gtz"                   ,  Base_instruction::ARG_21t) ));          
    m_opcodes->insert(std::make_pair( OP_IF_LEZ                       , Opinfo(OP_IF_LEZ                       , "if-lez"                   ,  Base_instruction::ARG_21t) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_3E                    , Opinfo(OP_UNUSED_3E                    , "unused-3e"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_3F                    , Opinfo(OP_UNUSED_3F                    , "unused-3f"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_40                    , Opinfo(OP_UNUSED_40                    , "unused-40"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_41                    , Opinfo(OP_UNUSED_41                    , "unused-41"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_42                    , Opinfo(OP_UNUSED_42                    , "unused-42"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_43                    , Opinfo(OP_UNUSED_43                    , "unused-43"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET                         , Opinfo(OP_AGET                         , "aget"                     ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET_WIDE                    , Opinfo(OP_AGET_WIDE                    , "aget-wide"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET_OBJECT                  , Opinfo(OP_AGET_OBJECT                  , "aget-object"              ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET_BOOLEAN                 , Opinfo(OP_AGET_BOOLEAN                 , "aget-boolean"             ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET_BYTE                    , Opinfo(OP_AGET_BYTE                    , "aget-byte"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET_CHAR                    , Opinfo(OP_AGET_CHAR                    , "aget-char"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AGET_SHORT                   , Opinfo(OP_AGET_SHORT                   , "aget-short"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT                         , Opinfo(OP_APUT                         , "aput"                     ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT_WIDE                    , Opinfo(OP_APUT_WIDE                    , "aput-wide"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT_OBJECT                  , Opinfo(OP_APUT_OBJECT                  , "aput-object"              ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT_BOOLEAN                 , Opinfo(OP_APUT_BOOLEAN                 , "aput-boolean"             ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT_BYTE                    , Opinfo(OP_APUT_BYTE                    , "aput-byte"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT_CHAR                    , Opinfo(OP_APUT_CHAR                    , "aput-char"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_APUT_SHORT                   , Opinfo(OP_APUT_SHORT                   , "aput-short"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_IGET                         , Opinfo(OP_IGET                         , "iget"                     ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_WIDE                    , Opinfo(OP_IGET_WIDE                    , "iget-wide"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_OBJECT                  , Opinfo(OP_IGET_OBJECT                  , "iget-object"              ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_BOOLEAN                 , Opinfo(OP_IGET_BOOLEAN                 , "iget-boolean"             ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_BYTE                    , Opinfo(OP_IGET_BYTE                    , "iget-byte"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_CHAR                    , Opinfo(OP_IGET_CHAR                    , "iget-char"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_SHORT                   , Opinfo(OP_IGET_SHORT                   , "iget-short"               ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT                         , Opinfo(OP_IPUT                         , "iput"                     ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT_WIDE                    , Opinfo(OP_IPUT_WIDE                    , "iput-wide"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT_OBJECT                  , Opinfo(OP_IPUT_OBJECT                  , "iput-object"              ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT_BOOLEAN                 , Opinfo(OP_IPUT_BOOLEAN                 , "iput-boolean"             ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT_BYTE                    , Opinfo(OP_IPUT_BYTE                    , "iput-byte"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT_CHAR                    , Opinfo(OP_IPUT_CHAR                    , "iput-char"                ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_IPUT_SHORT                   , Opinfo(OP_IPUT_SHORT                   , "iput-short"               ,  Base_instruction::ARG_22c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET                         , Opinfo(OP_SGET                         , "sget"                     ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET_WIDE                    , Opinfo(OP_SGET_WIDE                    , "sget-wide"                ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET_OBJECT                  , Opinfo(OP_SGET_OBJECT                  , "sget-object"              ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET_BOOLEAN                 , Opinfo(OP_SGET_BOOLEAN                 , "sget-boolean"             ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET_BYTE                    , Opinfo(OP_SGET_BYTE                    , "sget-byte"                ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET_CHAR                    , Opinfo(OP_SGET_CHAR                    , "sget-char"                ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SGET_SHORT                   , Opinfo(OP_SGET_SHORT                   , "sget-short"               ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT                         , Opinfo(OP_SPUT                         , "sput"                     ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT_WIDE                    , Opinfo(OP_SPUT_WIDE                    , "sput-wide"                ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT_OBJECT                  , Opinfo(OP_SPUT_OBJECT                  , "sput-object"              ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT_BOOLEAN                 , Opinfo(OP_SPUT_BOOLEAN                 , "sput-boolean"             ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT_BYTE                    , Opinfo(OP_SPUT_BYTE                    , "sput-byte"                ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT_CHAR                    , Opinfo(OP_SPUT_CHAR                    , "sput-char"                ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_SPUT_SHORT                   , Opinfo(OP_SPUT_SHORT                   , "sput-short"               ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_VIRTUAL               , Opinfo(OP_INVOKE_VIRTUAL               , "invoke-virtual"           ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_SUPER                 , Opinfo(OP_INVOKE_SUPER                 , "invoke-super"             ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_DIRECT                , Opinfo(OP_INVOKE_DIRECT                , "invoke-direct"            ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_STATIC                , Opinfo(OP_INVOKE_STATIC                , "invoke-static"            ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_INTERFACE             , Opinfo(OP_INVOKE_INTERFACE             , "invoke-interface"         ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_73                    , Opinfo(OP_UNUSED_73                    , "unused-73"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_VIRTUAL_RANGE         , Opinfo(OP_INVOKE_VIRTUAL_RANGE         , "invoke-virtual/range"     ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_SUPER_RANGE           , Opinfo(OP_INVOKE_SUPER_RANGE           , "invoke-super/range"       ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_DIRECT_RANGE          , Opinfo(OP_INVOKE_DIRECT_RANGE          , "invoke-direct/range"      ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_STATIC_RANGE          , Opinfo(OP_INVOKE_STATIC_RANGE          , "invoke-static/range"      ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_INTERFACE_RANGE       , Opinfo(OP_INVOKE_INTERFACE_RANGE       , "invoke-interface/range"   ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_79                    , Opinfo(OP_UNUSED_79                    , "unused-79"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_UNUSED_7A                    , Opinfo(OP_UNUSED_7A                    , "unused-7a"                ,  Base_instruction::ARG_10x) ));          
    m_opcodes->insert(std::make_pair( OP_NEG_INT                      , Opinfo(OP_NEG_INT                      , "neg-int"                  ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_NOT_INT                      , Opinfo(OP_NOT_INT                      , "not-int"                  ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_NEG_LONG                     , Opinfo(OP_NEG_LONG                     , "neg-long"                 ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_NOT_LONG                     , Opinfo(OP_NOT_LONG                     , "not-long"                 ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_NEG_FLOAT                    , Opinfo(OP_NEG_FLOAT                    , "neg-float"                ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_NEG_DOUBLE                   , Opinfo(OP_NEG_DOUBLE                   , "neg-double"               ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_INT_TO_LONG                  , Opinfo(OP_INT_TO_LONG                  , "int-to-long"              ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_INT_TO_FLOAT                 , Opinfo(OP_INT_TO_FLOAT                 , "int-to-float"             ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_INT_TO_DOUBLE                , Opinfo(OP_INT_TO_DOUBLE                , "int-to-double"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_LONG_TO_INT                  , Opinfo(OP_LONG_TO_INT                  , "long-to-int"              ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_LONG_TO_FLOAT                , Opinfo(OP_LONG_TO_FLOAT                , "long-to-float"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_LONG_TO_DOUBLE               , Opinfo(OP_LONG_TO_DOUBLE               , "long-to-double"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_FLOAT_TO_INT                 , Opinfo(OP_FLOAT_TO_INT                 , "float-to-int"             ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_FLOAT_TO_LONG                , Opinfo(OP_FLOAT_TO_LONG                , "float-to-long"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_FLOAT_TO_DOUBLE              , Opinfo(OP_FLOAT_TO_DOUBLE              , "float-to-double"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DOUBLE_TO_INT                , Opinfo(OP_DOUBLE_TO_INT                , "double-to-int"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DOUBLE_TO_LONG               , Opinfo(OP_DOUBLE_TO_LONG               , "double-to-long"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DOUBLE_TO_FLOAT              , Opinfo(OP_DOUBLE_TO_FLOAT              , "double-to-float"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_INT_TO_BYTE                  , Opinfo(OP_INT_TO_BYTE                  , "int-to-byte"              ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_INT_TO_CHAR                  , Opinfo(OP_INT_TO_CHAR                  , "int-to-char"              ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_INT_TO_SHORT                 , Opinfo(OP_INT_TO_SHORT                 , "int-to-short"             ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_INT                      , Opinfo(OP_ADD_INT                      , "add-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_INT                      , Opinfo(OP_SUB_INT                      , "sub-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_INT                      , Opinfo(OP_MUL_INT                      , "mul-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_INT                      , Opinfo(OP_DIV_INT                      , "div-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_INT                      , Opinfo(OP_REM_INT                      , "rem-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AND_INT                      , Opinfo(OP_AND_INT                      , "and-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_OR_INT                       , Opinfo(OP_OR_INT                       , "or-int"                   ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_XOR_INT                      , Opinfo(OP_XOR_INT                      , "xor-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SHL_INT                      , Opinfo(OP_SHL_INT                      , "shl-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SHR_INT                      , Opinfo(OP_SHR_INT                      , "shr-int"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_USHR_INT                     , Opinfo(OP_USHR_INT                     , "ushr-int"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_LONG                     , Opinfo(OP_ADD_LONG                     , "add-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_LONG                     , Opinfo(OP_SUB_LONG                     , "sub-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_LONG                     , Opinfo(OP_MUL_LONG                     , "mul-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_LONG                     , Opinfo(OP_DIV_LONG                     , "div-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_LONG                     , Opinfo(OP_REM_LONG                     , "rem-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_AND_LONG                     , Opinfo(OP_AND_LONG                     , "and-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_OR_LONG                      , Opinfo(OP_OR_LONG                      , "or-long"                  ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_XOR_LONG                     , Opinfo(OP_XOR_LONG                     , "xor-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SHL_LONG                     , Opinfo(OP_SHL_LONG                     , "shl-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SHR_LONG                     , Opinfo(OP_SHR_LONG                     , "shr-long"                 ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_USHR_LONG                    , Opinfo(OP_USHR_LONG                    , "ushr-long"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_FLOAT                    , Opinfo(OP_ADD_FLOAT                    , "add-float"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_FLOAT                    , Opinfo(OP_SUB_FLOAT                    , "sub-float"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_FLOAT                    , Opinfo(OP_MUL_FLOAT                    , "mul-float"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_FLOAT                    , Opinfo(OP_DIV_FLOAT                    , "div-float"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_FLOAT                    , Opinfo(OP_REM_FLOAT                    , "rem-float"                ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_DOUBLE                   , Opinfo(OP_ADD_DOUBLE                   , "add-double"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_DOUBLE                   , Opinfo(OP_SUB_DOUBLE                   , "sub-double"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_DOUBLE                   , Opinfo(OP_MUL_DOUBLE                   , "mul-double"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_DOUBLE                   , Opinfo(OP_DIV_DOUBLE                   , "div-double"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_DOUBLE                   , Opinfo(OP_REM_DOUBLE                   , "rem-double"               ,  Base_instruction::ARG_23x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_INT_2ADDR                , Opinfo(OP_ADD_INT_2ADDR                , "add-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_INT_2ADDR                , Opinfo(OP_SUB_INT_2ADDR                , "sub-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_INT_2ADDR                , Opinfo(OP_MUL_INT_2ADDR                , "mul-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_INT_2ADDR                , Opinfo(OP_DIV_INT_2ADDR                , "div-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_INT_2ADDR                , Opinfo(OP_REM_INT_2ADDR                , "rem-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_AND_INT_2ADDR                , Opinfo(OP_AND_INT_2ADDR                , "and-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_OR_INT_2ADDR                 , Opinfo(OP_OR_INT_2ADDR                 , "or-int/2addr"             ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_XOR_INT_2ADDR                , Opinfo(OP_XOR_INT_2ADDR                , "xor-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SHL_INT_2ADDR                , Opinfo(OP_SHL_INT_2ADDR                , "shl-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SHR_INT_2ADDR                , Opinfo(OP_SHR_INT_2ADDR                , "shr-int/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_USHR_INT_2ADDR               , Opinfo(OP_USHR_INT_2ADDR               , "ushr-int/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_LONG_2ADDR               , Opinfo(OP_ADD_LONG_2ADDR               , "add-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_LONG_2ADDR               , Opinfo(OP_SUB_LONG_2ADDR               , "sub-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_LONG_2ADDR               , Opinfo(OP_MUL_LONG_2ADDR               , "mul-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_LONG_2ADDR               , Opinfo(OP_DIV_LONG_2ADDR               , "div-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_LONG_2ADDR               , Opinfo(OP_REM_LONG_2ADDR               , "rem-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_AND_LONG_2ADDR               , Opinfo(OP_AND_LONG_2ADDR               , "and-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_OR_LONG_2ADDR                , Opinfo(OP_OR_LONG_2ADDR                , "or-long/2addr"            ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_XOR_LONG_2ADDR               , Opinfo(OP_XOR_LONG_2ADDR               , "xor-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SHL_LONG_2ADDR               , Opinfo(OP_SHL_LONG_2ADDR               , "shl-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SHR_LONG_2ADDR               , Opinfo(OP_SHR_LONG_2ADDR               , "shr-long/2addr"           ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_USHR_LONG_2ADDR              , Opinfo(OP_USHR_LONG_2ADDR              , "ushr-long/2addr"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_FLOAT_2ADDR              , Opinfo(OP_ADD_FLOAT_2ADDR              , "add-float/2addr"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_FLOAT_2ADDR              , Opinfo(OP_SUB_FLOAT_2ADDR              , "sub-float/2addr"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_FLOAT_2ADDR              , Opinfo(OP_MUL_FLOAT_2ADDR              , "mul-float/2addr"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_FLOAT_2ADDR              , Opinfo(OP_DIV_FLOAT_2ADDR              , "div-float/2addr"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_FLOAT_2ADDR              , Opinfo(OP_REM_FLOAT_2ADDR              , "rem-float/2addr"          ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_DOUBLE_2ADDR             , Opinfo(OP_ADD_DOUBLE_2ADDR             , "add-double/2addr"         ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_SUB_DOUBLE_2ADDR             , Opinfo(OP_SUB_DOUBLE_2ADDR             , "sub-double/2addr"         ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_DOUBLE_2ADDR             , Opinfo(OP_MUL_DOUBLE_2ADDR             , "mul-double/2addr"         ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_DOUBLE_2ADDR             , Opinfo(OP_DIV_DOUBLE_2ADDR             , "div-double/2addr"         ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_REM_DOUBLE_2ADDR             , Opinfo(OP_REM_DOUBLE_2ADDR             , "rem-double/2addr"         ,  Base_instruction::ARG_12x) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_INT_LIT16                , Opinfo(OP_ADD_INT_LIT16                , "add-int/lit16"            ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_RSUB_INT                     , Opinfo(OP_RSUB_INT                     , "rsub-int"                 ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_INT_LIT16                , Opinfo(OP_MUL_INT_LIT16                , "mul-int/lit16"            ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_INT_LIT16                , Opinfo(OP_DIV_INT_LIT16                , "div-int/lit16"            ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_REM_INT_LIT16                , Opinfo(OP_REM_INT_LIT16                , "rem-int/lit16"            ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_AND_INT_LIT16                , Opinfo(OP_AND_INT_LIT16                , "and-int/lit16"            ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_OR_INT_LIT16                 , Opinfo(OP_OR_INT_LIT16                 , "or-int/lit16"             ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_XOR_INT_LIT16                , Opinfo(OP_XOR_INT_LIT16                , "xor-int/lit16"            ,  Base_instruction::ARG_22s) ));          
    m_opcodes->insert(std::make_pair( OP_ADD_INT_LIT8                 , Opinfo(OP_ADD_INT_LIT8                 , "add-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_RSUB_INT_LIT8                , Opinfo(OP_RSUB_INT_LIT8                , "rsub-int/lit8"            ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_MUL_INT_LIT8                 , Opinfo(OP_MUL_INT_LIT8                 , "mul-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_DIV_INT_LIT8                 , Opinfo(OP_DIV_INT_LIT8                 , "div-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_REM_INT_LIT8                 , Opinfo(OP_REM_INT_LIT8                 , "rem-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_AND_INT_LIT8                 , Opinfo(OP_AND_INT_LIT8                 , "and-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_OR_INT_LIT8                  , Opinfo(OP_OR_INT_LIT8                  , "or-int/lit8"              ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_XOR_INT_LIT8                 , Opinfo(OP_XOR_INT_LIT8                 , "xor-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_SHL_INT_LIT8                 , Opinfo(OP_SHL_INT_LIT8                 , "shl-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_SHR_INT_LIT8                 , Opinfo(OP_SHR_INT_LIT8                 , "shr-int/lit8"             ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_USHR_INT_LIT8                , Opinfo(OP_USHR_INT_LIT8                , "ushr-int/lit8"            ,  Base_instruction::ARG_22b) ));          
    m_opcodes->insert(std::make_pair( OP_IGET_VOLATILE                , Opinfo(OP_IGET_VOLATILE                , "+iget-volatile"           ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_IPUT_VOLATILE                , Opinfo(OP_IPUT_VOLATILE                , "+iput-volatile"           ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_SGET_VOLATILE                , Opinfo(OP_SGET_VOLATILE                , "+sget-volatile"           ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_SPUT_VOLATILE                , Opinfo(OP_SPUT_VOLATILE                , "+sput-volatile"           ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_IGET_OBJECT_VOLATILE         , Opinfo(OP_IGET_OBJECT_VOLATILE         , "+iget-object-volatile"    ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_IGET_WIDE_VOLATILE           , Opinfo(OP_IGET_WIDE_VOLATILE           , "+iget-wide-volatile"      ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_IPUT_WIDE_VOLATILE           , Opinfo(OP_IPUT_WIDE_VOLATILE           , "+iput-wide-volatile"      ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_SGET_WIDE_VOLATILE           , Opinfo(OP_SGET_WIDE_VOLATILE           , "+sget-wide-volatile"      ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_SPUT_WIDE_VOLATILE           , Opinfo(OP_SPUT_WIDE_VOLATILE           , "+sput-wide-volatile"      ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_BREAKPOINT                   , Opinfo(OP_BREAKPOINT                   , "^breakpoint"              ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_THROW_VERIFICATION_ERROR     , Opinfo(OP_THROW_VERIFICATION_ERROR     , "^throw-verification-error",  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_EXECUTE_INLINE               , Opinfo(OP_EXECUTE_INLINE               , "+execute-inline"          ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_EXECUTE_INLINE_RANGE         , Opinfo(OP_EXECUTE_INLINE_RANGE         , "+execute-inline/range"    ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_INVOKE_OBJECT_INIT_RANGE     , Opinfo(OP_INVOKE_OBJECT_INIT_RANGE     , "+invoke-object-init/range",  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_RETURN_VOID_BARRIER          , Opinfo(OP_RETURN_VOID_BARRIER          , "+return-void-barrier"     ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_IGET_QUICK                   , Opinfo(OP_IGET_QUICK                   , "+iget-quick"              ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F3                    , Opinfo(OP_UNUSED_F3                    , "unused-f3"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F4                    , Opinfo(OP_UNUSED_F4                    , "unused-f4"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F5                    , Opinfo(OP_UNUSED_F5                    , "unused-f5"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F6                    , Opinfo(OP_UNUSED_F6                    , "unused-f6"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F7                    , Opinfo(OP_UNUSED_F7                    , "unused-f7"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F8                    , Opinfo(OP_UNUSED_F8                    , "unused-f8"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_UNUSED_F9                    , Opinfo(OP_UNUSED_F9                    , "unused-f9"                ,  Base_instruction::ARG_10x) ));          //unused
    m_opcodes->insert(std::make_pair( OP_INVOKE_POLYMORPHIC           , Opinfo(OP_INVOKE_POLYMORPHIC           , "invoke-polymorphic"       , Base_instruction::ARG_45cc) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_POLYMORPHIC_RANGE     , Opinfo(OP_INVOKE_POLYMORPHIC_RANGE     , "invoke-polymorphic/range" , Base_instruction::ARG_4rcc) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_CUSTOM                , Opinfo(OP_INVOKE_CUSTOM                , "invoke-custom"            ,  Base_instruction::ARG_35c) ));          
    m_opcodes->insert(std::make_pair( OP_INVOKE_CUSTOM_RANGE          , Opinfo(OP_INVOKE_CUSTOM_RANGE          , "invoke-custom/range"      ,  Base_instruction::ARG_3rc) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_METHOD_HANDLE          , Opinfo(OP_CONST_METHOD_HANDLE          , "const-method-handle"      ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_CONST_METHOD_TYPE            , Opinfo(OP_CONST_METHOD_TYPE            , "const-method-type"        ,  Base_instruction::ARG_21c) ));          
    m_opcodes->insert(std::make_pair( OP_PACKED_SWITCH_PAYLOAD        , Opinfo(OP_PACKED_SWITCH_PAYLOAD        , "packed_switch_payload"    , Base_instruction::ARG_PACKED_SWITCH_PAYLOAD) ));
    m_opcodes->insert(std::make_pair( OP_SPARSE_SWITCH_PAYLOAD        , Opinfo(OP_SPARSE_SWITCH_PAYLOAD        , "sparse_switch_payload"    , Base_instruction::ARG_SPARSE_SWITCH_PAYLOAD) ));
    m_opcodes->insert(std::make_pair( OP_FILL_ARRAY_DATA_PAYLOAD      , Opinfo(OP_FILL_ARRAY_DATA_PAYLOAD      , "fill_array_data_payload"  , Base_instruction::ARG_FILL_ARRAY_DATA_PAYLOAD) ));
}

unsigned int Opcode::__parssing(unsigned int _command_flag){
    //m_offset_count :: 총 byte 갯수를 의미
    unsigned int offset_revision = 0;
    unsigned int opcode_value = *((unsigned short*)(m_dex + m_start_offset + offset_revision));

    opcode_value = parssing_type_opcode((unsigned short)opcode_value);
    // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t opcode %04X\n",m_start_offset + offset_revision, opcode_value);
    // m_opinfo = (*m_opcodes)[opcode_value];
    m_opinfo = get_opinfo(opcode_value);
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t opcode %04X  %s\n",m_start_offset + offset_revision, opcode_value, m_opinfo.name.c_str());
    offset_revision += (opcode_value >= OP_START_PAYLOAD ? 2 : 1) ; // PAYLOAD 내용이라면 2바이트 증가, 아니라면 1바이트 증가

    m_instruction = new_instruction(m_opinfo.arg_type);
    m_instruction->init(m_dex, m_start_offset + offset_revision, m_offset_count - offset_revision);
    return m_instruction->parssing(get_command_flag());
}

Base_instruction* Opcode::new_instruction(unsigned int insn_type){
    switch(insn_type){
        case Base_instruction::ARG_NONE  :
        //.........주의 ! PAYLOAD
        break;
        case Base_instruction::ARG_00x   :
            return new Instruction_00x;
        case Base_instruction::ARG_10x   :
            return new Instruction_10x;
        case Base_instruction::ARG_12x   :
            return new Instruction_12x;
        case Base_instruction::ARG_11n   :
            return new Instruction_11n;
        case Base_instruction::ARG_11x   :
            return new Instruction_11x;
        case Base_instruction::ARG_10t   :
            return new Instruction_10t;
        case Base_instruction::ARG_20t   :
            return new Instruction_20t;
        case Base_instruction::ARG_20bc  :
            return new Instruction_20bc;
        case Base_instruction::ARG_22x   :
            return new Instruction_22x;
        case Base_instruction::ARG_21t   :
            return new Instruction_21t;
        case Base_instruction::ARG_21s   :
            return new Instruction_21s;
        case Base_instruction::ARG_21h   :
        //.........주의 ! 뭔짓인지 모르겠음
            return new Instruction_21h;
        case Base_instruction::ARG_21ih  :
            return new Instruction_21ih;
        case Base_instruction::ARG_21lh  :
            return new Instruction_21lh;
        case Base_instruction::ARG_21c   :
            return new Instruction_21c;
        case Base_instruction::ARG_23x   :
            return new Instruction_23x;
        case Base_instruction::ARG_22b   :
            return new Instruction_22b;
        case Base_instruction::ARG_22t   :
            return new Instruction_22t;
        case Base_instruction::ARG_22s   :
            return new Instruction_22s;
        case Base_instruction::ARG_22c   :
            return new Instruction_22c;
        case Base_instruction::ARG_22cs  :
            return new Instruction_22cs;
        case Base_instruction::ARG_30t   :
            return new Instruction_30t;
        case Base_instruction::ARG_32x   :
            return new Instruction_32x;
        case Base_instruction::ARG_31i   :
            return new Instruction_31i;
        case Base_instruction::ARG_31t   :
            return new Instruction_31t;
        case Base_instruction::ARG_31c   :
            return new Instruction_31c;
        case Base_instruction::ARG_35c   :
            return new Instruction_35c;
        case Base_instruction::ARG_35ms  :
            return new Instruction_35ms;
        case Base_instruction::ARG_35mi  :
            return new Instruction_35mi;
        case Base_instruction::ARG_3rc   :
            return new Instruction_3rc;
        case Base_instruction::ARG_3rms  :
            return new Instruction_3rms;
        case Base_instruction::ARG_3rmi  :
            return new Instruction_3rmi;
        case Base_instruction::ARG_45cc  :
            return new Instruction_45cc;
        case Base_instruction::ARG_4rcc  :
            return new Instruction_4rcc;
        case Base_instruction::ARG_51l   :
            return new Instruction_51l;
        case Base_instruction::ARG_PACKED_SWITCH_PAYLOAD   :
            return new Instruction_packed_switch_payload;
        case Base_instruction::ARG_SPARSE_SWITCH_PAYLOAD   :
            return new Instruction_sparese_switch_payload;
        case Base_instruction::ARG_FILL_ARRAY_DATA_PAYLOAD :
            return new Instruction_fill_array_data_payload;
    }
    Log::print(Log::LOG_ERROR, "ERROR !! Opcde.init_instruction(insn_type[%d])\n", insn_type);
    return NULL;

}
