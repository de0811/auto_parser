#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_BASE_PARSSING
#define _IN_BASE_PARSSING
#include "log_manager.h"
#include "dex_base_reference.h"
#include <map>
/*
모든 섹션 분석
멀티 덱스 분석
덱스 문제 분석
원본 회귀
변환
*/

class Base_dex_parssing{
public:
    enum{
        eTYPE_HEADER_ITEM                   =0x0000,    //1
        eTYPE_STRING_ID_ITEM                =0x0001,    //3
        eTYPE_TYPE_ID_ITEM                  =0x0002,    //4
        eTYPE_PROTO_ID_ITEM                 =0x0003,    //6
        eTYPE_FIELD_ID_ITEM                 =0x0004,    //5
        eTYPE_METHOD_ID_ITEM                =0x0005,    //7
        eTYPE_CLASS_DEF_ITEM                =0x0006,    //6
        eTYPE_CALL_SITE_ID_ITEM             =0x0007,    //??
        eTYPE_METHOD_HANDLE_ITEM            =0x0008,    //8

        eTYPE_MAP_LIST                      =0x1000,    //2
        eTYPE_TYPE_LIST                     =0x1001,    //5
        eTYPE_ANNOTATION_SET_REF_LIST       =0x1002,    //11
        eTYPE_ANNOTATION_SET_ITEM           =0x1003,    //10
        
        eTYPE_CLASS_DATA_ITEM               =0x2000,    //8
        eTYPE_CODE_ITEM                     =0x2001,    //??
        eTYPE_STRING_DATA_ITEM              =0x2002,    //3.5
        eTYPE_DEBUG_INFO_ITEM               =0x2003,    //링크 부분이 애매...
        eTYPE_ANNOTATION_ITEM               =0x2004,    //9
        eTYPE_ENCODED_ARRAY_ITEM            =0x2005,    //9
        eTYPE_ANNOTATIONS_DIRECTORY_ITEM    =0x2006,    //12
        //6/20
    };
    enum{
        eFLAG_LOG                           =1 << 0,    //0000 0001
        eFLAG_OBFUSCATION                   =1 << 1,    //0000 0010 //난독화를 적용
        eFLAG_NORMALIZATION                 =1 << 2,    //0000 0100 //apktool로 풀리도록 정상화
    };
    unsigned int                                 _command_flag;
protected:
    unsigned int                                       m_start_offset;
    unsigned int                                       m_offset_count;
    unsigned char*                                              m_dex;
    static std::map<unsigned int, Base_dex_reference*>     *m_map_ref;

    //각 섹션에 맞게 분석할 부분
    //print 값으로 확인 필요하지 않을까 소스 낭비가 너무 많음
    // virtual unsigned int __parssing(int print)
    virtual unsigned int __parssing(unsigned int _command_flag){ return 0; }
    //virtual unsigned int __parssing() {return 0;}
    //padding이 끼여 있는 것들이 있기 때문에 위치를 맞춰주기 위해 필요
    unsigned int __align_offset(unsigned int offset, unsigned int alignment);
    //init에서 추가 조작이 필요할 경우 사용
    //ERROR virtual로 처리하려 했지만 어째서 가상이 먹히지 않음
    virtual void __init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count) {}
    //자기 자신을 reference로 등록할 경우 사용
    void init_reference(unsigned int type, Base_dex_reference* ref);
    Base_dex_reference* get_reference(unsigned int ref_type);
    unsigned int get_command_flag(unsigned int flag){ return flag & _command_flag; }
    unsigned int get_command_flag(){ return _command_flag; }

public:
    Base_dex_parssing() : m_dex(0), m_start_offset(0), m_offset_count(0){}

    //따로 선언 가능
    void init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count);
    //하... 그냥 통일 시켜야지 어휴
    // unsigned int parssing(bool cprint){return __parssing(cprint);}

    unsigned int parssing(unsigned int _command_flag){ set_command_flag(_command_flag); return __parssing(_command_flag);}
    //공통으로 분석될 부분
    unsigned int parssing(unsigned char* dex, unsigned int start_offset, unsigned int offset_count);
    //분석 상태 값 표현
    void set_command_flag(unsigned int _command_flag){ this->_command_flag |= _command_flag; }
};
std::map<unsigned int, Base_dex_reference*> *Base_dex_parssing::m_map_ref              = new std::map<unsigned int, Base_dex_reference*>();

void Base_dex_parssing::init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count){
    m_dex = dex;
    m_start_offset = start_offset;
    m_offset_count = offset_count;
    __init(dex, start_offset, offset_count);
}

void Base_dex_parssing::init_reference(unsigned int type, Base_dex_reference* ref){
    m_map_ref->insert(std::make_pair(type, ref));
}

unsigned int Base_dex_parssing::__align_offset(unsigned int offset, unsigned int alignment){
    int mask = alignment -1;
    if(! (alignment >= 0 && (mask & alignment) == 0) ){
        return 0;
    }
    return (offset + mask) & ~mask;
}

Base_dex_reference* Base_dex_parssing::get_reference(unsigned int ref_type){
    return (*m_map_ref)[ref_type];
}

unsigned int Base_dex_parssing::parssing(unsigned char* dex, unsigned int start_offset, unsigned int offset_count){
    init(dex, start_offset, offset_count);
    return __parssing(false);
}


#endif  /* _IN_BASE_PARSSING */