#pragma warning(disable : 4996)
// auto_parssing.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include <stdio.h>
// t #include <sys/stat.h> //stat struct

#ifdef WIN32
#include <Windows.h>
#include <io.h>
#else
#endif
// #include <fcntl.h>
//#include "common.h"
//#include "zip.h"
#include "cd_option.h"
#include "dex.h"


#ifdef WIN32
#else
#endif

unsigned int get_file_size(char *pathname)
{
	FILE* pf = fopen(pathname, "rb");
	if (!pf) return 0;

	fseek(pf, 0, SEEK_END);
	unsigned int size = ftell(pf);

	fclose(pf);

	return size;
}


int main(int argc, char* argv[])
{
	CD_option<C_dex> opt;
	C_dex dex;

	// unsigned int add_opt(char* str_opt, OPT_TYPE etype, unsigned int order, unsigned int arg_min_count, unsigned int arg_max_count, I_option_runner* popt_runner, unsigned int (*prun)(int argc, char* argv[])){
	// unsigned int set_dex_path(std::vector<const char*> args);
	// unsigned int set_write_path(std::vector<const char*> args);
	// unsigned int set_log_print(std::vector<const char*> args);
	// unsigned int set_apk_obfuscation(std::vector<const char*> args);
	// unsigned int set_apk_normalization(std::vector<const char*> args);
	// unsigned int parssing(std::vector<const char*> args);
	opt.add_opt("", CD_option<C_dex>::OPT_TYPE::MAIN, 2, 0, 0, &dex, &C_dex::parssing, NULL);
	opt.add_opt("-h", CD_option<C_dex>::OPT_TYPE::MAIN, 2, 0, 0, NULL, NULL, help_print);
	opt.add_opt("-s", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 1, 1, &dex, &C_dex::set_dex_path, NULL);
	opt.add_opt("-w", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 1, 1, &dex, &C_dex::set_write_path, NULL);
	opt.add_opt("-p", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 0, 0, &dex, &C_dex::set_log_print, NULL);
	opt.add_opt("-ob", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 0, 0, &dex, &C_dex::set_apk_obfuscation, NULL);
	opt.add_opt("-no", CD_option<C_dex>::OPT_TYPE::OPTION, 0, 0, 0, &dex, &C_dex::set_apk_normalization, NULL);
	opt.parssing(argc, argv);
	opt.run();
	return 0;
}

