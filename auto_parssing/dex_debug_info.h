#include "leb128.h"
#include "dex_base_parssing.h"
#include "dex_base_debug_opcode.h"
#include <vector>




#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_DEBUG_INFO
#define _IN_DEBUG_INFO

class Debug_info : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Debug_info_item{
                ULEB128                                     line_start;                     //4
                ULEB128                                     parameters_size;                //8
                std::vector<ULEB128>                        parameter_names;                 //12
                std::vector<Base_dex_debug_opcode*>          opcodes;                        //추가로 사용
    };
#pragma pack(pop)

    std::vector<Debug_info_item> m_debug_infos;
    Base_dex_debug_opcode* new_debug_opcode(int type);
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);
public:
    ~Debug_info();
};

Debug_info::~Debug_info(){
    for(unsigned int i = 0; i < m_debug_infos.size(); i++){
        for(unsigned int k = 0; k < m_debug_infos[i].opcodes.size(); k++){
            delete m_debug_infos[i].opcodes[k];
        }
    }
}

unsigned int Debug_info::__parssing(unsigned int _command_flag){
    if(m_debug_infos.size()) return 0;
    unsigned int offset_revision = 0;

    Base_dex_reference* pstr_ids = get_reference(eTYPE_STRING_ID_ITEM);
    if(get_command_flag(eFLAG_LOG)){
        if(pstr_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Debug_info.__parssing() reference NULL\n");
            return m_start_offset;
        }

        Log::print(Log::LOG_INFO, "**Debug_info_item Section\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t NAME\t\t\t\t DATA\n");
        Log::print(Log::LOG_INFO, "------------------------------------------------------------------------------------------------------\n");
    }

    unsigned char* tmp_dex = NULL;
    ULEB128 tmp_param_name;
    m_debug_infos.reserve(m_offset_count);

    for(unsigned int i = 0; i < m_offset_count; i++){
        Debug_info_item debug_info;

        tmp_dex = m_dex + m_start_offset + offset_revision;
        debug_info.line_start = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t debug_info_item[%8d].line_start   \t %8d\n", m_start_offset + offset_revision, i, debug_info.line_start);
        offset_revision = tmp_dex - m_dex - m_start_offset;


        tmp_dex = m_dex + m_start_offset + offset_revision;
        debug_info.parameters_size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t debug_info_item[%8d].parameters_size   \t %8d\n", m_start_offset + offset_revision, i, debug_info.parameters_size);
        offset_revision = tmp_dex - m_dex - m_start_offset;

        debug_info.parameter_names.reserve(debug_info.parameters_size);
        // if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t parameters ==\n"m_start_offset + offset_revision);
        for(unsigned int k = 0; k < debug_info.parameters_size; k++){
            tmp_dex = m_dex + m_start_offset + offset_revision;
            tmp_param_name = readUnsignedLeb128((const unsigned char**)&tmp_dex) - 1;
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t debug_info_item[%8d].parameter_names   \t %8d\t %s\n", m_start_offset + offset_revision, i, tmp_param_name, pstr_ids->as_string(tmp_param_name).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
            debug_info.parameter_names.push_back(tmp_param_name);
        }

        unsigned int op_count = 0;
        while(true){
            op_count++;
            u1 opcode = *((u1*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t Debug_OPCODE\t %d\n", m_start_offset + offset_revision, opcode);
            offset_revision += sizeof(u1);
            Base_dex_debug_opcode* deop = new_debug_opcode(opcode);
            if(deop == NULL) return m_start_offset + offset_revision;
            deop->init(m_dex, m_start_offset + offset_revision, 1, opcode);
            offset_revision = deop->parssing(get_command_flag()) - m_start_offset;
            debug_info.opcodes.reserve(op_count);
            debug_info.opcodes.push_back(deop);

            if( opcode == Base_dex_debug_opcode::DBG_END_SEQUENCE ) {
                break;
            }
        }

        m_debug_infos.push_back(debug_info);
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    return m_start_offset + offset_revision;
}

Base_dex_debug_opcode* Debug_info::new_debug_opcode(int type){
    switch(type){
        case Base_dex_debug_opcode::DBG_END_SEQUENCE                :
        return new Debug_opcode_END_SEQUENCE;
        case Base_dex_debug_opcode::DBG_ADVANCE_PC                  :
        return new Debug_opcode_ADVANCE_PC;
        case Base_dex_debug_opcode::DBG_ADVANCE_LINE                :
        return new Debug_opcode_ADVANCE_LINE;
        case Base_dex_debug_opcode::DBG_START_LOCAL                 :
        return new Debug_opcode_START_LOCAL;
        case Base_dex_debug_opcode::DBG_START_LOCAL_EXTENDED        :
        return new Debug_opcode_START_LOCAL_EXTENDED;
        case Base_dex_debug_opcode::DBG_END_LOCAL                   :
        return new Debug_opcode_END_LOCAL;
        case Base_dex_debug_opcode::DBG_RESTART_LOCAL               :
        return new Debug_opcode_RESTART_LOCAL;
        case Base_dex_debug_opcode::DBG_SET_PROLOGUE_END            :
        return new Debug_opcode_SET_PROLOGUE_END;
        case Base_dex_debug_opcode::DBG_SET_EPILOGUE_BEGIN          :
        return new Debug_opcode_SET_EPILOGUE_BEGIN;
        case Base_dex_debug_opcode::DBG_SET_FILE                    :
        return new Debug_opcode_SET_FILE;
        default :
        {
            if(type >= Base_dex_debug_opcode::DBG_SPECIAL_OP_START && type <= Base_dex_debug_opcode::DBG_SPECIAL_OP_END){
                return new Debug_opcode_SPECIAL_OP();
            }
            else{
                Log::print(Log::LOG_ERROR, "ERROR !! Debug_info.new_debug_opcode(type[%d]) Over type\n", type);
                return NULL;
            }
        }
    }
}

#endif /*_IN_DEBUG_INFO*/