

#include "dex_common.h"
#include "dex_base_parssing.h"
#include <vector>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_ANNOTATIONS_DIRECTORY
#define _IN_ANNOTATIONS_DIRECTORY



class Annotations_directory : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Field_annotation{
        u4              field_idx;
        u4              annotations_off;
    };
    struct Method_annotation{
        u4              method_idx;
        u4              annotations_off;
    };
    struct Parameter_annotation{
        u4              method_idx;
        u4              annotations_off;
    };
    struct Annotations_directory_item{
                u4                                      class_annotations_off;
                u4                                      fields_size;
                u4                                      annotated_methods_size;
                u4                                      annotated_parameters_size;
                std::vector<Field_annotation>           field_annotations;
                std::vector<Method_annotation>          method_annotations;
                std::vector<Parameter_annotation>       parameter_annotations;
    };
#pragma pack(pop)
    std::vector<Annotations_directory_item>     m_annotations_directory_items;
protected:
    unsigned int __parssing(unsigned int _command_flag);
};

unsigned int Annotations_directory::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    Base_dex_reference* pfield_ids = get_reference(eTYPE_FIELD_ID_ITEM);
    Base_dex_reference* pmethod_ids = get_reference(eTYPE_METHOD_ID_ITEM);

    if(get_command_flag(eFLAG_LOG)){
        if(pfield_ids == NULL || pmethod_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Annotations_directory.__parssing() reference is NULL\n");
            return m_start_offset + offset_revision;
        }
        Log::print(Log::LOG_INFO, "**Annotations_directory parssing\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t ....\n");
        Log::print(Log::LOG_INFO, "----------------------------------------------------------------------------------------------------------------\n");
    }

    m_annotations_directory_items.reserve(m_offset_count);
    for(unsigned int i = 0; i < m_offset_count; i++){
        Annotations_directory_item annotations_directory_item;

        annotations_directory_item.class_annotations_off = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotations_directory_item.class_annotations_off\t 0x%08X\n", m_start_offset + offset_revision, i, annotations_directory_item.class_annotations_off);
        offset_revision += sizeof(u4);

        annotations_directory_item.fields_size = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotations_directory_item.fields_size\t %05d\n", m_start_offset + offset_revision, i, annotations_directory_item.fields_size);
        offset_revision += sizeof(u4);

        annotations_directory_item.annotated_methods_size = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotations_directory_item.annotated_methods_size\t %05d\n", m_start_offset + offset_revision, i, annotations_directory_item.annotated_methods_size);
        offset_revision += sizeof(u4);

        annotations_directory_item.annotated_parameters_size = *((u4*)(m_dex + m_start_offset + offset_revision));
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t Annotations_directory_item.annotated_parameters_size\t %05d\n", m_start_offset + offset_revision, i, annotations_directory_item.annotated_parameters_size);
        offset_revision += sizeof(u4);

        annotations_directory_item.field_annotations.reserve(annotations_directory_item.fields_size);
        for(unsigned int k = 0; k < annotations_directory_item.fields_size; k++){
            Field_annotation field_annotations;

            //reference pfield_ids
            field_annotations.field_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t\t Field_annotation.field_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, field_annotations.field_idx, pfield_ids->as_string(field_annotations.field_idx).c_str());
            offset_revision += sizeof(u4);

            //reference AnnotationSetItem
            field_annotations.annotations_off = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t\t Field_annotation.annotations_off\t 0x%08X\n", m_start_offset + offset_revision, i, field_annotations.annotations_off);
            offset_revision += sizeof(u4);

            annotations_directory_item.field_annotations.push_back(field_annotations);
        }

        annotations_directory_item.method_annotations.reserve(annotations_directory_item.annotated_methods_size);
        for(unsigned int k = 0; k < annotations_directory_item.annotated_methods_size; k++){
            Method_annotation method_annotation;
            
            //reference method_ids
            method_annotation.method_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t\t Method_annotation.method_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, method_annotation.method_idx, pmethod_ids->as_string(method_annotation.method_idx).c_str());
            offset_revision += sizeof(u4);

            //reference AnnotationSetItem
            method_annotation.annotations_off = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t\t Method_annotation.annotations_off\t 0x%08X\n", m_start_offset + offset_revision, i, method_annotation.annotations_off);
            offset_revision += sizeof(u4);
            annotations_directory_item.method_annotations.push_back(method_annotation);
        }

        annotations_directory_item.parameter_annotations.reserve(annotations_directory_item.annotated_parameters_size);
        for(unsigned int k = 0; k < annotations_directory_item.annotated_parameters_size; k++){
            Parameter_annotation parameter_annotation;
            
            //reference method_ids
            parameter_annotation.method_idx = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t\t Parameter_annotation.method_idx\t %05d\t %s\n", m_start_offset + offset_revision, i, parameter_annotation.method_idx, pmethod_ids->as_string(parameter_annotation.method_idx).c_str());
            offset_revision += sizeof(u4);

            //reference AnnotationSetItem
            parameter_annotation.annotations_off = *((u4*)(m_dex + m_start_offset + offset_revision));
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t %05d\t\t Parameter_annotation.annotations_off\t 0x%08X\n", m_start_offset + offset_revision, i, parameter_annotation.annotations_off);
            offset_revision += sizeof(u4);
            annotations_directory_item.parameter_annotations.push_back(parameter_annotation);
        }

        m_annotations_directory_items.push_back(annotations_directory_item);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");


    return m_start_offset + offset_revision;
}


#endif /* _IN_ANNOTATIONS_DIRECTORY */