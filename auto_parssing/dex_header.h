#include "dex_common.h"
#include "dex_base_parssing.h"
// #include "crc32.hxx"
#include "crc32.hxx"
#include <vector>
#include <string>

#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_DEX_HEADER
#define _IN_DEX_HEADER

class Dex_header : public Base_dex_parssing{
private:
    //unsigned char* m_dex;
#pragma pack(push, 1)
    struct Header_item {
        enum{
            eARR_SIZE = 112,
            eMAGIC_NUMBER_SIZE = 8,
            eCHECKSUM_START = 8,
            eCHECKSUM_CAL_START = 12,
            eSHA1_START = 12,
            eSHA1_SIGNITURE_SIZE = 20,
            eSHA1_CAL_START = eCHECKSUM_CAL_START + eSHA1_SIGNITURE_SIZE,
        };
                u1                magic_number[eMAGIC_NUMBER_SIZE];           //8
                u4                checksum;                                   //12
                u1                sha1_signiture[eSHA1_SIGNITURE_SIZE];       //32
                u4                file_size;                                  //36
                u4                header_size;                                //40
                u4                endian_tag;                                 //44
                u4                link_size;                                  //48
                u4                link_off;                                   //52
                u4                map_off;                                    //56
                u4                string_ids_size;                            //60
                u4                string_ids_off;                             //64
                u4                type_ids_size;                              //68
                u4                type_ids_off;                               //72
                u4                proto_ids_size;                             //76
                u4                proto_ids_off;                              //80
                u4                field_ids_size;                             //84
                u4                field_ids_off;                              //88
                u4                method_ids_size;                            //92
                u4                method_ids_off;                             //96
                u4                class_defs_size;                            //100
                u4                class_defs_off;                             //104
                u4                data_size;                                  //108
                u4                data_off;                                   //112
    };
#pragma pack(pop)

protected:
    // virtual void __init(unsigned char* dex, unsigned int start_offset, unsigned int offset_count) {printf("???HEADER???\n");}
    unsigned int __parssing(unsigned int _command_flag);
public:
    Header_item m_header_item;
    unsigned int apply_cal_checksum(unsigned int file_full_size);
    void apply_cal_sha1(unsigned int file_full_size);
};

unsigned int Dex_header::apply_cal_checksum(unsigned int file_full_size){
    unsigned int checksum = 0;
    checksum = adler32((char*)&m_dex[Header_item::eCHECKSUM_CAL_START], file_full_size - Header_item::eCHECKSUM_CAL_START);
    Log::print(Log::LOG_DEBUG, "CAL !!! CHECKSUM\t 0x%08X\n", checksum);

   *((u4*)(m_dex + m_start_offset + Header_item::eCHECKSUM_START)) = checksum;

    m_header_item.checksum = *((u4*)(m_dex + m_start_offset + Header_item::eCHECKSUM_START));
    Log::print(Log::LOG_INFO, "0x%08X\t MODIFY_CHECKSUM\t 0x%08X\n", m_start_offset + Header_item::eCHECKSUM_START, m_header_item.checksum);

    return checksum;
}

void Dex_header::apply_cal_sha1(unsigned int file_full_size){
    // int GetSHA1Buffer(char *szHashCode, char *buffer, int len)
    
    if( GetSHA1Buffer((char*)&m_dex[Header_item::eSHA1_START], (char*)&m_dex[Header_item::eSHA1_CAL_START], file_full_size - Header_item::eSHA1_CAL_START) ){
        // 1나오면 정상
        for(int i = 0; i < Header_item::eSHA1_SIGNITURE_SIZE; i++){
            m_header_item.sha1_signiture[i] = *((u1*)(m_dex + m_start_offset + Header_item::eSHA1_START + i));
        }

        Log::print(Log::LOG_INFO, "0x%08X\t MODIFY_SHA1\t\t ", m_start_offset + Header_item::eSHA1_START);
        for(int i = 0; i < Header_item::eSHA1_SIGNITURE_SIZE; i++){
            Log::print(Log::LOG_INFO, "%02X", m_header_item.sha1_signiture[i]);
            if(i != Header_item::eSHA1_SIGNITURE_SIZE - 1) Log::print(Log::LOG_INFO, " ");
        }
        Log::print(Log::LOG_INFO, "\n");
    }
    else{
        // 0나오면 에러
        Log::print(Log::LOG_ERROR, "ERROR !! Dex_header.apply_cal_sha1 ERROR !!\n");
    }
}

unsigned int Dex_header::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;

    if(get_command_flag(eFLAG_LOG)){
        printf("OFFSET\t\t NAME\t\t DATA[Num]\t\t\t DATA[String]\n");
        printf("----------------------------------------------------------------------------\n");
    }

    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t MAGIC_NUM\t ", m_start_offset + offset_revision);
    for(int i = 0; i < Header_item::eMAGIC_NUMBER_SIZE; i++){
        m_header_item.magic_number[i] = *((u1*)(m_dex + m_start_offset + offset_revision));
        offset_revision += sizeof(u1);
    }
    if(get_command_flag(eFLAG_LOG)){
        for(int i = 0; i < Header_item::eMAGIC_NUMBER_SIZE; i++){
            Log::print(Log::LOG_INFO, "%02X", m_header_item.magic_number[i]);
            if(i != Header_item::eMAGIC_NUMBER_SIZE - 1) Log::print(Log::LOG_INFO, " ");
        }
        Log::print(Log::LOG_INFO, "\n");
    }

    m_header_item.checksum = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t CHECKSUM\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.checksum);
    offset_revision += sizeof(u4);


    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t SHA1\t\t ", m_start_offset + offset_revision);
    for(int i = 0; i < Header_item::eSHA1_SIGNITURE_SIZE; i++){
        m_header_item.sha1_signiture[i] = *((u1*)(m_dex + m_start_offset + offset_revision));
        offset_revision += sizeof(u1);
    }
    if(get_command_flag(eFLAG_LOG)){
        for(int i = 0; i < Header_item::eSHA1_SIGNITURE_SIZE; i++){
            Log::print(Log::LOG_INFO, "%02X", m_header_item.sha1_signiture[i]);
            if(i != Header_item::eSHA1_SIGNITURE_SIZE - 1) Log::print(Log::LOG_INFO, " ");
        }
        Log::print(Log::LOG_INFO, "\n");
    }

    m_header_item.file_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t file_size\t %10d\n", m_start_offset + offset_revision, m_header_item.file_size);
    offset_revision += sizeof(u4);

    m_header_item.header_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t header_size\t %10d\n", m_start_offset + offset_revision, m_header_item.header_size);
    char buff[100] = {0,};
    offset_revision += sizeof(u4);

    m_header_item.endian_tag = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t endian_tag\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.endian_tag);
    offset_revision += sizeof(u4);

    m_header_item.link_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t link_size\t %10d\n", m_start_offset + offset_revision, m_header_item.link_size);
    offset_revision += sizeof(u4);

    m_header_item.link_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t link_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.link_off);
    offset_revision += sizeof(u4);

    m_header_item.map_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t map_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.map_off);
    offset_revision += sizeof(u4);

    m_header_item.string_ids_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t string_ids_size\t %10d\n", m_start_offset + offset_revision, m_header_item.string_ids_size);
    offset_revision += sizeof(u4);

    m_header_item.string_ids_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t string_ids_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.string_ids_off);
    offset_revision += sizeof(u4);

    m_header_item.type_ids_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t type_ids_size\t %10d\n", m_start_offset + offset_revision, m_header_item.type_ids_size);
    offset_revision += sizeof(u4);

    m_header_item.type_ids_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t type_ids_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.type_ids_off);
    offset_revision += sizeof(u4);

    m_header_item.proto_ids_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t proto_ids_size\t %10d\n", m_start_offset + offset_revision, m_header_item.proto_ids_size);
    offset_revision += sizeof(u4);

    m_header_item.proto_ids_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t proto_ids_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.proto_ids_off);
    offset_revision += sizeof(u4);

    m_header_item.field_ids_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t field_ids_size\t %10d\n", m_start_offset + offset_revision, m_header_item.field_ids_size);
    offset_revision += sizeof(u4);

    m_header_item.field_ids_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t field_ids_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.field_ids_off);
    offset_revision += sizeof(u4);

    m_header_item.method_ids_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t method_ids_size\t %10d\n", m_start_offset + offset_revision, m_header_item.method_ids_size);
    offset_revision += sizeof(u4);

    m_header_item.method_ids_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t method_ids_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.method_ids_off);
    offset_revision += sizeof(u4);

    m_header_item.class_defs_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t class_defs_size\t %10d\n", m_start_offset + offset_revision, m_header_item.class_defs_size);
    offset_revision += sizeof(u4);

    m_header_item.class_defs_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t class_defs_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.class_defs_off);
    offset_revision += sizeof(u4);

    m_header_item.data_size = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t data_size\t %10d\n", m_start_offset + offset_revision, m_header_item.data_size);
    offset_revision += sizeof(u4);

    m_header_item.data_off = *((u4*)(m_dex + m_start_offset + offset_revision));
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t data_off\t 0x%08X\n", m_start_offset + offset_revision, m_header_item.data_off);
    offset_revision += sizeof(u4);

    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");

    // Log::print(Log::LOG_DEBUG, " ---- crc32 !  0x%08X\n", crc32b((char*)(m_dex + m_header_item.data_off), m_header_item.data_size) );

    return m_start_offset + offset_revision;
}



#endif  /* _IN_DEX_HEADER */