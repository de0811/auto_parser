#include "dex_common.h"
#include "dex_base_parssing.h"
#include "leb128.h"
#include "dex_access_flags.h"
#include <vector>



#if     _MSC_VER > 1000
#pragma once
#endif

#ifndef _IN_CLASS_DATA
#define _IN_CLASS_DATA

class Class_data : public Base_dex_parssing, public Base_dex_reference{
private:
#pragma pack(push, 1)
    struct Encoded_field{
        ULEB128                 field_idx_diff;
        ULEB128                 access_flags;
    };
    struct Encoded_method{
        ULEB128                 method_idx_diff;
        ULEB128                 access_flags;
        ULEB128                 code_off;
    };
    struct Class_data_item{
                ULEB128                             static_fields_size;
                ULEB128                             instance_fields_size;
                ULEB128                             direct_methods_size;
                ULEB128                             virtual_methods_size;
                std::vector<Encoded_field>          static_field;
                std::vector<Encoded_field>          instance_fields;
                std::vector<Encoded_method>         direct_methods;
                std::vector<Encoded_method>         virtual_methods;
    };
#pragma pack(pop)
    std::vector<Class_data_item>             m_class_datas;
protected:
    virtual unsigned int __parssing(unsigned int _command_flag);

public:
    Class_data(){init_reference(eTYPE_CLASS_DATA_ITEM, this);}
};

unsigned int Class_data::__parssing(unsigned int _command_flag){
    unsigned int offset_revision = 0;
    unsigned char* tmp_dex = NULL;
    m_class_datas.reserve(m_offset_count);

    Base_dex_reference* pfield_ids = get_reference(eTYPE_FIELD_ID_ITEM);
    Base_dex_reference* pmethod_ids = get_reference(eTYPE_METHOD_ID_ITEM);
    Access_flags access_flags;
    if(get_command_flag(eFLAG_LOG)){
        if(pfield_ids == NULL || pfield_ids == NULL){
            Log::print(Log::LOG_ERROR, "ERROR !! Class_data.__parssing() reference NULL\n");
            return m_start_offset;
        }
        Log::print(Log::LOG_INFO, "**Class_data Section\n");
        Log::print(Log::LOG_INFO, "OFFSET\t\t NAME\t\t\t\t\t\t\t\t\t\t VALUE\t STRING\n");
        Log::print(Log::LOG_INFO, "------------------------------------------------------------------------------------------------------------------------\n");
    }

    for(unsigned int i = 0; i < m_offset_count; i++){
        Class_data_item class_data_item;
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "COUNT %d\n", i);

        tmp_dex = m_dex + m_start_offset + offset_revision;
        class_data_item.static_fields_size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t class_data_item.static_field_size   \t %10d\n", m_start_offset + offset_revision, class_data_item.static_fields_size);
        offset_revision = tmp_dex - m_dex - m_start_offset;

        tmp_dex = m_dex + m_start_offset + offset_revision;
        class_data_item.instance_fields_size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t class_data_item.instance_fields_size\t %10d\n", m_start_offset + offset_revision, class_data_item.instance_fields_size);
        offset_revision = tmp_dex - m_dex - m_start_offset;

        tmp_dex = m_dex + m_start_offset + offset_revision;
        class_data_item.direct_methods_size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t class_data_item.direct_methods_size\t %10d\n", m_start_offset + offset_revision, class_data_item.direct_methods_size);
        offset_revision = tmp_dex - m_dex - m_start_offset;

        tmp_dex = m_dex + m_start_offset + offset_revision;
        class_data_item.virtual_methods_size = readUnsignedLeb128((const unsigned char**)&tmp_dex);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t class_data_item.virtual_methods_size\t %10d\n", m_start_offset + offset_revision, class_data_item.virtual_methods_size);
        offset_revision = tmp_dex - m_dex - m_start_offset;

        class_data_item.static_field.reserve(class_data_item.static_fields_size);
        for(unsigned int k = 0; k < class_data_item.static_fields_size; k++){
            Encoded_field encoded_field;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_field.field_idx_diff = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t static_field.field_idx_diff[%d]\t\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_field.field_idx_diff, pfield_ids->as_string(encoded_field.field_idx_diff).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_field.access_flags = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)){
                std::vector<unsigned char*> access_strings = Access_flags::get_strings( encoded_field.access_flags );
                std::string str_access_flags = "";
                for(unsigned int k = 0; k < access_strings.size(); k++){
                    str_access_flags.append( (char*)access_strings[k] );
                    if( k != access_strings.size() - 1 ) str_access_flags.append("|");
                }
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t static_field.access_flags[%d]\t\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_field.access_flags, str_access_flags.c_str());
            }
            offset_revision = tmp_dex - m_dex - m_start_offset;
            class_data_item.static_field.push_back(encoded_field);
        }

        class_data_item.instance_fields.reserve(class_data_item.instance_fields_size);
        for(unsigned int k = 0; k < class_data_item.instance_fields_size; k++){
            Encoded_field encoded_field;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_field.field_idx_diff = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t instance_fields.field_idx_diff[%d]\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_field.field_idx_diff, pfield_ids->as_string(encoded_field.field_idx_diff).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_field.access_flags = readUnsignedLeb128((const unsigned char**)&tmp_dex);

            if(get_command_flag(eFLAG_LOG)){
                std::vector<unsigned char*> access_strings = Access_flags::get_strings( encoded_field.access_flags );
                std::string str_access_flags = "";
                for(unsigned int k = 0; k < access_strings.size(); k++){
                    str_access_flags.append( (char*)access_strings[k] );
                    if( k != access_strings.size() - 1 ) str_access_flags.append("|");
                }
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t instance_fields.access_flags[%d]\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_field.access_flags, str_access_flags.c_str());
            }
            offset_revision = tmp_dex - m_dex - m_start_offset;

            class_data_item.instance_fields.push_back(encoded_field);
        }

        class_data_item.direct_methods.reserve(class_data_item.direct_methods_size);
        for(unsigned int k = 0; k < class_data_item.direct_methods_size; k++){
            Encoded_method encoded_method;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_method.method_idx_diff = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t direct_methods.method_idx_diff[%d]\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_method.method_idx_diff, pmethod_ids->as_string(encoded_method.method_idx_diff).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_method.access_flags = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)){
                std::vector<unsigned char*> access_strings = Access_flags::get_strings( encoded_method.access_flags );
                std::string str_access_flags = "";
                for(unsigned int k = 0; k < access_strings.size(); k++){
                    str_access_flags.append( (char*)access_strings[k] );
                    if( k != access_strings.size() - 1 ) str_access_flags.append("|");
                }
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t direct_methods.access_flags[%d]\t\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_method.access_flags, str_access_flags.c_str());
            }
            offset_revision = tmp_dex - m_dex - m_start_offset;

            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_method.code_off = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t direct_methods.code_off[%d]\t\t\t %10d\n", m_start_offset + offset_revision, i, encoded_method.code_off);
            offset_revision = tmp_dex - m_dex - m_start_offset;

            class_data_item.direct_methods.push_back(encoded_method);
        }

        class_data_item.virtual_methods.reserve(class_data_item.virtual_methods_size);
        for(unsigned int k = 0; k < class_data_item.virtual_methods_size; k++){
            Encoded_method encoded_method;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_method.method_idx_diff = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t virtual_method.method_idx_diff[%d]\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_method.method_idx_diff, pmethod_ids->as_string(encoded_method.method_idx_diff).c_str());
            offset_revision = tmp_dex - m_dex - m_start_offset;
            
            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_method.access_flags = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)){
                std::vector<unsigned char*> access_strings = Access_flags::get_strings( encoded_method.access_flags );
                std::string str_access_flags = "";
                for(unsigned int k = 0; k < access_strings.size(); k++){
                    str_access_flags.append( (char*)access_strings[k] );
                    if( k != access_strings.size() - 1 ) str_access_flags.append("|");
                }
                if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t virtual_method.access_flags[%d]\t\t %10d\t %s\n", m_start_offset + offset_revision, i, encoded_method.access_flags, str_access_flags.c_str());
            }
            offset_revision = tmp_dex - m_dex - m_start_offset;

            tmp_dex = m_dex + m_start_offset + offset_revision;
            encoded_method.code_off = readUnsignedLeb128((const unsigned char**)&tmp_dex);
            if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "0x%08X\t\t virtual_method.code_off[%d]\t\t\t 0x%08X\n", m_start_offset + offset_revision, i, encoded_method.code_off);
            offset_revision = tmp_dex - m_dex - m_start_offset;

            class_data_item.virtual_methods.push_back(encoded_method);
        }


        m_class_datas.push_back(class_data_item);
        if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");
    }
    if(get_command_flag(eFLAG_LOG)) Log::print(Log::LOG_INFO, "\n");

    return m_start_offset + offset_revision;
}

#endif /* _IN_CLASS_DATA */